// stdafx.h
// Project DataVisualizerGUI
#ifndef __PrecompiledHeader_H__
#define __PrecompiledHeader_H__
#define WIN32_LEAN_AND_MEAN

// Qt
#include <QWidget>

// C++
#include <map>
#include <queue>
#include <cmath>
#include <ctime>
#include <string>
#include <vector>
#include <cstring>
#include <sstream>
#include <numeric>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <algorithm>

// JSON Parser
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/rapidjson.h>

// Included for drawing plots and graphs.
#include "qcustomplot.h"

// Undefine max
#undef max

#endif