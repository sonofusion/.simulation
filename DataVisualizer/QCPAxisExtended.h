// QCPAxisExtended.h - QCPAxis class extension declaration.
// Written By Jesse Z. Zhong
#ifndef __QCP_Axis_Extended_H__
#define __QCP_Axis_Extended_H__
#pragma region Includes
#include "qcustomplot.h"
#include <QScrollBar>
using namespace std;
using namespace QCPlot;
#pragma endregion
namespace DataVisualizerGUI {

	/*! 
	  Extends the axis class to allow scrolling when zoomed.
	*/
	class QCPAxisExtended : public QCPAxis {
	#pragma region Constants
		//! Sets the starting state of the scroll bar.
		//! NOTE: False is hidden; True is show.
		static const bool DefaultScrollState;

		//! The height of the scroll bar.
		static const int ScrollBarHeight;
	#pragma endregion
	public:
		/*! 
		  Constructor
		*/
		QCPAxisExtended(QCustomPlot* parentPlot,
			QCPAxis::AxisType type);

		/*! 
		  Sets if the viewport is zoomed or not.
		*/
		void SetIsZoomed(bool isZoomed);

		/*! 
		  Returns the reference to the scroll bar.
		*/
		QScrollBar* GetScrollBar();

	protected:
		/*! 
		  Extend the draw method to incorporate scroll bars.
		*/
		void draw(QCPPainter* painter);

		/*! 
		  Overload the calculate margin method to include scroll bar size.
		*/
		int calculateMargin() const;

	private:
		QScrollBar* ScrollBar_;		//!< The scroll bar for shifting the viewport when zoomed.
		bool IsZoomed_;				//!< Indicates if the view port is zoomed or not.
	};
}

#endif // !__QCP_Axis_Extended_H__