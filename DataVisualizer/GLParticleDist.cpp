// GLParticleDist.cpp
// Written by Joshua Mellott-Lillie and Jesse Z. Zhong
#include "stdafx.h"

#include "qmessagebox.h"

#include "GLParticleDist.h"
#include "GraphGL.h"
#include "GL/GLU.h"

#include <map>
#include <limits>

using namespace std;
using namespace DataVisualizerGUI;

double GLParticleDist::maximalX = 0.0;
double GLParticleDist::maximalY = 0.0;
double GLParticleDist::maximalZ = 0.0;

GLParticleDist::GLParticleDist(QWidget *parent)
	: GraphGL(parent) {
		
}


GLParticleDist::~GLParticleDist(void) {
}

void GLParticleDist::changeDataSet(int index)
{
	this->listNumber = this->listMap[index];
	updateGL();
}

void GLParticleDist::setData(DataSavepoint *dataSavePoint, int index) {

	this->makeCurrent();

	GLuint newList = glGenLists(1);

	map<int, Vector3<float>*> gasColorMap;
	gasColorMap[0] = new Vector3<float>(0.0, 0.5, 0.5);
	gasColorMap[1] = new Vector3<float>(0.5, 0.0, 0.0);
	gasColorMap[2] = new Vector3<float>(0.5, 0.5, 0.0);
	gasColorMap[3] = new Vector3<float>(0.5, 0.0, 0.5);
	gasColorMap[4] = new Vector3<float>(0.5, 0.5, 0.5);
	gasColorMap[5] = new Vector3<float>(1.0, 0.0, 0.0);
	gasColorMap[6] = new Vector3<float>(1.0, 0.5, 0.5);
	gasColorMap[7] = new Vector3<float>(1.0, 0.5, 0.5);
	gasColorMap[8] = new Vector3<float>(1.0, 0.5, 0.5);

	vector<ParticleData> data = dataSavePoint->GetData();

	if(data.size() == 0)
	{
		glNewList(newList, GL_COMPILE);
		glEndList();
		this->listMap[index] = newList;
		return;
	}

	glNewList(newList, GL_COMPILE);

	int scale = 100;

	if(index == 1)
	{
		const int maxTest = 15;
	
		double maxX[maxTest];
		double maxY[maxTest];
		double maxZ[maxTest];

		for(int i = 0; i < maxTest; i++)
			maxX[i] = maxY[i] = maxZ[i] = 0;

		foreach(ParticleData pd, data) {
			double pushMaxY = pd.Position.Y;
			double pushMaxZ = pd.Position.Z;
			double pushMaxX = pd.Position.X;
			for(int i = 0; i < maxTest; i++)
			{
				if(pushMaxY > maxY[i])
				{
					double temp = maxY[i];
					maxY[i] = pushMaxY;
					pushMaxY = temp;
				}

				if(pushMaxZ > maxZ[i])
				{
					double temp = maxZ[i];
					maxZ[i] = pushMaxZ;
					pushMaxZ = temp;
				}

				if(pushMaxX > maxX[i])
				{
					double temp = maxX[i];
					maxX[i] = pushMaxX;
					pushMaxX = temp;
				}
			}
		}

		this->maximalX = maxX[maxTest - 1];
		this->maximalZ = maxY[maxTest - 1];
		this->maximalZ = maxZ[maxTest - 1];
	}
	

	glBegin(GL_POINTS);

	double xScale = maximalX / (float)(scale);
	double zScale = maximalZ / (float)(scale);

	foreach(ParticleData pd, data) {
		Vector3<float> *color;

		if(gasColorMap.count(pd.GasType) != 0)
			color = gasColorMap[pd.GasType];
		else
		{
			color = new Vector3<float>(0.0,0.0,0.0);
			//count++;
		}

		glColor4f(color->X, color->Y, color->Z, 0.5f);
		glVertex2d(pd.Position.X / xScale, pd.Position.Z / zScale);
	}

	glEnd();
	glEndList();

	this->listMap[index] = newList;
}

void GLParticleDist::draw() {
	glClear(GL_COLOR_BUFFER_BIT);
	glCallList(this->listNumber);

	glPushMatrix();
    glColor4f(0.2, 0.2, 0.2, 1.0);
    renderText(90, 10, 0, "Particle Distribution");
    glPopMatrix();
}
