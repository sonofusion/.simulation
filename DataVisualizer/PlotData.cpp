// PlotData.cpp - PlotData class implementation.
// Written By Jesse Z. Zhong
#include "stdafx.h"
#include "PlotData.h"

using namespace DataVisualizerGUI;

const QColor PlotData::DefaultColor = Qt::darkCyan;

PlotData::PlotData() {
	this->X = QVector<double>();
	this->Y = QVector<double>();
	this->Color = DefaultColor;
	this->xOffset_ = 0.0;
	this->yOffset_ = 0.0;
	this->xLower_ = 0.0;
	this->xUpper_ = 0.0;
	this->yLower_ = 0.0;
	this->yUpper_ = 0.0;
	this->Selected_ = false;
}

PlotData& PlotData::operator=(const PlotData& source) {
	this->Name = source.Name;
	this->Color = source.Color;
	this->X = source.X;
	this->Y = source.Y;
	this->xOffset_ = source.GetXOffset();
	this->yOffset_ = source.GetYOffset();
	this->xLower_ = source.GetXLower();
	this->xUpper_ = source.GetXUpper();
	this->yLower_ = source.GetYLower();
	this->yUpper_ = source.GetYUpper();
	this->Selected_ = source.GetSelected();
	return *this;
}

void PlotData::AddPoint(double x, double y) {
	this->AddX(x);
	this->AddY(y);
}	

void PlotData::AddX(double x) {
	this->X.push_back(x);
	this->SetXUpper((x > this->xUpper_) ? x : this->xUpper_);
	this->SetXLower((x < this->xLower_) ? x : this->xLower_);
}

void PlotData::AddY(double y) {
	this->Y.push_back(y);
	this->SetYUpper((y > this->yUpper_) ? y : this->yUpper_);
	this->SetYLower((y < this->yLower_) ? y : this->yLower_);
}

double PlotData::GetXOffset() const { 
	return this->xOffset_; 
}

double PlotData::GetYOffset() const { 
	return this->yOffset_; 
}

void PlotData::SetXYOffset(double x, double y) {

	// Iterate through all points and adjust the values.
	double differenceX = x - this->xOffset_;
	double differenceY = y - this->yOffset_;
	if(this->X.size() == this->Y.size()) {
		for(int i = 0, j = this->X.size(); i < j; i++) {
			this->X[i] += differenceX;
			this->Y[i] += differenceY;
		}
	}

	// Set the internal values.
	this->xOffset_ = x;
	this->yOffset_ = y;
}

void PlotData::SetXOffset(double val) {

	// Iterate through all points and adjust the values.
	double difference = val - this->xOffset_;
	for(int i = 0, j = this->X.size(); i < j; i++)
		this->X[i] += difference;

	// Set the internal value.
	this->xOffset_ = val;
}

void PlotData::SetYOffset(double val) {

	// Iterate through all points and adjust the values.
	double difference = val - this->yOffset_;
	for(int i = 0, j = this->Y.size(); i < j; i++)
		this->Y[i] += difference;

	// Set the internal value.
	this->yOffset_ = val;
}

double PlotData::GetXLower() const { 
	return this->xLower_ + this->xOffset_; 
}

double PlotData::GetXUpper() const { 
	return this->xUpper_ + this->xOffset_; 
}

double PlotData::GetYLower() const { 
	return this->yLower_ + this->yOffset_; 
}

double PlotData::GetYUpper() const { 
	return this->yUpper_ + this->yOffset_; 
}

void PlotData::SetXLower(double val) { 
	this->xLower_ = val; 
}

void PlotData::SetXUpper(double val) { 
	this->xUpper_ = val; 
}

void PlotData::SetYLower(double val) { 
	this->yLower_ = val;
}

void PlotData::SetYUpper(double val) { 
	this->yUpper_ = val; 
}

bool PlotData::GetSelected() const { 
	return this->Selected_; 
}

void PlotData::SetSelected(bool selected) { 
	this->Selected_ = selected; 
}