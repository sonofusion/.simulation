// MetadataName.cpp - Metadata name class implementation.
// Written by Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "MetadataName.h"
using namespace DataVisualizerGUI;
#pragma endregion

MetadataName::MetadataName(QString complex, QString simple) {
	this->Complex = complex;
	this->Simple = simple;
}

const MetadataName MetadataName::GetName(MetadataName::EName name) {
	return (DisplayNames.end() != (DisplayNames.find(name))) 
		? DisplayNames.at(name) : MetadataName();
}

const int MetadataName::GetSize() {
	return DisplayNames.size();
}

const map<MetadataName::EName, MetadataName> 
	MetadataName::DisplayNames = MetadataName::InitDisplayNames();

map<MetadataName::EName, MetadataName> MetadataName::InitDisplayNames() {

	// Create an instance of the map.
	map<EName, MetadataName> displayNames;

	// Initiate the map with the correct names.
	displayNames[MD_TIME] = MetadataName("Time ( <i>s</i> )", "t ( <i>s</i> )");
	displayNames[MD_WALL_RAD] = MetadataName("Wall Radius ( <i>s</i> )", "R<sub>Wall</sub> ( <i>m</i> )");
	displayNames[MD_WALL_VELO] = MetadataName("Wall Velocity ( <i>m/s</i> )", "V<sub>Wall</sub> ( <i>m/s</i> )");
	displayNames[MD_MAX_PRESS] = MetadataName("Max Pressure ( <i>atm</i> )", "P<sub>Max</sub> ( <i>atm</i> )");
	displayNames[MD_AVG_PRESS] = MetadataName("Average Pressure ( <i>atm</i> )", "P<sub>Avg</sub> ( <i>atm</i> )");
	displayNames[MD_MAX_TEMP] = MetadataName("Max Temperature ( <i>K</i> )", "T<sub>Max</sub> ( <i>K</i> )");
	displayNames[MD_AVG_TEMP] = MetadataName("Average Temperature ( <i>K</i> )", "T<sub>Avg</sub> ( <i>K</i> )");
	displayNames[MD_MAX_ENERGY] = MetadataName("Max Energy ( <i>KeV</i> )", "E<sub>Max</sub> ( <i>KeV</i> )");
	displayNames[MD_AVG_ENERGY] = MetadataName("Average Energy ( <i>KeV</i> )", "E<sub>Avg</sub> ( <i>KeV</i> )");
	displayNames[MD_FUSION_RATE] = MetadataName("Fusion Rate ( <i>&#37;</i> )", "F ( <i>&#37;</i> )");
	displayNames[MD_MAX_DENSE] = MetadataName("Max Density ( <i>kg/m<sup>3</sup></i> )", "&#961;<sub>Max</sub> ( <i>kg/m<sup>3</sup></i> )");
	displayNames[MD_AVG_DENSE] = MetadataName("Average Density ( <i>kg/m<sup>3</sup></i> )", "&#961;<sub>Avg</sub> ( <i>kg/m<sup>3</sup></i> )");

	// Return the generated map.
	return displayNames;
}