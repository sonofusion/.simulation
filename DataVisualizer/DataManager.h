// DataManager.h - Data Manager Class Declaration
// Written By Jesse Z. Zhong
#ifndef __Data_Manager_H__
#define __Data_Manager_H__
#pragma region Includes
#include "stdafx.h"
#include "DataSnapshot.h"
#include "DataSavepoint.h"
#include "DataCollisionEnergy.h"
#include "DataRPK.h"
using namespace std;
using namespace DataReader;
#pragma endregion
namespace DataVisualizerGUI {
	/*! 
	  Object used for reading and storing data.
	*/
	class DataManager : public QObject {
		Q_OBJECT

	#pragma region File Extensions
		//! File extension for snapshot files.
		static const QString EXT_Snapshot;

		//! File extension for save point files.
		static const QString EXT_Save_Point;

		//! File extension for collision energy files.
		static const QString EXT_Collide_Energy;

		//! File extension for snapshot files.
		static const QString EXT_RPK_Data;

		//! Keyword for indicating snapshot files.
		static const QString KEY_Snapshot;

		//! Keyword for indicating save point files.
		static const QString KEY_Save_Point;

		//! Keyword for indicating collision energy files.
		static const QString KEY_Collide_Energy;

		//! Keyword for indicating RPK data files.
		static const QString KEY_RPK_Data;

		//! Asterisk reference.
		static const QString CHR_Asterisk;

		// Forward slash reference.
		static const QString CHR_Slash;
	#pragma endregion
	#pragma region ThreadPool and File I/O
		//! Indicates the maximum number of threads
		//! and file pointers allowed at the same time.
		static const int MaxReadingTasks;
	#pragma endregion
	public:
		/*! 
		  Constructor
		*/
		DataManager(QWidget* parent = 0);

		/*! 
		  Destructor
		*/
		~DataManager();
		
		/*! 
		  Reads and stores all available
		  files in a specified directory.
		*/
		void StoreData();

		/*! 
		  Returns the RP Guess 2 data.
		  Returns a blank RPK if no data is found.
		*/
		DataRPK& GetRPGuess2Data();

		/*! 
		  Returns the RP Actual data.
		  Returns a blank RPK if no data is found.
		*/
		DataRPK& GetRPActual();

		/*! 
		  Returns snapshot data at a given index.
		  Returns a blank snapshot if no data is found.
		*/
		DataSnapshot& GetSnapshotData(int index);
		 
		/*! 
		  Returns the save point data at a given index.
		  Returns a blank save point data if no data is found.
		*/
		DataSavepoint& GetSavepointData(int index);
		
		/*! 
		  Returns the collision energy data at a given index.
		  Returns a blank collision energy data if no data is found.
		*/
		DataCollisionEnergy& GetCollisionEnergyData(int index);
		 
		/*! 
		  Returns the maximum number of files in either
		  the snapshot, save point, or collision energy lists.
		*/
		int GetMaxSets() const;

		/*! 
		  Returns the simulation output directory.
		*/
		QString GetDirPath() const;

		/*! 
		  Sets the simulation output directory.
		*/
		void SetDirPath(const QString& dir);

	protected:
		/*! 
		  Check if an index is within the 
		  range of a given container or array.
		*/
		template<typename T>
		bool CheckBounds(int index, vector<T> container) const;

		// Members
		DataRPK RPGuess2_;		//!< Holds the RP prediction data set.
		DataRPK RPActual_;		//!< Holds the RP actual data set.
		vector<DataSnapshot> Snapshot_;		//!< List of compiled snapshot data.
		vector<DataSavepoint> Savepoint_;	//!< List of compiled save point data.
		vector<DataCollisionEnergy> CollideEnergy_;		//!< List of compiled collision energy data.
		QString DirPath_;		//!< Path of the file directory that the files are read from.
		int MaxSets_;			//!< Records the number of files whole sets stored.

		// Reference to a widget that will be used
		// for any temporary UI elements created.
		QWidget* Parent_;
	};
}
#endif // !__Data_Manager_H__

