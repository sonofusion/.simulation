// DataVisualizer.cpp - Main UI class implementation.
// Written By Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include <QDir>
#include <QFileDialog>
#include <QtOpenGL/QtOpenGL>

#include "DataVisualizer.h"
using namespace DataReader;
using namespace DataVisualizerGUI;
#pragma endregion
#pragma region Constants
#pragma region Window Titles
const string DataVisualizer::ApplicationName = "Data Visualizer";
const string DataVisualizer::OrganizationName = "Quantum Potential Corporation";
const string DataVisualizer::StartupTitle = "Welcome to Data Visualizer";
#pragma endregion
#pragma region Refresh Interval
const int DataVisualizer::DefaultInterval = 200;
const int DataVisualizer::MinInterval = 100;
const int DataVisualizer::MaxInterval = 1000;
#pragma endregion
#pragma region Plot Dimensions
const int DataVisualizer::MinPlotSize = 300;
const int DataVisualizer::MaxPlotSize = 1280;
const int DataVisualizer::ResizeIncrement = 10;
#pragma endregion
#pragma region Registry Items
const QString DataVisualizer::FileDirectory = "File Directory";
const QString DataVisualizer::RefereshInterval = "Refresh Interval";
#pragma endregion
#pragma endregion
#pragma region Unit Testing
int DataVisualizerGUI::PullInMyLibrary() { return 0; }
#pragma endregion
#pragma region Instance
		DataVisualizer::DataVisualizer(QWidget *parent) 
			: QMainWindow(parent) {

				// Program data storage containers.
				this->DataManager_ = new DataManager(this);
				this->ProgramSettings_ = new QSettings(OrganizationName.c_str(),
					ApplicationName.c_str(), this);

				// Initialize the main form.
				this->UI_.setupUi(this);
				this->UI_.InitializeUI(this);
				this->setWindowTitle(tr(ApplicationName.c_str()));

				// Initialize the startup form.
				this->StartupBox_ = new QDialog(this);
				this->StartupUI_.setupUi(this->StartupBox_);
				this->StartupUI_.InitializeUI(this->StartupBox_);
				this->StartupBox_->setWindowTitle(StartupTitle.c_str());

				// Initialize members.
				this->CurrentPlot_ = 0;
				this->PlotsIterating_ = false;
				this->PlotUpdateTimer_ = new QTimer(this);

				// Initialize and bring together various UI elements.
				this->SetupUI();

				// Initialize user defined signals and slots.
				this->ConnectWidgets();

				// Display the startup UI.
				this->StartupBox_->show();
		}

		DataVisualizer::~DataVisualizer() {

			// Delete the startup box.
			delete this->StartupBox_;
		}
	#pragma endregion
#pragma region Private Slots
#pragma region Data and Plots
void DataVisualizer::StoreData() {

	// Localize the previous directory, if it exists.
	QString dir = "";
	if(this->ProgramSettings_->contains(FileDirectory))
		dir = this->ProgramSettings_->value(FileDirectory).toString();

	// Check if the director is valid.
	if(!QDir().exists(dir)) {
		Util::Print(dir.toStdString() + " does not exist.");
		return;
	}

	// Set the directory that the data manager will read data from.
	this->DataManager_->SetDirPath(dir);

	// Begin storing the data into the program.
	Util::Print("Attempting to read data from " + dir.toStdString() + ".");
	this->DataManager_->StoreData();

	// Load the stored data into display lists for the OpenGL plots
	int maxSets = this->DataManager_->GetMaxSets();

	this->UI_.ParticleDist->makeCurrent();
	for(int i = 0; i < maxSets; i++)
		this->UI_.ParticleDist->setData(&(this->DataManager_->GetSavepointData(i)), i);

	this->UI_.EnergyCollision->makeCurrent();
	for(int i = 0; i < maxSets; i++)
		this->UI_.EnergyCollision->setData(&(this->DataManager_->GetCollisionEnergyData(i)), i);

	Util::Print("Data stored.");

	// Determine the number of the last snapshot.
	int lastSnapshot = (this->DataManager_->GetMaxSets() < -1) 
		? 0 : (this->DataManager_->GetMaxSets() - 1);
	assert(lastSnapshot >= 0);

	// Initialize slider bar with number of the first and last snapshot.
	this->UI_.SeekSlider->setMinimum(0);
	this->UI_.SeekSlider->setMaximum(lastSnapshot);

	// Initialize the the spin box with the number of the first and last snapshot.
	this->UI_.SnapshotSpinBox->setMinimum(0);
	this->UI_.SnapshotSpinBox->setMaximum(lastSnapshot);

	// Reset the all the plot related UI's.
	this->RefreshGraphs(0);
}

void DataVisualizer::SubRefreshGraphs(int value) {
	// bounds checking
	if(!((0 <= value) && (value < this->DataManager_->GetMaxSets())))
		return;

	// Assign the passed value to the current plot index.
	this->CurrentPlot_ = value;

	// Set the position of the slider to the value of the current plot.
	this->UI_.SeekSlider->setValue(this->CurrentPlot_);

	// Set the value of the spin box to the value of the current plot.
	this->UI_.SnapshotSpinBox->setValue(this->CurrentPlot_);
}

void DataVisualizer::RefreshGraphs(int value) {
	
	// Check if the value passed is within legal bounds.
	if((0 <= value) && (value < this->DataManager_->GetMaxSets())) {

			// Assign the passed value to the current plot index.
			this->CurrentPlot_ = value;

			// Remove all the previous data in the plots.
			this->UI_.ClearAll();

			// Localize data files.
			auto currentSnapshot = this->DataManager_->GetSnapshotData(this->CurrentPlot_);
			auto rpguess2 = this->DataManager_->GetRPGuess2Data();
			auto rpactual = this->DataManager_->GetRPActual();

			// Store data into their respective plots.
			this->UI_.IonRadius->AddPlot(currentSnapshot);
			this->UI_.DensityRadius->AddPlot(currentSnapshot);
			this->UI_.TempRadius->AddPlot(currentSnapshot);
			this->UI_.WallRadTime->AddPlot(rpguess2);
			this->UI_.WallRadTime->AddPlot(rpactual, this->CurrentPlot_ + 1); // + 1 for size, not index.

			this->UI_.ParticleDist->changeDataSet(this->CurrentPlot_);
			this->UI_.EnergyCollision->changeDataSet(this->CurrentPlot_);

			// Refresh to redraw the plots.
			this->UI_.RefreshAll();

			// Set the position of the slider to the value of the current plot.
			this->UI_.SeekSlider->setValue(this->CurrentPlot_);

			// Set the value of the spin box to the value of the current plot.
			this->UI_.SnapshotSpinBox->setValue(this->CurrentPlot_);

			// Write certain averages and maximums to the line edits.
			this->UI_.ChangeMDValue(MDName::MD_TIME, currentSnapshot.GetSimulationTime());
			this->UI_.ChangeMDValue(MDName::MD_WALL_RAD, currentSnapshot.GetBubbleRadius());
			this->UI_.ChangeMDValue(MDName::MD_WALL_VELO, currentSnapshot.GetWallVelocity());
			//this->UI_.ChangeMDValue(MDName::MD_MAX_PRESS, currentSnapshot.GetMaxPressure());
			//this->UI_.ChangeMDValue(MDName::MD_AVG_PRESS, currentSnapshot.GetAvgPressure());
			this->UI_.ChangeMDValue(MDName::MD_MAX_TEMP, currentSnapshot.GetMaxTemperature());
			this->UI_.ChangeMDValue(MDName::MD_AVG_TEMP, currentSnapshot.GetAvgTemperature());
			this->UI_.ChangeMDValue(MDName::MD_MAX_ENERGY, currentSnapshot.GetMaxEnergy());
			this->UI_.ChangeMDValue(MDName::MD_AVG_ENERGY, currentSnapshot.GetAvgEnergy());
			this->UI_.ChangeMDValue(MDName::MD_FUSION_RATE, currentSnapshot.GetFusionRate());
			this->UI_.ChangeMDValue(MDName::MD_MAX_DENSE, currentSnapshot.GetMaxDensity());
			this->UI_.ChangeMDValue(MDName::MD_AVG_DENSE, currentSnapshot.GetAvgDensity());
		
	} else {
		// Stop the timer if it is currently running.
		this->PlotsIterating_ = false;
		if(this->PlotUpdateTimer_->isActive()) this->PlotUpdateTimer_->stop();
		this->UI_.StartButton->setText("Start");
	}
}

void DataVisualizer::RefreshGraphs() {

	// Clears and redraws the plots with the current data set.
	this->RefreshGraphs(this->CurrentPlot_);

	// If the animation state is true, iterate the current plot up.
	if(this->PlotsIterating_) this->CurrentPlot_++;
}

void DataVisualizer::ToggleIteratingPlots() {
	
	// Invert the previous state.
	if(this->PlotsIterating_) 
		this->PlotsIterating_ = false;
	else if(!this->PlotsIterating_) 
		this->PlotsIterating_ = true;

	// Change the display text of the
	// button and toggle the state of the
	// timer depending on the current state.
	if(this->PlotsIterating_) {
		this->PlotUpdateTimer_->start();
		this->UI_.StartButton->setText("Stop");
	} else {
		this->PlotUpdateTimer_->stop();
		this->UI_.StartButton->setText("Start");
	}
}

void DataVisualizer::EnlargePlots() {
	if(this->UI_.GetGraphWidth() < MaxPlotSize)
		this->UI_.ChangePlotWidths(this->UI_.GetGraphWidth() + ResizeIncrement);
}

void DataVisualizer::ShrinkPlots() {
	if(this->UI_.GetGraphWidth() > MinPlotSize)
		this->UI_.ChangePlotWidths(this->UI_.GetGraphWidth() - ResizeIncrement);
}
#pragma endregion
#pragma region Preference Tab
void DataVisualizer::UserSetDirectory() {

	// Localize the previous directory, if it exists.
	QString prevDir = "";
	if(this->ProgramSettings_->contains(FileDirectory))
		prevDir = this->ProgramSettings_->value(FileDirectory).toString();

	// Open FileDialog to prompt the user for the directory of the data files.
	QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), prevDir);
		
	// Check if there was a directory referenced.
	if(dir == "")
		return;

	// Change the directory saved to the registry.
	this->ProgramSettings_->setValue(FileDirectory, dir);

	// Sets the line edits for the UI's.
	this->UI_.SimulationDirectoryLineEdit->setText(dir);
	this->StartupUI_.DirectoryLineEdit->setText(dir);

	// Emit signals alerting connected methods of the change.
	emit ChangedDirectory(dir);
	emit ChangedDirectory();
}

void DataVisualizer::ChangeInterval(int value) {

	// Check if the interval is non-negative.
	if(value < 0) {
		Util::Print("The refresh interval cannot be a negative number.");
		return;
	}

	// Sets the interval for the timer with the passed value.
	this->PlotUpdateTimer_->setInterval(value);

	// Re-adjust the slider with the correct value.
	this->UI_.RefreshIntervalSlider->setValue(value);

	// Change the line edit used to display the interval.
	const QString label = "%1 Milliseconds";
	this->UI_.RefreshIntervalMSLabel->setText(label.arg(value));
	
	// Change the value stored in the registry.
	this->ProgramSettings_->setValue(RefereshInterval, value);
}

void DataVisualizer::ChangeViewMode(int index) {
	this->UI_.ChangeLabelDisplayMode((bool)index);
}
#pragma endregion
#pragma endregion
#pragma region Helper Methods
void DataVisualizer::ConnectWidgets() {
	connect(this->UI_.StartButton, SIGNAL(clicked()), this, SLOT(ToggleIteratingPlots()));
	connect(this->UI_.SeekSlider, SIGNAL(valueChanged(int)), this, SLOT(SubRefreshGraphs(int)));
	connect(this->UI_.SnapshotSpinBox, SIGNAL(valueChanged(int)), this, SLOT(RefreshGraphs(int)));
	connect(this->UI_.ViewModeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(ChangeViewMode(int)));
	connect(this->UI_.BrowseDirectoryButton, SIGNAL(clicked()), this, SLOT(UserSetDirectory()));
	connect(this->UI_.DirLoadButton, SIGNAL(clicked()), this, SLOT(StoreData()));
	connect(this->UI_.RefreshIntervalSlider, SIGNAL(valueChanged(int)), this, SLOT(ChangeInterval(int)));
	connect(this->UI_.EnlargeGraphButton, SIGNAL(pressed()), this, SLOT(EnlargePlots()));
	connect(this->UI_.ShrinkGraphButton, SIGNAL(pressed()), this, SLOT(ShrinkPlots()));
	connect(this->PlotUpdateTimer_, SIGNAL(timeout()), this, SLOT(RefreshGraphs()));
	connect(this->StartupUI_.buttonBox, SIGNAL(accepted()), this, SLOT(StoreData()));
	connect(this->StartupUI_.BrowseDirectoryButton, SIGNAL(clicked()), this, SLOT(UserSetDirectory()));
}

void DataVisualizer::SetupUI() {

	// Allows for button effects to continue when held down.
	this->UI_.EnlargeGraphButton->setAutoRepeat(true);
	this->UI_.ShrinkGraphButton->setAutoRepeat(true);

	// Bind keyboard keys to each of the buttons, so that they can be used as hotkeys.
	this->UI_.EnlargeGraphButton->setShortcut(QString("="));
	this->UI_.ShrinkGraphButton->setShortcut(QString("-"));

	// Writes tool tips for each button indicating what they do.
	this->UI_.EnlargeGraphButton->setToolTip(QString("Enlarges the plots when pressed. Alternatively, press <b>\"=\"</b> to enlarge."));
	this->UI_.ShrinkGraphButton->setToolTip(QString("Shrinks the plots when pressed. Alternatively, press <b>\"=\"</b> to shrink."));

	// Initialize the numerical range of the refresh interval slider.
	this->UI_.RefreshIntervalSlider->setMinimum(MinInterval);
	this->UI_.RefreshIntervalSlider->setMaximum(MaxInterval);

	// Check if the refresh interval is recorded in the registry.
	// If so, grab the value and set it to the slider. Otherwise assign the default.
	if(this->ProgramSettings_->contains(RefereshInterval))
		this->ChangeInterval(this->ProgramSettings_->value(RefereshInterval).toInt());
	else
		this->ChangeInterval(DefaultInterval);

	// Localize the previous directory, if it exists.
	QString dir = "";
	if(this->ProgramSettings_->contains(FileDirectory))
		dir = this->ProgramSettings_->value(FileDirectory).toString();

	// Sets the line edits for the UI's.
	this->UI_.SimulationDirectoryLineEdit->setText(dir);
	this->StartupUI_.DirectoryLineEdit->setText(dir);
}
#pragma endregion