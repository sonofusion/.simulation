// TaskRunner.h - Implementation for TaskRunner.
// Written By Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "TaskRunner.h"
using namespace DataVisualizerGUI;
#pragma endregion

TaskRunner::TaskRunner(Task task, int val, 
		   const QString& str, QObject* parent) 
		   : QObject(parent), QRunnable() {
			   this->Task_ = task;
			   this->Val_ = val;
			   this->Str_ = str;
			   this->setAutoDelete(true);
}

void TaskRunner::run() {
	if(this->Task_ != NULL)
		this->Task_(this->Val_, this->Str_);
	emit Finished();
}