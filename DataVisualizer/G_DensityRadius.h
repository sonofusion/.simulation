// G_DensityRadius.h - Density vs Radius plot class declaration.
// Written By Jesse Z. Zhong
#ifndef __Density_vs_Radius_H__
#define __Density_vs_Radius_H__
#pragma region Includes
#include "Graph.h"
#include "DataSnapshot.h"
using namespace std;
using namespace DataReader;
#pragma endregion
namespace DataVisualizerGUI {
	/*! 
	  Implements shell density versus shell 
	  radius plots using data from snapshots.
	*/
	class G_DensityRadius : public Graph<DataSnapshot> {
	public:
		/*! 
		  Constructor
		*/
		G_DensityRadius(QWidget* parent = NULL);

	protected:
		/*! 
		  Adds a new set of data to a plot.
		*/
		virtual void ProcessPoints(DataSnapshot& data, int upper);
	};
}
#endif // !__Density_vs_Radius_H__