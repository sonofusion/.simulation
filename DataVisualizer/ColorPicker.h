// ColorPicker.h - ColorPicker Class Declaration.
// Written By Jesse Z. Zhong
#ifndef __ColorPicker_H__
#define __ColorPicker_H__
#include "stdafx.h"
using namespace std;
namespace DataVisualizerGUI {

	/*! 
	  Allows the drawing of a color from a list of presets.
	*/
	class ColorPicker {
	public:
		
		/*! 
		  Constructor
		*/
		ColorPicker();

		/*! 
		  Destructor
		*/
		~ColorPicker();

		/*! 
		  Type of color drawing.
		*/
		enum EDrawType {
			Random = 0,		//!< Colors picked in this manner are randomly selected.
			Ordered,		//!< Colors are picked from a list that is ordered by their position on a color spectrum.
			Preset			//!< Colors are picked from a special list of colors.
		};

		/*! 
		  Draws a color from the list of colors.
		*/
		QColor Draw(EDrawType type);

		/*! 
		  Resets the picking index to the beginning.
		*/
		void Reset();
	protected:
		/*! 
		  Initialize the list of colors.
		*/
		void Init();

		// Members
		vector<QColor> OrderedColorList_;	//!< List of ordered colors.
		vector<QColor> PresetColorList_;	//!< List of preset colors.
		int OrderedDrawIndex_;	//!< Index that keeps track of the next color to draw with the next call of an ordered draw.
		int PresetDrawIndex_;	//!< Index that keeps track of the next color to draw with the next call of a preset draw.
	};
}
#endif // !__ColorPicker_H__