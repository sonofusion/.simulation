// TaskRunner.h - Declaration for TaskRunner.
// Written By Jesse Z. Zhong
#ifndef __Task_Runner_H__
#define __Task_Runner_H__
#pragma region Includes
#include <QObject>
#include <QString>
#include <QRunnable>
#include <functional>
using namespace std;
#pragma endregion
namespace DataVisualizerGUI {
	/*! 
	  Specialized function object for file
	  reading tasks done by the DataManager.
	*/
	typedef function<void(int, const QString&)> Task;

	/*! 
	  A worker used to run file reading tasks.
	*/
	class TaskRunner : public QObject, public QRunnable {
		Q_OBJECT
	public:
		/*! 
		  Constructor

		*/
		TaskRunner(Task task = NULL, int val = 0, 
			const QString& str = "", QObject* parent = NULL);

		/*! 
		  Attempts to run an assigned task.
		*/
		void run();

	signals:
		/*! 
		  Alerts that the task has finished running.
		*/
		void Finished();

	protected:
		// Members
		Task Task_;		//! Task that needs to be run by the worker.

	private:
		// Function parameters.
		int Val_;
		QString Str_;
	};
}
#endif // !__Task_Runner_H__