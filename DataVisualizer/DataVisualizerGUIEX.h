// DataVisualizerGUIEX.h - Data Visualizer GUI class extension declaration.
// Written By Jesse Z. Zhong
//! \file
#ifndef __Data_Visualizer_GUI_EX_H__
#define __Data_Visualizer_GUI_EX_H__
#pragma region Includes
#include "stdafx.h"
#include "ui_DataVisualizerGUI.h"

#include "DataFile.h"
#include "G_DensityRadius.h"
#include "G_IonizationRadius.h"
#include "G_TempRadius.h"
#include "G_WallRadTime.h"
#include "GLParticleDist.h"
#include "GLEnergyCollision.h"

#include "MetadataName.h"
using namespace std;
using namespace Ui;
using namespace DataReader;
#pragma endregion
namespace DataVisualizerGUI {
	/*! 
	  Extends the primary GUI window to have the intended items and behaviors.

	  In this GUI class extension, data plots and a metadata table are added
	  and initialized so that they may be used for visualizing simulation data.
	*/
	class DataVisualizerGUIEX : public DataVisualizerGUIClass {
		#pragma region Constants
		#pragma region Plot
		//! The default width of individual plots.
		static const int DefaultWidth = 460;

		//! The padding used between the drawing of each plot.
		static const int PlotPadding = 40;

		//! The space between the left edge of 
		//! the plot area and the left most plots.
		static const int PlotLeftMargin = 4;

		//! The space between the top edge of
		//! the plot area and the top most plots.
		static const int PlotTopMargin = 4;

		//! Indicates how many graphs can be
		//! arranged in a row in the graph area.
		static const int PlotRowSize = 2;
		#pragma endregion
		#pragma region Metadata Table
		//! The max number of columns
		//! in the metadata table.
		static const int MDTableCols = 2;

		//! The row index for
		//! the metadata values.
		static const int MDValueCol = 1;

		//! The row index for the
		//! metadata display names.
		static const int MDNameCol = 0;
		#pragma endregion
		#pragma endregion
	public:
		// Plots
		G_IonizationRadius* IonRadius;		//!< Ionization level versus radius plot.
		G_DensityRadius* DensityRadius;		//!< Density versus radius plot.
		G_TempRadius* TempRadius;			//!< Temperature versus radius plot.
		G_WallRadTime* WallRadTime;			//!< Wall radius versus time plot.
		GLParticleDist* ParticleDist;		//!< 
		GLEnergyCollision* EnergyCollision;	//!< 

		/*! 
		  Destructor

		  Deallocates memory for the plots.
		*/
		~DataVisualizerGUIEX();
		
		/*! 
		  Initializes the UI elements.

		  Initializes all additional UI elements
		  that are added with the extension. This
		  method must only be called after setupUI().
		*/
		void InitializeUI(QMainWindow *parent);

		/*! 
		  Removes all data and plots from the graphs.
		*/
		void ClearAll();

		/*! 
		  Draws plots with the data loaded into each graph.
		*/
		void RefreshAll();

		/*! 
		  Changes the width of all the plots.
		*/
		void ChangePlotWidths(int width);
		 
		/*! 
		  Changes the displayed labels of the metadata
		  depending on the mode chosen: complex or simple.
		*/
		void ChangeLabelDisplayMode(bool mode);

		/*! 
		  Changes the value of a specific metadata table field.
		*/
		void ChangeMDValue(MDName::EName fieldName, double value);
		
		/*! 
		  Returns the width of the plots.
		*/
		int GetGraphWidth() const;
	protected:
		/*! 
		  Sets up plots with all the default settings.
		*/
		void SetupGraphs(QMainWindow *parent);

		/*! 
		  Initializes the metadata table with correct dimensions and names.
		*/
		void SetupMetadataTable();
		
		/*! 
		  Calculates appropriate graph height using the width and PHI.
		*/
		int Height(int width = DefaultWidth);
		
		// Members
		vector<Graph<DataFile>*> Plots_;	//!< Reference to all of the plots.
		vector<GraphGL*> PlotsGL_;			//!< 
		int GraphSize_;	//!< Stores the graph width to memory every time there is a size update.
	};
}
#endif // End : DataVisualizerGUIEX.h
