// QCPExtended.h - QCustomPlot class extension declaration.
// Written By Jesse Z. Zhong
#ifndef __QCP_Extended_H__
#define __QCP_Extended_H__
#pragma region Includes
#include "qcustomplot.h"
using namespace QCPlot;
#pragma endregion
namespace DataVisualizerGUI {

	/*! 
	  Extension of the QCustomPlot class that allows an
	  implementation of the class with different event handlers.
	  The purpose of this extension is to provide extra slots and
	  signals support that will not conflict with templating.
	*/
	class QCPExtended : public QCustomPlot {
		Q_OBJECT
	public:
		/*! 
		  Constructor
		*/
		QCPExtended(QWidget* parent = NULL);
	protected slots:
		/*! 
		  Shifts the viewport of the graph along the x axis.
		  NOTE: "value" changes the value of the lower bound.
		*/
		void ShiftViewport(int value);

	protected:
		/*! 
		  Sets the new upper and lower bounds and replots.
		  NOTE: This method should be called after adjusting bound member variables.
		*/
		void SetBounds();

		// Viewport bounds.
		double XLower_;		//!< 
		double XUpper_;		//!<
		double YLower_;		//!<
		double YUpper_;		//!<
		double FullXUpper_;		//!<
		double FullXLower_;		//!<
		double FullYUpper_;		//!<
		double FullYLower_;		//!<
	};
}
#endif // !__QCP_Extended_H__