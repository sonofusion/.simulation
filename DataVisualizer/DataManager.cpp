// DataManager.cpp - Data Manager Class Method Implementations
// Written By Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"

#include "DataManager.h"
#include "TaskRunner.h"
#include "Utilities.h"
#include "FileProgressDialog.h"

#include <QDir>
#include <QMutex>
#include <QThreadPool>
#include <QProgressDialog>
#include <QApplication>
using namespace DataVisualizerGUI;
namespace Util = Utilities;
#pragma endregion
#pragma region File Extensions
const QString DataManager::EXT_Snapshot = ".dat";
const QString DataManager::EXT_Save_Point = ".txt";
const QString DataManager::EXT_Collide_Energy = ".csv";
const QString DataManager::EXT_RPK_Data = ".dat";
const QString DataManager::KEY_Snapshot = "Snapshot";
const QString DataManager::KEY_Save_Point = "SavePoint";
const QString DataManager::KEY_Collide_Energy = "CollisionEnergy";
const QString DataManager::KEY_RPK_Data = "RP";
const QString DataManager::CHR_Asterisk = "*";
const QString DataManager::CHR_Slash = "/";
#pragma endregion
#pragma region Macros
// Using string builder when concatenating two QStrings.
#ifndef QT_USE_QSTRINGBUILDER
#define QT_USE_QSTRINGBUILDER
#endif // !QT_USE_QSTRINGBUILDER
#pragma endregion
#pragma region ThreadPool and File I/O
const int DataManager::MaxReadingTasks = 8;
#pragma endregion

DataManager::DataManager(QWidget* parent) : QObject(parent) {
	this->RPGuess2_ = DataRPK();
	this->DirPath_ = QString();
	this->Snapshot_ = vector<DataSnapshot>();
	this->Savepoint_ = vector<DataSavepoint>();
	this->CollideEnergy_ = vector<DataCollisionEnergy>();
	this->Parent_ = parent;
	this->MaxSets_ = 0;
}

DataManager::~DataManager() {

}

void DataManager::StoreData() {
	try {

		// Attempt to initialize directory access.
		QDir dir = QDir(this->DirPath_);
		if(!dir.exists()) {
			Util::Print("Path " + this->DirPath_.toStdString() 
				+ " could not be accessed or does not exist.");
			return;
		}

		// Clear the containers of previous data entries.
		this->Snapshot_.clear();
		this->Savepoint_.clear();
		this->CollideEnergy_.clear();

		// Get all the file names for each data type.
		QStringList rpkNames = dir.entryList(QStringList(KEY_RPK_Data
			+ CHR_Asterisk + EXT_RPK_Data), QDir::Files, QDir::Name);
		QStringList snapshotNames = dir.entryList(QStringList(KEY_Snapshot
			+ CHR_Asterisk + EXT_Snapshot), QDir::Files, QDir::Name);
		QStringList savePointNames = dir.entryList(QStringList(KEY_Save_Point
			+ CHR_Asterisk + EXT_Save_Point), QDir::Files, QDir::Name);
		QStringList collideEnergyNames = dir.entryList(QStringList(KEY_Collide_Energy
			+ CHR_Asterisk + EXT_Collide_Energy), QDir::Files, QDir::Name);

		// Sum up the number of files.
		// NOTE: Only one RPK should be used.
		int numOfFiles = ((rpkNames.size() > 0) ? 2 : rpkNames.size()) + snapshotNames.size() 
			+ savePointNames.size() + collideEnergyNames.size();
		Util::Print(QString().setNum(numOfFiles).toStdString() + " file(s) found.\n"
			+ QString().setNum(rpkNames.size()).toStdString() + " RP file(s) found.\n"
			+ QString().setNum(snapshotNames.size()).toStdString() + " Snapshot file(s) found.\n"
			+ QString().setNum(savePointNames.size()).toStdString() + " Save Point file(s) found.\n"
			+ QString().setNum(collideEnergyNames.size()).toStdString() + " Collision Energy file(s) found.");

		// Create a progress dialog to inform user of the file reading progress.
		FileProgressDialog* progressDialog = new FileProgressDialog(numOfFiles, this->Parent_);

		// Reserve space for data depending on the number of files found.
		int snapshotSize, savePointSize, collideEnergySize;
		this->Snapshot_.reserve(snapshotSize = snapshotNames.size());
		this->Savepoint_.reserve(savePointSize = savePointNames.size());
		this->CollideEnergy_.reserve(collideEnergySize = collideEnergyNames.size());

		// Append the slash to the directory path.
		QString dirPath = dir.path() + CHR_Slash;

		// Attempt to read in the first RPK file found.
		if(!rpkNames.empty()) {
			if(rpkNames.contains(Utilities::RPGuess2Name)) {
				this->RPGuess2_.Read((dirPath + Utilities::RPGuess2Name).toStdString());
				progressDialog->Increment();
			}
			if(rpkNames.contains(Utilities::RPActualName)) {
				this->RPActual_.Read((dirPath + Utilities::RPActualName).toStdString());
				progressDialog->Increment();
			}
		}

		// Make sure that there is at least one file for each type.
		if(snapshotNames.empty() || savePointNames.empty() || collideEnergyNames.empty()) {
			Util::Print("At least one of the file data types is missing from the directory."
				 "\nPlease ensure that you have Snapshot*.dat, SavePoint*.txt, and Collision Energy*.csv files in the directory.");
			return;
		}

		// Special case method used for converting a string to integer.
		// The method skips over all non-numerical characters until the
		// first digit is found. It proceeds to create a number out of the
		// digits until a non-numerical character is found or the end is met.
		auto strToInt = [] (const string& str)->int {

			// Define method used for checking
			// if a character is a numerical digit.
			auto isNum = [] (char c)->bool {
				return (('0' <= c) && 
					(c <= '9')) ? true : false;
			};

			// Define method used for convert 
			// a number character to an integer.
			auto charToInt = [] (char c)->int {
				return (int)(c - '0');
			};

			// Create an index and localize string length.
			int i = 0;
			int length = str.size();

			// Skip over all characters that aren't numbers.
			while((i < length) && !isNum(str[i]))
				i++;

			// Check if the index already 
			// reached the end of the string.
			if(i >= length)
				return 0;

			// Read in leftmost digit into the results.
			int result = charToInt(str[i]);

			// Iterate through the string until the 
			// length is hit or there is no more digits.
			for(i += 1; i < length; i++) {
				if(!isNum(str[i]))
					return result;

				// Bump all read in digits left and insert new digit.
				result = result * 10 + charToInt(str[i]);
			}

			// Return the final result.
			return result;
		};

		// Grab the last number values of each of the data file types.
		int snapshotLast = strToInt(snapshotNames[snapshotNames.size() - 1].toStdString().c_str());
		int savePointLast = strToInt(savePointNames[savePointNames.size() - 1].toStdString().c_str());
		int collisionEnergyLast = strToInt(collideEnergyNames[collideEnergyNames.size() - 1].toStdString().c_str());

		// Assign the last file number that will be used for when to stop reading in files.
		int lastFile = max(max(snapshotLast, savePointLast), collisionEnergyLast);

		// Resize the vectors and fill them with empty file data containers.
		this->Snapshot_.resize(lastFile, DataSnapshot());
		this->Savepoint_.resize(lastFile, DataSavepoint());
		this->CollideEnergy_.resize(lastFile, DataCollisionEnergy());

		// Create iterators for each of the file name lists.
		auto snapshotIter = snapshotNames.begin();
		auto snapshotIterLast = snapshotNames.end();
		auto savePointIter = savePointNames.begin();
		auto savePointIterLast = savePointNames.end();
		auto collisionEnergyIter = collideEnergyNames.begin();
		auto collisionEnergyIterLast = collideEnergyNames.end();

		// Create a value to record the number of sets successfully stored.
		int fileSetsStored = 0;

		// Create a thread pool for reading files.
		QThreadPool* threadPool = QThreadPool::globalInstance();

		// Set the maximum number of threads allowed at one time.
		threadPool->setMaxThreadCount(MaxReadingTasks);

		// Create an instance of QMutex for resource locking.
		QMutex mutex;

		// Iterate through the lists of files, attempt to read them in order.
		for(int i = 0; i < lastFile; i++) {

			// Snapshots
			// Check if the iterator has reached its end or not
			// and compare the number ID's in the file names with i.
			if((snapshotIter != snapshotIterLast)
				&& (i == strToInt(snapshotIter->toStdString()))) {

					// Create a new task runner with a reading and storing method.
					auto worker = new TaskRunner([&](int fileNumber, const QString& fileName) {

						// Parse the file for data.
						DataSnapshot dataSnapshot = DataSnapshot((dirPath + fileName).toStdString());

						// Store the newly read data.
						mutex.lock();
						this->Snapshot_[fileNumber] = dataSnapshot;
						mutex.unlock();

					}, i, *snapshotIter);

					// Increment the progress bar up.
					progressDialog->Increment();

					// Add a new working thread for reading a snapshot.
					threadPool->start(worker);

					// Iterate until the number is higher than i.
					// This is to skip over copies and dupes.
					while((snapshotIter != snapshotIterLast) 
						&& (i >= strToInt(snapshotIter->toStdString())))
						snapshotIter++;
			}

			// Save Points
			// Check if the iterator has reached its end or not
			// and compare the number ID's in the file names with i.
			if((savePointIter != savePointIterLast) 
				&& (i == strToInt(savePointIter->toStdString().c_str()))) {

					// Create a new task runner with a reading and storing method.
					auto worker = new TaskRunner([&](int fileNumber, const QString& fileName) {

						// Parse the file for data.
						DataSavepoint savepoint = DataSavepoint((dirPath + fileName).toStdString());

						// Store the newly read data.
						mutex.lock();
						this->Savepoint_[fileNumber] = savepoint;
						mutex.unlock();

					}, i, *savePointIter);

					// Increment the progress bar up.
					progressDialog->Increment();

					// Add a new working thread for reading a save point.
					threadPool->start(worker);

					// Iterate until the number is higher than i.
					// This is to skip over copies and dupes.
					while((savePointIter != savePointIterLast)
						&& (i >= strToInt(savePointIter->toStdString().c_str())))
						savePointIter++;
			}
		
			// Collision Energy
			// Check if the iterator has reached its end or not
			// and compare the number ID's in the file names with i.
			if((collisionEnergyIter != collisionEnergyIterLast)
				&& (i == strToInt(collisionEnergyIter->toStdString().c_str()))) {

					// Add a new working thread for reading a save point.
					auto worker = new TaskRunner([&](int fileNumber, const QString& fileName) {

						// Parse the file for data.
						DataCollisionEnergy colliEnergy = DataCollisionEnergy((dirPath + fileName).toStdString());

						// Store the newly read data.
						mutex.lock();
						this->CollideEnergy_[fileNumber] = colliEnergy;
						mutex.unlock();

					}, i, *collisionEnergyIter);

					// Increment the progress bar up.
					progressDialog->Increment();

					// Add a new working thread for reading a save point.
					threadPool->start(worker);

					// Iterate until the number is higher than i.
					// This is to skip over copies and dupes.
					while((collisionEnergyIter != collisionEnergyIterLast)
						&& (i >= strToInt(collisionEnergyIter->toStdString().c_str())))
						collisionEnergyIter++;
			}

			// Ensure that the progress dialog is updating.
			QCoreApplication::processEvents();

			// Iterate files stored.
			fileSetsStored++;
		}

		// Internalize the amount of sets stored to the data manager.
		this->MaxSets_ = fileSetsStored;

		// Wait for the thread pool to finish.
		threadPool->waitForDone();

		// Sets the progress bar to full.
		progressDialog->Complete();

	} catch(char* msg) {
		cout << msg << endl;
	}
}

// Returns the RP Guess 2 data.
// Returns a blank RPK if no data is found.
DataRPK& DataManager::GetRPGuess2Data() {
	return this->RPGuess2_;
}

// Returns the RP Actual data.
// Returns a blank RPK if no data is found.
DataRPK& DataManager::GetRPActual() {
	return this->RPActual_;
}

// Returns snapshot data at a given index.
// Returns a blank snapshot if no data is found.
DataSnapshot& DataManager::GetSnapshotData(int index) {
	if(this->CheckBounds<DataSnapshot>(index, this->Snapshot_))
		return this->Snapshot_[index];
	else
		throw "Index accessed is out of bounds.";
}

// Returns the save point data at a given index.
// Returns a blank save point data if no data is found.
DataSavepoint& DataManager::GetSavepointData(int index) {
	if(this->CheckBounds<DataSavepoint>(index, this->Savepoint_))
		return this->Savepoint_[index];
	else
		throw "Index accessed is out of bounds.";
}

// Returns the collision energy data at a given index.
// Returns a blank collision energy data if no data is found.
DataCollisionEnergy& DataManager::GetCollisionEnergyData(int index) {
	if(this->CheckBounds<DataCollisionEnergy>(index, this->CollideEnergy_))
		return this->CollideEnergy_[index];
	else
		throw "Index accessed is out of bounds.";
}

// Returns the collision energy data at a given index.
// Returns a blank collision energy data if no data is found.
template<typename T>
bool DataManager::CheckBounds(int index, vector<T> container) const {
	if(!((index >= 0) && (index < container.size())))
		return false;
	return true;
}

// Returns the maximum number of file sets of
// snapshots, save points, or collision energy.
int DataManager::GetMaxSets() const {
	return max(max(this->Snapshot_.size(), this->Savepoint_.size()),
		this->CollideEnergy_.size());
}

// File Directory
QString DataManager::GetDirPath() const {
	return this->DirPath_;
}
void DataManager::SetDirPath(const QString& dir) {
	this->DirPath_ = dir;
}