// GraphGL.h
// Written by Joshua Mellott-Lillie and Jesse Z. Zhong

#pragma once

#include <QtOpenGL/QtOpenGL>
#include <vector>

using namespace std;
namespace DataVisualizerGUI {

	/*! 
	This is the base class to display an OpenGL 
	application for the DataVisualizer.
	*/
	class GraphGL : public QGLWidget
	{
		Q_OBJECT

	public:
	
		/*! 
		 * Initializes the settings for the widget
		 * @param parent The parent container.
		 */
		GraphGL(QWidget *parent = 0);

		/*!
		 * Destructs the memory allocation for the object.
		 */
		~GraphGL(void);

	protected:
		/*!
		 * This is the override function for the 
		 * shared functions of the OpenGL based graphs.
		 */
		void paintGL(void);

		/*!
		 * This function is called after initialization
		 * and every subsequent resize of the graph.
		 * @param width The new width of the widget.
		 * @param height The new height of the widget.
		 */
		void resizeGL(int width, int height);

		/*!
		 * This function is to be overridden in the subclasses 
		 * and is required to perform all drawing operations.
		 */
		virtual void draw(void) = 0;

		/*!
		* This number is used by draw to display the glList.
		*/
		GLuint listNumber;

		map<int, GLuint> listMap;
	private:
		
	};
}