// StartupUIEX.h - Startup dialog extension class implementation.
// Written By Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "StartupUIEX.h"
using namespace std;
using namespace DataVisualizerGUI;
#pragma endregion

void StartupUIEX::InitializeUI(QDialog* parent) {
	this->InitializeText();
}

void StartupUIEX::InitializeText() {
	this->InstructionLabel->setText(QApplication::translate("StartUpDialog", 
		"Welcome to the Visualizer. \nPlease begin by selecting the data set directory.", 0));
}