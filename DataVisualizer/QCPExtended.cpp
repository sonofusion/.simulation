// QCPExtended.cpp - QCustomPlot class extension implementation.
// Written By Jesse Z. Zhong

#include "stdafx.h"
#include "QCPExtended.h"

using namespace DataVisualizerGUI;

QCPExtended::QCPExtended(QWidget* parent) 
	: QCustomPlot(parent) {

}

void QCPExtended::ShiftViewport(int value) {

	// Calculate the size of the viewport.
	double viewRange = this->XUpper_ - this->XLower_;

	// Get its magnitude, in case it is negative.
	viewRange = abs(viewRange);

	// Adjust the left bounds of the viewport.
	this->XLower_ = this->xAxis->pixelToCoord(value) + this->FullXLower_;

	// Adjust the right bounds of the viewport.
	this->XUpper_ = this->XLower_ + viewRange;

	// Sets in the new bounds and replots.
	this->SetBounds();
}

void QCPExtended::SetBounds() {

	// Set the view range for the x axis.
	this->xAxis->setRange(this->XLower_, this->XUpper_);

	// Set the view range for the y axis.
	this->yAxis->setRange(this->YLower_, this->YUpper_);

	// Redraw the plot.
	// NOTE: Replot is called instead of reloading all of the data.
	this->replot();
}