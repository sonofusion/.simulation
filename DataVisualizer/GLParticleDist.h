// GLParticleDist.h
// Written by Joshua Mellott-Lillie
#pragma once
#include "GraphGL.h"
#include "DataSavepoint.h"

using namespace DataReader;

namespace DataVisualizerGUI {
	/*!
	* This class takes in a DataSavepoint
	* and outputs an OpenGL plot of points.
	*/
	class GLParticleDist : public GraphGL
	{

	public:
		/*! 
		* Initializes the settings for the widget
		* @param parent The parent container.
		*/
		GLParticleDist(QWidget *parent = 0);

	
		// Destructs the memory allocation for the object.
		~GLParticleDist(void);
	
		/*!
		* Set the data to be read for this savepoint.
		* @param data The data to be read.
		*/
		void setData(DataSavepoint *data, int index);

		void changeDataSet(int index);

	protected:
	
		// This function draws the widget.
		void draw();

	private:
		static double maximalX;
		static double maximalY;
		static double maximalZ;
	};
}