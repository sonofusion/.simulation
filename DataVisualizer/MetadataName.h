// MetadataName.h - Metadata name class declaration.
// Written by Jesse Z. Zhong
#ifndef __Metadata_Name_H__
#define __Metadata_Name_H__
#pragma region Includes
#include "stdafx.h"
#include <QTextDocument>
using namespace std;
#pragma endregion
namespace DataVisualizerGUI {

	/*! 
	  Structure for storing the strings for representing metadata.
	*/
	struct MetadataName {
	public:
		/*! 
		  Names of the metadata.
		*/
		enum EName {
			MD_TIME,		//!< Time of the simulation.
			MD_WALL_RAD,	//!< The bubble's radius from the center to the wall.
			MD_WALL_VELO,	//!< The velocity of the bubble's compression or expansion.
			MD_MAX_PRESS,	//!< The maximum measured pressure from within the bubble.
			MD_AVG_PRESS,	//!< The average measured pressure from within the bubble.
			MD_MAX_TEMP,	//!< The maximum measured temperature from within the bubble.
			MD_AVG_TEMP,	//!< The average measured temperature from within the bubble.
			MD_MAX_ENERGY,	//!< The maximum measured energy from within the bubble.
			MD_AVG_ENERGY,	//!< The average measured energy from within the bubble.
			MD_FUSION_RATE,	//!< The rate that fusion is occurring in the bubble.
			MD_MAX_DENSE,	//!< The maximum measured density from within the bubble.
			MD_AVG_DENSE	//!< The average measured density from within the bubble.
		};

		/*! 
		  Constructor
		*/
		MetadataName(QString complex = QString(), 
			QString simple = QString());
		
		QString Complex;	//!< Full name of the item.
		QString Simple;		//!< Short hand name of the item.

		/*! 
		  Returns the display names if they are found in the hash table.
		*/
		static const MetadataName GetName(MetadataName::EName name);

		/*! 
		  Returns the size of the hash table.
		*/
		static const int GetSize();

		/*! 
		  Mapping of metadata names to their display strings.
		*/
		static const map<EName, MetadataName> DisplayNames;
		
	protected:
		/*! 
		  Initializes a map with the list of display names.
		*/
		static map<EName, MetadataName> InitDisplayNames();
	};

	/*! 
	  Alias for the metadata name struct.
	*/
	typedef MetadataName MDName;
}

#endif // !__Metadata_Name_H__