// StringResource.h - Program String Resource Sheet
// Written By Jesse Zhong
#include "stdafx.h"

// Enumerate All Strings
const QString SRC_START = "Play";
const QString SRC_STOP = "Stop";