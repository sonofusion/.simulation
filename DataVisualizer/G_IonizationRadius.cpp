// G_IonizationRadius.cpp - Ionization vs Radius plot class implementation.
// Written By Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "G_IonizationRadius.h"
#include "AtomicProperties.h"
using namespace DataVisualizerGUI;
namespace MDConst = MDSimulation::Constants;
#pragma endregion

G_IonizationRadius::G_IonizationRadius(QWidget* parent) 
	: Graph<DataSnapshot>(parent) {

}

void G_IonizationRadius::ProcessPoints(DataSnapshot& data, int upper) {

	// Localize the number of gas types.
	int numOfGases = data.GetGasTypes();

	// Perform storage for each gas type.
	for(int k = 0; k < numOfGases; k++) {

		// Create new plot data set.
		PlotData* plotData = new PlotData();

		// Set the name of the plot. The name of the plot is
		// determined by using the index, k, to look up the list
		// of simulation gas names in Constants::ELEMENT_NAMES.
		if((k >= 0) && (k < MDConst::ELEMENT_TYPES))
			plotData->Name = QString(MDConst::ELEMENT_NAMES[k].c_str());

		// Reserve the space in the arrays.
		int totalPoints = data.GetShellData().size();
		plotData->X.reserve(totalPoints);
		plotData->Y.reserve(totalPoints);

		// Iterate through all the points in the shell data
		// and begin storing relevant values in the graph.
		for(int i = 0; i < totalPoints; i++)
			plotData->AddPoint(data.GetShellData()[i].Radius,
			data.GetShellData()[i].Ionization[k]);

		// Register new plot data accordingly.
		this->RegisterPlot(plotData);
	}
}