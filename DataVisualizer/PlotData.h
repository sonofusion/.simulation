// PlotData.h - PlotData class declaration.
// Written By Jesse Z. Zhong
#ifndef __Plot_Data_H__
#define __Plot_Data_H__
#pragma region Includes
#include <QString>
#include <QVector>
#include <QColor>
#pragma endregion
namespace DataVisualizerGUI {

	/*! 
	  Structure for storing data for individual curves.
	*/
	class PlotData {

		//! Default curve and plot color.
		static const QColor DefaultColor;

	public:
		// Public members.
		QString Name;		//!< The name of the plot/curve.
		QVector<double> X;	//!< X-axis data points.
		QVector<double> Y;	//!< Y-axis data points.
		QColor Color;		//!< The expected color of the plot.

	public:
		/*! 
		  Constructor
		*/
		PlotData();

		/*! 
		  Overloads the assignment operator to allow for assigning/overwriting
		  the values of one plot data with that of another.
		*/
		PlotData& operator=(const PlotData& source);

		/*! 
		  Adds a single new point on both the x and y components.
		  The upper and lower ranges for both axis will be tested
		  and a new range will be assigned if the new data exceeds
		  the bounds of the existing data.

		  \param x is the value of the point along the x-axis.
		  \param y is the value of the point along the x-axis.
		  \see DataPlot::AddX, DataPlot::AddY
		*/
		void AddPoint(double x, double y);

		/*! 
		  Adds a single new point on the x component.
		  The upper and lower ranges for the x axis will be tested
		  and a new range will be assigned if the new data exceeds
		  the bounds of the existing data.

		  \param x is the value of the point along the x-axis.
		  \see DataPlot::AddPoint, DataPlot::AddY
		*/
		void AddX(double x);

		/*! 
		  Adds a single new point on the y component.
		  The upper and lower ranges for the y axis will be tested
		  and a new range will be assigned if the new data exceeds
		  the bounds of the existing data.

		  \param y is the value of the point along the y-axis.
		  \see DataPlot::AddPoint, DataPlot::AddX
		*/
		void AddY(double y);

#pragma region Accessors
		/*! 
		  Returns the assigned offset used to shift/translate
		  all data points along the x-axis.

		  \returns a double for the value of the offset.
		  \see PlotData::SetXYOffset, PlotData::SetXOffset, PlotData::SetYOffset
		*/
		inline double GetXOffset() const;

		/*! 
		  Returns the assigned offset used to shift/translate
		  all data points along the y-axis.

		  \returns a double for the value of the offset.
		*/
		inline double GetYOffset() const;

		/*! 
		  Assigns offsets for the x and y axis by which all points
		  along those axises will be shifted/translated by.

		  \param x is the value that all points on the x-axis will be shifted by.
		  \param y is the value that all points on the y-axis will be shifted by.
		  \see PlotData::SetXOffset, PlotData::SetYOffset
		*/
		void SetXYOffset(double x, double y);

		/*! 
		  Assigns an offset for the x axis by which all points
		  along the x-axis will be shifted/translated by.

		  \param val is the value that all points on the x-axis will be shifted by.
		  \see PlotData::SetYOffset, PlotData::SetXYOffset
		*/
		inline void SetXOffset(double val);

		/*! 
		  Assigns an offset for the y axis by which all points
		  along the y-axis will be shifted/translated by.

		  \param val is the value that all points on the y-axis will be shifted by.
		  \see PlotData::SetXYOffset, PlotData::SetXYOffset
		*/
		inline void SetYOffset(double val);

		/*! 
		  Returns the lowest value of all points along the x-axis.
		  The value returned has the x offset added to it.

		  \see DataPlot::SetXLower
		*/
		inline double GetXLower() const;

		/*! 
		  Returns the highest value of all points along the x-axis.
		  The value returned has the x offset added to it.

		  \see DataPlot::SetXUpper
		*/
		inline double GetXUpper() const;

		/*! 
		  Returns the lowest value of all points along the y-axis.
		  The value returned has the y offset added to it.

		  \see DataPlot::SetYLower
		*/
		inline double GetYLower() const;

		/*! 
		  Returns the highest value of all points along the y-axis.
		  The value returned has the y offset added to it.

		  \see DataPlot::SetYUpper
		*/
		inline double GetYUpper() const;

		/*! 
		  Assigns the lower bound of the x-axis. This value is used
		  to determine the leftmost bound of the plot's viewport.

		  \see DataPlot::GetXLower
		*/
		inline void SetXLower(double val);

		/*! 
		  Assigns the upper bound of the x-axis. This value is used
		  to determine the rightmost bound of the plot's viewport.

		  \see DataPlot::GetXUpper
		*/
		inline void SetXUpper(double val);

		/*! 
		  Assigns the lower bound of the y-axis. This value is used
		  to determine the bottommost bound of the plot's viewport.

		  \see DataPlot::GetYLower
		*/
		inline void SetYLower(double val);

		/*! 
		  Assigns the upper bound of the y-axis. This value is used
		  to determine the topmost bound of the plot's viewport.
		*/
		inline void SetYUpper(double val);
		
		/*! 
		  Returns a flag/boolean indicating whether the plot has been
		  selected by the user using the mouse.

		  \see DataPlot::SetSelected
		*/
		inline bool GetSelected() const;

		/*! 
		  Assigns a flag/boolean indicated whether the plot has been
		  selected by the user using the mouse.

		  \see DataPlot::GetSelected
		*/
		inline void SetSelected(bool selected);
#pragma endregion
	protected:
#pragma region Members
		double xOffset_;	//!< Value by which all values on the x-axis will be shifted by.
		double yOffset_;	//!< Value by which all values on the y-axis will be shifted by.
		double xLower_;		//!< The lower bound of the viewport along the x-axis.
		double xUpper_;		//!< The upper bound of the viewport along the x-axis.
		double yLower_;		//!< The lower bound of the viewport along the y-axis.
		double yUpper_;		//!< The upper bound of the viewport along the y-axis.
		bool Selected_;		//!< A flag indicating if the user has selected the plot with the mouse.
#pragma endregion
	};
}

#endif // !__Plot_Data_H__
