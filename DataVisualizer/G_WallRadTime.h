// G_WallRadTime.h - Wall Radius vs Time Graph plot class declaration.
// Written By Jesse Z. Zhong
#ifndef __Wall_Radius_vs_Time_H__
#define __Wall_Radius_vs_Time_H__
#pragma region Includes
#include "stdafx.h"
#include "Graph.h"
#include "DataRPK.h"
using namespace std;
using namespace DataReader;
#pragma endregion
namespace DataVisualizerGUI {
	/*! 
	  Implements the plotting of data from RPK files.
	*/
	class G_WallRadTime : public Graph<DataRPK> {
	public:
		/*! 
		  Constructor
		*/
		G_WallRadTime(QWidget* parent = NULL);

	private:
		/*! 
		  Adds a new set of data to a plot.
		  If \a upper is used the data used will be a subset of the 
		  original set with the range starting from zero to upper - 1.

		  \param upper indicates the upper bound of the subset.
		  A value of 0 means the whole set will be used.
		*/
		virtual void ProcessPoints(DataRPK& data, int upper);
	};
}
#endif // !__Wall_Radius_vs_Time_H__