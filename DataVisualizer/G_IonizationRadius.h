// G_IonizationRadius.h - Ionization vs Radius plot class declaration.
// Written By Jesse Z. Zhong
#ifndef __Ionization_vs_Radius_H__
#define __Ionization_vs_Radius_H__
#pragma region Includes
#include "Graph.h"
#include "DataSnapshot.h"
using namespace std;
using namespace DataReader;
#pragma endregion
namespace DataVisualizerGUI {
	/*! 
	  Implements shell ionization levels versus
	  shell radius plots using data from snapshots.
	*/
	class G_IonizationRadius : public Graph<DataSnapshot> {
	public:
		/*! 
		  Constructor
		*/
		G_IonizationRadius(QWidget* parent = NULL);

	private:
		/*! 
		  Adds a new set of data to a plot.
		*/
		virtual void ProcessPoints(DataSnapshot& data, int upper);
	};
}
#endif // !__Ionization_vs_Radius_H__