// StartupUIEX.h - Startup dialog extension class declaration.
// Written By Jesse Z. Zhong
#ifndef __Startup_UI_EX_H__
#define __Startup_UI_EX_H__
#pragma region Includes
#include "ui_StartupUI.h"
using namespace Ui;
#pragma endregion
namespace DataVisualizerGUI {
	/*! 
	  Extension of the startup dialog prompt.
	*/
	class StartupUIEX : public StartupUI {
	public:
		/*! 
		  Initialize the UI elements.
		*/
		void InitializeUI(QDialog* parent);
		
		/*! 
		  Initialize text fields.
		*/
		void InitializeText();
	};
}
#endif // !__Startup_UI_EX_H__
