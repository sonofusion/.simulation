// Graph.h - Graph extension template class implementation.
// Written By Jesse Z. Zhong
#ifndef __Graph_H__
#define __Graph_H__
#pragma region Includes
#include "stdafx.h"

#include <QRubberBand>
#include "QCPExtended.h"

#include "DataFile.h"
#include "PlotData.h"
#include "QCPAxisExtended.h"
#include "ColorPicker.h"
#include "Utilities.h"

using namespace std;
namespace Util = Utilities;
#pragma endregion
namespace DataVisualizerGUI {
 
	/*! 
	  Handles the basic requirements for handling
	  data and drawing said data to plots and curves.
	*/
	template<class T = DataFile>
	class Graph : public QCPExtended {
	#pragma region Constants
		//! Size of the scatter plot dots.
		static const float ScatterDotSize;

		//! Width of a bold pen.
		static const float BoldPenWidth;
	#pragma endregion
	public:
		/*! 
		  Constructor

		  Initializes all of the data members in the plot.

		  \param parent is the widget that claims ownership of this graph.
		*/
		Graph(QWidget* parent);

		/*! 
		  Destructor

		  Clears and deletes the internal graph.
		*/
		~Graph();

		/*! 
		  Sets up the dimensions and labels for graph.
		*/
		void InitializeGraph(const QRect& rect,
			string xAxisLabel, string yAxisLabel);

		/*! 
		  Processes and stores points when called.
		  NOTE: Only affects QCustomPlotEX.

		  If \a upper is used the data used will be a subset of the 
		  original set with the range starting from zero to upper - 1.

		  \param upper indicates the upper bound of the subset.
		  A value of 0 means the whole set will be used.
		*/
		void AddPlot(T& data, int upper = 0);

		/*! 
		  Removes a specific data set from the graph.

		  \param dataPoint is a pointer to the data set whose plot needs to be removed.
		*/
		void RemovePlot(PlotData* dataSet);
 
	#pragma region Draw Methods
		/*! 
		  Clears and redraws the graph.
		*/
		void Refresh();

		/*! 
		  Replot the graph area taking into account user interactions, such as zooming.
		*/
		void replot();

		/*! 
		  Resets the view port to the maximum viewable range.
		*/
		void ResetView();

		/*! 
		  Returns a state indicating if the viewport is zoomed or not.
		*/
		bool IsZoomed() const;

		/*! 
		  Calculates the new bounds after a plot has been changed.
		*/
		void AdjustRange();

		/*! 
		  Safely clears all the data from the graph.
		*/
		void Clear();

		/*! 
		  Wipes the full view port bounds.
		*/
		void ClearFullViewport();
	#pragma endregion
	#pragma region Accessors
		/*! 
		  Returns the pointer to the plot data.

		  \returns list<PlotData*>* that holds all the data sets
		  that the graph is in charge of plotting.
		*/
		list<PlotData*>* GetData();

		/*! 
		  Returns the lower bound on the x-axis.
		*/
		double GetXLower() const;

		/*! 
		  Returns the upper bound on the x-axis.
		*/
		double GetXUpper() const;

		/*! 
		  Returns the lower bound on the y-axis.
		*/
		double GetYLower() const;

		/*! 
		  Returns the upper bound on the y-axis.
		*/
		double GetYUpper() const;

		/*! 
		  Assigns the x-axis' lower bound with a new value.

		  \param val is the new value that will be assigned.
		*/
		void SetXLower(double val);

		/*! 
		  Assigns the x-axis' upper bound with a new value.

		  \param val is the new value that will be assigned.
		*/
		void SetXUpper(double val);

		/*! 
		  Assigns the y-axis' lower bound with a new value.

		  \param val is the new value that will be assigned.
		*/
		void SetYLower(double val);

		/*! 
		  Assigns the y-axis' upper bound with a new value.

		  \param val is the new value that will be assigned.
		*/
		void SetYUpper(double val);

		/*! 
		  
		*/
		double GetFullXLower() const;

		/*! 
		  
		*/
		double GetFullXUpper() const;

		/*! 
		  
		*/
		double GetFullYLower() const;

		/*! 
		  
		*/
		double GetFullYUpper() const;

		/*! 
		  
		*/
		void SetFullXLower(double val);

		/*! 
		  
		*/
		void SetFullXUpper(double val);

		/*! 
		  
		*/
		void SetFullYLower(double val);

		/*! 
		  
		*/
		void SetFullYUpper(double val);

		/*! 
		  Sets the full x-axis range that can be returned to at any time.
		*/
		void SetXAxis(double lower, double upper);

		/*! 
		  Sets the full y-axis range that can be returned to at any time.
		*/
		void SetYAxis(double lower, double upper);

		/*! 
		  Sets the labels for the x-axis.
		*/
		void SetXLabel(string label);

		/*! 
		  Sets the labels for the y-axis.
		*/
		void SetYLabel(string label);

		/*! 
		  Set or get the ability for the graph to 
		  resize itself along with changes in data.
		*/
		void SetAutomaticResize(bool automaticallyResizes);

		/*! 
		  Set if a plot will be boldfaced when selected.
		*/
		void SetBoldfacedSelect(bool isBold);
	#pragma endregion
	#pragma region Plot Data and Drawing: Methods
		/*! 
		  Reads, interprets, and stores data for graphing and returns pointer to data.

		  If \a upper is used the data used will be a subset of the 
		  original set with the range starting from zero to upper - 1.

		  \param upper indicates the upper bound of the subset.
		  A value of 0 means the whole set will be used.
		*/
		virtual void ProcessPoints(T& data, int upper = 0) = 0;

		/*! 
		  Stores new ranges, resets the plots, and stores new data into graph list.
		*/
		void RegisterPlot(PlotData* plotData);

		/*! 
		  Adds a new graph item to the graph list.
		*/
		void AddPlots();

	#pragma endregion
	#pragma region User Events: Methods
		/*! 
		  Override the mouse press event to incorporate extra behavior.
		*/
		void mousePressEvent(QMouseEvent *event);

		/*! 
		  Override the original mouse event to handle a different behavior.
		*/
		void mouseMoveEvent(QMouseEvent *event);

		/*! 
		  Override the mouse release event to incorporate extra behavior.
		*/
		void mouseReleaseEvent(QMouseEvent* event);

		/*! 
		  Override the scroll wheel event.
		*/
		void wheelEvent(QWheelEvent *event);
		
		/*! 
		  Override the key press event to handle special behavior.
		*/
		void keyPressEvent(QKeyEvent* event);

		/*! 
		  Override the key release event to handle special behavior.
		*/
		void keyReleaseEvent(QKeyEvent* event);

		/*! 
		  Returns the bounding area of plot view area.
		*/
		QRect BoundingArea() const;
	#pragma endregion
	#pragma region Plot Data and Drawing: Members
		list<PlotData*>* Data_;		//!< List of data sets for individual plots.
		map<QCPAbstractPlottable*, PlotData*> PlottableList_;	//!< List of the plots mapped with their data.
		ColorPicker ColorPicker_;	//!< Object used to choose plot colors from a preset list of colors.
		bool RescaleAxis_;			//!< Flag for indicating if the graph can resize with the change in data points.
		bool IsScatterPlot_;		//!< Changes the style of plot between curve and scatter.
	#pragma endregion
	#pragma region Zooming with Mouse Selection: Members
		QRubberBand* SelectionBox_;		//!< Object used to visually identify the area the user selects.
		QPoint Origin_;					//!< Point in the viewport that the user first clicks.
		bool IsMouseZooming_;			//!< Indicates if the user is making an area selection.
	#pragma endregion
	#pragma region Plot Axises: Members
		QCPAxisExtended* xAxisEX_;	//!< Extended x-axis object with scrollbar.
		QCPAxisExtended* yAxisEX_;	//!< Extended y-axis object with scrollbar.
		string XAxisLabel_;		//!< The label used to identify values on the x-axis.
		string YAxisLabel_;		//!< The label used to identify values on the y-axis.
	#pragma endregion
	#pragma region User Events: Members
		QCPAbstractPlottable* SelectedPlot_;	//!< Pointer to a previously selected plot.
		PlotData* SelectedPlotData_;			//!< Pointer to the plot data of a selected plot.
		bool IsPlotGrabbed_;		//!< Indicates that a plot is currently being selected.
		float DefaultPenWidth_;		//!< Stores the default pen width.
		bool IsSelectBoldfaced_;	//!< Indicates if selected plots should be drawn with boldface.
	#pragma endregion
	};

	template<class T>
	const float Graph<T>::ScatterDotSize = 0.1f;

	template<class T>
	const float Graph<T>::BoldPenWidth = 1.5f;

	template<class T>
	Graph<T>::Graph(QWidget* parent) 
		: QCPExtended(parent) {

			// Initialize graphing data.
			this->Data_ = new list<PlotData*>();
			this->XLower_ = 0;
			this->XUpper_ = 0;
			this->YLower_ = 0;
			this->YUpper_ = 0;
			this->RescaleAxis_ = false;
			this->IsScatterPlot_ = false;
			this->PlottableList_ = map<QCPAbstractPlottable*, PlotData*>();

			// Initialize event handling members.
			this->Origin_ = QPoint();
			this->FullXLower_ = 0;
			this->FullXUpper_ = 0;
			this->FullYLower_ = 0;
			this->FullYUpper_ = 0;
			this->IsMouseZooming_ = false;
			this->IsPlotGrabbed_ = false;
			this->SelectionBox_ = new QRubberBand(QRubberBand::Rectangle, this);
			this->SelectedPlot_ = NULL;
			this->SelectedPlotData_ = NULL;
			this->DefaultPenWidth_ = QPen().widthF();
			this->IsSelectBoldfaced_ = false;

			// Assign new axis extension type.
			delete this->xAxis;
			delete this->yAxis;
			this->xAxis = this->xAxisEX_ = new QCPAxisExtended(this, QCPAxis::atBottom);
			this->yAxis = this->yAxisEX_ =  new QCPAxisExtended(this, QCPAxis::atLeft);

			// Set focus level for handling keyboard input.
			this->setFocusPolicy(Qt::FocusPolicy::StrongFocus);

			// Connect the scroll bar of the x axis to the viewport moving event.
			connect(this->xAxisEX_->GetScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(ShiftViewport(int)));
	}

	template<class T>
	Graph<T>::~Graph() {

		// Delete the data for plots and curves.
		for(auto i = this->Data_->begin(), 
			j = this->Data_->end(); i != j; i++) {
				delete (*i);
				*i = NULL;
		}
		this->Data_->clear();
		delete this->Data_;
		this->Data_ = NULL;

		// Delete the curves and plots.
		// NOTE: Allow QCustomPlot to handle plottable deletion.
		// NOTE: The value is assumed to already
		// be deleted at this point. Refer to above.
		// 		for(auto i = this->PlottableList_.begin(), 
		// 			j = this->PlottableList_.end(); i != j; i++) {
		// 				if((*i).first != NULL) {
		// 					delete (*i).first;
		// 				}
		// 		}
		this->PlottableList_.clear();

		// Delete the selection box.
		if(this->SelectionBox_ != NULL) {
			delete this->SelectionBox_;
			this->SelectionBox_ = NULL;
		}
	}

	template<class T>
	void Graph<T>::InitializeGraph(const QRect& rect,
		string xAxisLabel, string yAxisLabel) {

			// Adjust the geometry.
			this->setGeometry(rect);

			// Set graph labels.
			this->SetXLabel(xAxisLabel);
			this->SetYLabel(yAxisLabel);

			// Redraw the graph.
			this->Refresh();
	}

	template<class T>
	void Graph<T>::AddPlot(T& data, int upper) {

		// Reset the full view port bounds.
		this->ClearFullViewport();

		// Processes data and stores them as data points.
		this->ProcessPoints(data, upper);
	}

	template<class T>
	void Graph<T>::RemovePlot(PlotData* dataSet) {

		// Remove the data from the list.
		this->Data_->remove(dataSet);

		// Refresh the graph.
		this->Refresh();
	}

	template<class T>
	void Graph<T>::Refresh() {

		// Clears old plots and curves.
		this->clearPlottables();
		this->clearGraphs();
		this->clearItems();
		this->PlottableList_.clear();

		// Sets the user interaction level.
		this->setInteraction(QCustomPlot::iSelectPlottables);

		// Add plots to the graph.
		this->AddPlots();

		// Set label names into the graph.
		this->xAxis->setLabel(this->XAxisLabel_.c_str());
		this->yAxis->setLabel(this->YAxisLabel_.c_str());

		// Set the graphs drawable ranges.
		this->xAxis->setRange(this->XLower_, this->XUpper_);
		this->yAxis->setRange(this->YLower_, this->YUpper_);

		// Set other graph settings.
		if(this->RescaleAxis_)
			this->rescaleAxes();

		// Redraw the graph.
		this->replot();
	}

	template<class T>
	void Graph<T>::replot() {

		// Changes the state of the axis.
		bool isZoomed;
		this->xAxisEX_->SetIsZoomed(isZoomed = this->IsZoomed());

		// Check if the viewport is zoomed in.
		if(isZoomed) {

			// Localize the scroll bar.
			QScrollBar* scrollBar = this->xAxisEX_->GetScrollBar();

			// Calculate the ranges.
			double viewRange = (this->XUpper_ - this->XLower_);
			double fullRange = (this->FullXUpper_ - this->FullXLower_);

			// Set the bounds of the scroll bar.
			scrollBar->setRange(0, (int)ceil(this->xAxis->coordToPixel(fullRange - viewRange)));

			// Calculate the current slider position in 
			// terms of the lower bound of the viewport.
			int sliderPos = (int)ceil(this->xAxis->coordToPixel(this->XLower_ - this->FullXLower_));

			// Corrects the position on the scroll bar.
			if((sliderPos <= scrollBar->maximum())
				&& (sliderPos >= scrollBar->minimum()))
				scrollBar->setSliderPosition(sliderPos);
		}

		// Call the base draw method.
		QCustomPlot::replot();
	}

	template<class T>
	void Graph<T>::ResetView() {

		// Reset the viewable range to the original.
		this->xAxis->setRange(this->XLower_ = this->FullXLower_,
			this->XUpper_ = this->FullXUpper_);
		this->yAxis->setRange(this->YLower_ = this->FullYLower_,
			this->YUpper_ = this->FullYUpper_);

		// Redraw the plot.
		// NOTE: Replot is called instead of reloading all of the data.
		this->replot();
	}

	template<class T>
	void Graph<T>::Clear() {

		// Iterate through the list of data.
		for(auto i = this->Data_->begin(), 
			j = this->Data_->end(); i != j; i++) {

				// Delete the plot data.
				delete *i;
				*i = NULL;
		}

		// Clear all the items from the data list.
		this->Data_->clear();
	}

	template<class T>
	void Graph<T>::ClearFullViewport() {
		this->FullXLower_ = 0;
		this->FullXUpper_ = 0;
		this->FullYLower_ = 0;
		this->FullYUpper_ = 0;
	}

	template<class T>
	bool Graph<T>::IsZoomed() const {
		if((this->FullXLower_ != this->XLower_)
			|| (this->FullXUpper_ != this->XUpper_)
			|| (this->FullYLower_ != this->YLower_)
			|| (this->FullYUpper_ != this->YUpper_))
			return true;
		return false;
	}

	template<class T>
	void Graph<T>::AdjustRange() {

		// Declare intermediate variables.
		// NOTE: The initialization values are 0.
		// This means no min should be above 0 and no
		// max should be below 0. i.e. There is a purpose.
		double xLower(0), xUpper(0), yLower(0), yUpper(0);

		// Iterate through and rejudge maximums and minimums.
		for(auto i = this->Data_->begin(), 
			j = this->Data_->end(); i != j; i++) {

				// Localize for consistency purposes.
				double iXLower = (*i)->GetXLower();
				double iXUpper = (*i)->GetXUpper();
				double iYLower = (*i)->GetYLower();
				double iYUpper = (*i)->GetYUpper();

				// Perform comparisons.
				xLower = (xLower > iXLower) ? iXLower : xLower;
				xUpper = (xUpper < iXUpper) ? iXUpper : xUpper;
				yLower = (yLower > iYLower) ? iYLower : yLower;
				yUpper = (yUpper < iYUpper) ? iYUpper : yUpper;
		}

		// Reassign the maximums and minimums.
		this->FullXLower_ = xLower;
		this->FullXUpper_ = xUpper;
		this->FullYLower_ = yLower;
		this->FullYUpper_ = yUpper;
	}

	template<class T>
	list<PlotData*>* Graph<T>::GetData() { 
		return this->Data_; 
	}

	template<class T>
	double Graph<T>::GetXLower() const { 
		return this->XLower_; 
	}

	template<class T>
	double Graph<T>::GetXUpper() const { 
		return this->XUpper_; 
	}

	template<class T>
	double Graph<T>::GetYLower() const { 
		return this->YLower_; 
	}

	template<class T>
	double Graph<T>::GetYUpper() const { 
		return this->YUpper_; 
	}

	template<class T>
	void Graph<T>::SetXLower(double val) { 
		this->XLower_ = val; 
	}

	template<class T>
	void Graph<T>::SetXUpper(double val) { 
		this->XUpper_ = val; 
	}

	template<class T>
	void Graph<T>::SetYLower(double val) { 
		this->YLower_ = val; 
	}

	template<class T>
	void Graph<T>::SetYUpper(double val) { 
		this->YUpper_ = val; 
	}

	template<class T>
	double Graph<T>::GetFullXLower() const { 
		return this->FullXLower_; 
	}

	template<class T>
	double Graph<T>::GetFullXUpper() const { 
		return this->FullXUpper_; 
	}

	template<class T>
	double Graph<T>::GetFullYLower() const { 
		return this->FullYLower_; 
	}

	template<class T>
	double Graph<T>::GetFullYUpper() const { 
		return this->FullYUpper_; 
	}

	template<class T>
	void Graph<T>::SetFullXLower(double val) { 
		this->FullXLower_ = val; 
	}

	template<class T>
	void Graph<T>::SetFullXUpper(double val) { 
		this->FullXUpper_ = val; 
	}

	template<class T>
	void Graph<T>::SetFullYLower(double val) { 
		this->FullYLower_ = val; 
	}

	template<class T>
	void Graph<T>::SetFullYUpper(double val) { 
		this->FullYUpper_ = val; 
	}

	template<class T>
	void Graph<T>::SetXAxis(double lower, double upper) {
		this->xAxis->setRange(lower, upper);
		this->FullXLower_ = lower;
		this->FullXUpper_ = upper;
	}

	template<class T>
	void Graph<T>::SetYAxis(double lower, double upper) {
		this->yAxis->setRange(lower, upper);
		this->FullYLower_ = lower;
		this->FullYUpper_ = upper;
	}

	template<class T>
	void Graph<T>::SetXLabel(string label) {
		this->XAxisLabel_ = label;
	}

	template<class T>
	void Graph<T>::SetYLabel(string label) {
		this->YAxisLabel_ = label;
	}

	template<class T>
	void Graph<T>::SetAutomaticResize(bool automaticallyResizes) {
		this->RescaleAxis_ = automaticallyResizes;
	}

	template<class T>
	void Graph<T>::RegisterPlot(PlotData* plotData) {

		// Store the data into the data list.
		this->GetData()->push_back(plotData);

		// Recalculate the view port ranges.
		this->AdjustRange();

		// Reset the view port with the new max and min.
		this->ResetView();
	}

	template<class T>
	void Graph<T>::AddPlots() {

		// Reset the list of the colors.
		this->ColorPicker_.Reset();

		// Keep track of the number of plots that have names.
		int numOfNamedPlots = 0;

		// Iterate through list to add plots.
		for(auto i = this->Data_->begin(), 
			j = this->Data_->end(); i != j; i++) {

				// Create an abstract plot used for 
				// storing and adding new plots and curves.
				QCPAbstractPlottable* tempPlottable;

				// Pick a color for the pen used in drawing the
				// plot and the text for the plot's legend entry.
				QColor plotColor = this->ColorPicker_.Draw(ColorPicker::Preset);
				QPen drawPen = QPen(plotColor);

				// Check if the graph type is a scatter plot.
				if(this->IsScatterPlot_) {

					// Create a new scatter plot.
					QCPGraph *ScatterGraph = new QCPGraph(this->xAxis, 
						this->yAxis);

					// Add the new plot as a new entry in the graph.
					this->addPlottable(ScatterGraph);
					ScatterGraph->setData((*i)->X, (*i)->Y);

					// Adjust the colors of the new plot.
					drawPen.setWidthF(((*i)->GetSelected()) ? BoldPenWidth : this->DefaultPenWidth_);
					ScatterGraph->setPen(drawPen);
					ScatterGraph->setScatterStyle(QCP::ssDisc);
					ScatterGraph->setScatterSize(ScatterDotSize);
					ScatterGraph->setLineStyle(QCPGraph::lsNone);

					// Reference the new plot to be 
					// stored in the list of graphs later.
					tempPlottable = ScatterGraph;

				} else {

					// Create a new curve.
					QCPCurve *CurveGraph = new QCPCurve(this->xAxis, 
						this->yAxis);

					// Add the new curve as a new entry in the graph.
					this->addPlottable(CurveGraph);
					CurveGraph->setData((*i)->X, (*i)->Y);

					// Adjust the colors of the new curve.
					drawPen.setWidthF(((*i)->GetSelected()) ? BoldPenWidth : this->DefaultPenWidth_);
					CurveGraph->setPen(drawPen);

					// Reference the new curve to be
					// stored in the list of graphs later.
					tempPlottable = CurveGraph;
				}

				// Check if the plot data has a name for the plot.
				if((*i)->Name != "") {

					// Set the name of the plottable.
					tempPlottable->setName((*i)->Name);

					// Increment the number of plots if this plot has a name.
					numOfNamedPlots++;
				}

				// Add new curve or plot to the list of plots.
				this->PlottableList_.insert(pair<QCPAbstractPlottable*, 
					PlotData*>(tempPlottable, (*i)));
		}

		// If there are named plots, show the legend.
		this->legend->setVisible((numOfNamedPlots > 0) ? true : false);
	}

	template<class T>
	void Graph<T>::SetBoldfacedSelect(bool isBold) { 
		this->IsSelectBoldfaced_ = isBold; 
	}

	template<class T>
	QRect Graph<T>::BoundingArea() const {

		// Create a bounding area out of the graph margins.
		QRect plotGeo = this->geometry();
		return QRect(this->marginLeft(), this->marginTop(), 
			(plotGeo.width()/* - this->marginLeft() - this->marginRight()*/), 
			(plotGeo.height() - this->marginTop() - this->marginBottom()));
	}

	template<class T>
	void Graph<T>::mousePressEvent(QMouseEvent *event) {
		// Plot Selection Zooming
		// Check if the button pressed is the left mouse button.
		if(event->button() == Qt::MouseButton::LeftButton) {

			// Pull the starting position of the mouse.
			this->Origin_ = event->pos();

			// Change the dimensions of the selection box.
			if(Util::CheckBoundaries(this->Origin_, this->BoundingArea())) {
				this->SelectionBox_->setGeometry(QRect(this->Origin_, QSize()));
				this->SelectionBox_->show();

				// Change the state.
				this->IsMouseZooming_ = true;
			}

			// Set the mouse dragging flag to true.
			this->mDragging = true;
		}
	
		// Reset the Viewport to Full View
		// Check if right mouse button was pressed.
		if(event->button() == Qt::MouseButton::RightButton) {

			// Resets the viewport to full view.
			this->ResetView();
		}

		// Call base event.
		QWidget::mousePressEvent(event);
	}

	template<class T>
	void Graph<T>::mouseMoveEvent(QMouseEvent *event) {

		// Emit the mouse move event.
		emit mouseMove(event);

		// Check if the mouse is clicked and dragging.
		if(this->mDragging) {
			// Controls the Viewport Zooming
			// Check if the mouse is invoking the zoom state.
			if(this->IsMouseZooming_) {

				// Localize the position.
				QPoint currPos = event->pos();

				// Localize the bounding area of the plot.
				QRect boundingArea = this->BoundingArea();

				// Update the dimensions of the selection box.
				this->SelectionBox_->setGeometry(QRect(this->Origin_,
					Util::CheckPoint(currPos, boundingArea)).normalized());

			} else {

				// Controls Plot Selection and Moving
				// Check if a plot is currently selected.
				if(this->IsPlotGrabbed_ && this->SelectedPlotData_) {

					// Localize the position of the mouse.
					QPoint mp = event->pos();

					// Calculate the offsets.
					double xOffset = this->xAxis->pixelToCoord(mp.x()) 
						- this->xAxis->pixelToCoord(this->Origin_.x());
					double yOffset = this->yAxis->pixelToCoord(mp.y()) 
						- this->yAxis->pixelToCoord(this->Origin_.y());

					// Offset the data.
					this->SelectedPlotData_->SetXYOffset(this->SelectedPlotData_->GetXOffset() 
						+ xOffset, this->SelectedPlotData_->GetYOffset() + yOffset);

					// Set the new lower and upper bounds.
					this->AdjustRange();

					// Redraw the plots.
					this->Refresh();
				}

				// Save the current position of the mouse.
				this->Origin_ = event->pos();
			}
		}

		// Call base widget event handler.
		QWidget::mouseMoveEvent(event);
	}

	template<class T>
	void Graph<T>::mouseReleaseEvent(QMouseEvent* event) {

		// Check if the button released was the left mouse button.
		if(event->button() == Qt::MouseButton::LeftButton) {

			// Check if the mouse has invoked zooming.
			if(this->IsMouseZooming_) {

				// Localize the geometry of the selection box.
				QRect selectArea = this->SelectionBox_->geometry();

				// Set the view range for the x axis.
				this->XLower_ = this->xAxis->pixelToCoord(selectArea.x());
				this->XUpper_ = this->xAxis->pixelToCoord(selectArea.x() + selectArea.width());

				// Set the view range for the y axis.
				this->YLower_ = this->yAxis->pixelToCoord(selectArea.y());
				this->YUpper_ = this->yAxis->pixelToCoord(selectArea.y() + selectArea.height());

				// Adjust the viewport and replot.
				this->SetBounds();

				// Stop drawing the selection box.
				this->SelectionBox_->hide();

				// Clear the state.
				this->IsMouseZooming_ = false;
			} 

			// Set the selectable state to false.
			this->IsPlotGrabbed_ = false;

			// Set the mouse dragging flag to false.
			this->mDragging = false;

			this->replot();
		}

		// Call base event handler.
		QWidget::mouseReleaseEvent(event);
	}

	template<class T>
	void Graph<T>::wheelEvent(QWheelEvent *event) {
		emit mouseWheel(event);

		// Call base event handler.
		QWidget::wheelEvent(event);
	}

	template<class T>
	void Graph<T>::keyPressEvent(QKeyEvent* event) {

		// Call base event handler.
		QWidget::keyPressEvent(event);
	}

	template<class T>
	void Graph<T>::keyReleaseEvent(QKeyEvent* event) {

		// Call base event handler.
		QWidget::keyReleaseEvent(event);
	}
}
#endif // !__Graph_H__