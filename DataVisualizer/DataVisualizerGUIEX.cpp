// DataVisualizerGUIEX.cpp - Data Visualizer GUI class extension implementation.
// Written By Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "DataVisualizerGUIEX.h"

#include "StringResource.h"

#include "Utilities.h"
using namespace DataVisualizerGUI;
#pragma endregion

DataVisualizerGUIEX::~DataVisualizerGUIEX() {
	delete this->IonRadius;
	delete this->DensityRadius;
	delete this->TempRadius;
	delete this->WallRadTime;
	delete this->ParticleDist;
	delete this->EnergyCollision;
}

void DataVisualizerGUIEX::InitializeUI(QMainWindow *parent) {

	// Initialize graph size.
	this->GraphSize_ = DefaultWidth;

	// Setup the graphs.
	this->SetupGraphs(parent);

	// Set up the metadata table.
	this->SetupMetadataTable();
}

void DataVisualizerGUIEX::ClearAll() {
	for(int i = 0, j = this->Plots_.size(); i < j; i++)
		this->Plots_[i]->Clear();
}

void DataVisualizerGUIEX::RefreshAll() {
	for(int i = 0, j = this->Plots_.size(); i < j; i++)
		this->Plots_[i]->Refresh();
}

void DataVisualizerGUIEX::ChangePlotWidths(int width) {

	// Check if the passed width is a non negative number.
	if(width < 0)
		throw "Graph width cannot be a negative number";

	// Change the height and width of all of the plots.
	int height = this->Height(width);
	int numOfPlots = this->Plots_.size();
	int numOfPlotsGL = this->PlotsGL_.size();
	int totNumOfPlots = numOfPlots + numOfPlotsGL;
	for(int i = 0; i < totNumOfPlots; i++) {

		// Calculate the indices in the plot area
		// "table" where this plot will be drawn in.
		int rowIndex = i / PlotRowSize;
		int colIndex = i % PlotRowSize;

		auto calcGeo = [&] () {
			return QRect(
				(PlotLeftMargin + (colIndex * (PlotPadding + width))),
				(PlotTopMargin + (rowIndex * (PlotPadding + height))), 
				width, height);
		};

		// Assign the newly calculated geometry.
		if(i < numOfPlots)
			this->Plots_[i]->setGeometry(calcGeo());   // NOTE: Plots aren't QObjects
		else if((i - numOfPlots) >= 0)
			this->PlotsGL_[i - numOfPlots]->setGeometry(calcGeo());
	}

	// Recalculate the dimensions of the scroll area
	// according to the number of plots in the area.
	this->PlotArea->setGeometry(0, 0,
		(PlotLeftMargin + (PlotPadding + width) * PlotRowSize),
		(PlotTopMargin + (ceil(totNumOfPlots / PlotRowSize)) * height + PlotPadding) + 60);

	// Assign the size shared by the plots.
	this->GraphSize_ = width;
}

void DataVisualizerGUIEX::ChangeLabelDisplayMode(bool mode) {

	// Ensure that the table is already filled
	// out before proceeding to change its contents.
	int numOfRows = this->MetadataTable->rowCount();
	if(numOfRows != MDName::DisplayNames.size()) {

		// Send a message to console (debug).
		Util::Print("Error: Metadata table was not populated properly."
			"\nDisplay names for metadata cannot be changed.");
		return;
	}

	// Iterate through the table to change the names.
	for(int i = 0; i < numOfRows; i++) {

		// Create a new label with the changed name.
		QLabel* tableItem = new QLabel((mode) 
			? MDName::GetName((MDName::EName)i).Simple 
			: MDName::GetName((MDName::EName)i).Complex);

		// Set the new label into the corresponding cell.
		this->MetadataTable->setCellWidget(i, MDNameCol, tableItem);
	}

	// Resizes the columns to show all of the contents.
	this->MetadataTable->resizeColumnsToContents();
}

void DataVisualizerGUIEX::ChangeMDValue(MDName::EName fieldName, double value) {

	// Check bounds to ensure the accessing
	// field exists within the current table.
	int numOfRows = this->MetadataTable->rowCount();
	if((fieldName < 0) || (fieldName >= numOfRows)) {

		// Send a message to console (debug).
		string msg = "Error: The index used does not fall within bounds of the metadata table.\n";
		msg += to_string(fieldName) + " is not within the range of 0 and " + to_string(numOfRows);
		Util::Print(msg);
		return;
	}

	// Create a new label with the value stored in it.
	QLabel* valLabel = new QLabel(QString().setNum(value));

	// Set the value label corresponding to the name/index.
	this->MetadataTable->setCellWidget(fieldName, MDValueCol, valLabel);
}

int DataVisualizerGUIEX::GetGraphWidth() const {
	return this->GraphSize_;
}

void DataVisualizerGUIEX::SetupGraphs(QMainWindow *parent) {

	// Initialize the plots.
	this->Plots_ = vector<Graph<DataFile>*>();
	this->PlotsGL_ = vector<GraphGL*>();
	this->Plots_.push_back((Graph<DataFile>*)(this->IonRadius = new G_IonizationRadius(this->PlotArea)));
	this->Plots_.push_back((Graph<DataFile>*)(this->DensityRadius = new G_DensityRadius(this->PlotArea)));
	this->Plots_.push_back((Graph<DataFile>*)(this->TempRadius = new G_TempRadius(this->PlotArea)));
	this->Plots_.push_back((Graph<DataFile>*)(this->WallRadTime = new G_WallRadTime(this->PlotArea)));
	this->PlotsGL_.push_back((GraphGL*)(this->ParticleDist = new GLParticleDist(this->PlotArea)));
	this->PlotsGL_.push_back((GraphGL*)(this->EnergyCollision = new GLEnergyCollision(this->PlotArea)));
	this->ParticleDist->setObjectName(QString::fromUtf8("gl1"));
	this->EnergyCollision->setObjectName(QString::fromUtf8("gl2"));

	// Initialize the plots with label names.
	this->IonRadius->InitializeGraph(QRect(), "Radius", "Ionization %");
	this->DensityRadius->InitializeGraph(QRect(), "Radius", "Density");
	this->TempRadius->InitializeGraph(QRect(), "Radius", "Temperature");
	this->WallRadTime->InitializeGraph(QRect(), "Time", "Wall Radius");
	
	// Initialize shared information for all of the plots.
	for(int i = 0, j = this->Plots_.size(); i < j; i++) {

		// Change the size policy of the plots.
		this->Plots_[i]->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	}

	// Change the widths of all the plots.
	this->ChangePlotWidths(this->GraphSize_);
}

void DataVisualizerGUIEX::SetupMetadataTable() {

	// Initialize the table with the correct behaviors.
	this->MetadataTable->setSelectionBehavior(QAbstractItemView::SelectRows);
	this->MetadataTable->verticalHeader()->setVisible(false);
	this->MetadataTable->setHorizontalScrollMode(QAbstractItemView::ScrollMode::ScrollPerPixel);

	// Iterate through the list of
	// names to populate the table.
	int row = 0;
	for(auto it = MDName::DisplayNames.begin(), 
		end = MDName::DisplayNames.end(); it != end; it++) {

			// Insert a new row into the table.
			this->MetadataTable->insertRow(row = this->MetadataTable->rowCount());

			// Fill in the columns in the row.
			for(int i = 0; i < MDTableCols; i++) {

				// Create a new item; Fills the item with
				// the display name if the indices match.
				QLabel* tableItem = 
					new QLabel((i == MDNameCol) 
					? it->second.Complex : QString());

				// Add the new cell to the table.
				this->MetadataTable->setCellWidget(row, i, tableItem);
			}
	}

	// Resizes the columns to show all of the contents.
	this->MetadataTable->resizeColumnsToContents();
}

int DataVisualizerGUIEX::Height(int width) {
	return (int) (width / Util::PHI);
}