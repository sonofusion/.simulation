// G_TempRadius.h - Temperature vs Radius plot class declaration.
// Written By Jesse Z. Zhong
#ifndef __Temperature_vs_Radius_H__
#define __Temperature_vs_Radius_H__
#pragma region Includes
#include "stdafx.h"
#include "Graph.h"
#include "DataSnapshot.h"
using namespace std;
using namespace DataReader;
#pragma endregion
namespace DataVisualizerGUI {
	/*! 
	  Implements shell temperatures versus shell
	  radius plots using data from snapshots.
	*/
	class G_TempRadius : public Graph<DataSnapshot> {
	public:
		/*! 
		  Constructor
		*/
		G_TempRadius(QWidget* parent = NULL);

	private:
		/*! 
		  Adds a new set of data to a plot.
		*/
		virtual void ProcessPoints(DataSnapshot& data, int upper);
	};
}
#endif // !__Temperature_vs_Radius_H__