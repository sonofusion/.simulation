// GLEnergyCollision.cpp
// Written by Joshua Mellott-Lillie
#include "stdafx.h"
#include "GLEnergyCollision.h"
using namespace DataVisualizerGUI;

GLEnergyCollision::GLEnergyCollision(QWidget *parent)
	: GraphGL(parent) {
}


GLEnergyCollision::~GLEnergyCollision(void) {

}

void GLEnergyCollision::changeDataSet(int index)
{
	this->listNumber = this->listMap[index];
	updateGL();
}

void GLEnergyCollision::setData(DataCollisionEnergy *dataCollisionEnergy, int index) {
	this->makeCurrent();
	GLuint newList = glGenLists(1);

	vector<CollisionData> data = dataCollisionEnergy->GetData();

	if(data.size() == 0)
	{
		glNewList(newList, GL_COMPILE);
		glEndList();
		this->listMap[index] = newList;
		return;
	}

	glNewList(newList, GL_COMPILE);

	int scale = 100;

		const int maxTest = 4;

	double minY[maxTest];
	double maxY[maxTest];
	double minZ[maxTest];
	double maxZ[maxTest];
	double maxTemp[maxTest];
	double minTemp[maxTest];

	for(int i = 0; i < maxTest; i++)
	{
		minY[i] = maxY[i] = data[0].Position.Y;
		minZ[i] = maxZ[i] = data[0].Position.Z;
		minTemp[i] = maxTemp[i] = data[0].Temperature;
	}

	foreach(CollisionData cd, data)
	{
		double pushMaxY = cd.Position.Y;
		double pushMaxZ = cd.Position.Z;
		double pushTemp = cd.Temperature;
		for(int i = 0; i < maxTest; i++)
		{
			if(pushMaxY > maxY[i])
			{
				double temp = maxY[i];
				maxY[i] = pushMaxY;
				pushMaxY = temp;
			}

			if(pushMaxZ > maxZ[i])
			{
				double temp = maxZ[i];
				maxZ[i] = pushMaxZ;
				pushMaxZ = temp;
			}

			if(pushTemp > maxTemp[i])
			{
				double temp = maxTemp[i];
				maxTemp[i] = pushTemp;
				pushTemp = temp;
			}
		}

		double pushMinY = cd.Position.Y;
		double pushMinZ = cd.Position.Z;
		double pushMinTemp = cd.Temperature;
		for(int i = maxTest - 1; i >= 0; i--)
		{
			if(pushMinY < minY[i])
			{
				double temp = minY[i];
				minY[i] = pushMinY;
				pushMinY = temp;
			}

			if(pushMinZ < minZ[i])
			{
				double temp = minZ[i];
				minZ[i] = pushMinZ;
				pushMinZ = temp;
			}

			if(pushMinTemp > minTemp[i])
			{
				double temp = minTemp[i];
				minTemp[i] = pushMinTemp;
				pushMinTemp = temp;
			}
		}

	}

	double range = maxTemp[maxTest - 1] - minTemp[maxTest - 1];
	
	double yRange = maxY[maxTest - 1] - minY[maxTest - 1];
	yRange /= scale * 2;
	double zRange = maxZ[maxTest - 1] - minZ[maxTest - 1];
	zRange /= scale;

	glBegin(GL_POINTS);

	foreach(CollisionData cd, data)
	{
		double tempColor = (cd.Temperature - minTemp[maxTest - 1]) / range;

		glColor4f(tempColor, 0, 1 - tempColor, 0.5f);

		double posY = (cd.Position.Y - minY[maxTest - 1]) / yRange;
		double posZ = (cd.Position.Z - minZ[maxTest - 1]) / zRange;

		glVertex2d(posY - 100.0f, posZ);
	}

	glEnd();
	glEndList();

	this->listMap[index] = newList;
}

void GLEnergyCollision::draw() {
	/* Clear buffers */
	glClear(GL_COLOR_BUFFER_BIT);
	glCallList(this->listNumber);

	glPushMatrix();
    glColor4f(0.2, 0.2, 0.2, 1.0);
    renderText(90, 10, 0, "Collision Energies");
    glPopMatrix();
}