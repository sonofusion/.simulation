// G_DensityRadius.cpp - Density vs Radius plot class implementation.
// Written By Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "G_DensityRadius.h"
using namespace DataVisualizerGUI;
#pragma endregion

G_DensityRadius::G_DensityRadius(QWidget* parent) 
	: Graph<DataSnapshot>(parent) {

}

void G_DensityRadius::ProcessPoints(DataSnapshot& data, int upper) {

	// Create a new plot data set.
	PlotData* plotData = new PlotData();

	// Reserve space in the arrays.
	int totalPoints = data.GetShellData().size();
	plotData->X.reserve(totalPoints);
	plotData->Y.reserve(totalPoints);

	// Iterate through all the points in the shell data
	// and begin storing relevant values in the graph.
	for(int i = 0; i < totalPoints; i++)
		plotData->AddPoint(data.GetShellData()[i].Radius,
		data.GetShellData()[i].Density);

	// Register the plot accordingly.
	this->RegisterPlot(plotData);
}