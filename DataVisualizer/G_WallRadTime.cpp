// G_WallRadTime.h - Wall Radius vs Time Graph plot class implementation.
// Written By Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "G_WallRadTime.h"
#include "Utilities.h"
#include <QFileInfo>
using namespace DataVisualizerGUI;
#pragma endregion

G_WallRadTime::G_WallRadTime(QWidget* parent) 
	: Graph<DataRPK>(parent) {

}

void G_WallRadTime::ProcessPoints(DataRPK& data, int upper) {

	// Create new plot data set.
	PlotData* plotData = new PlotData();

	// Check the file name that the data set came
	// from to determine the name of the plot.
	QString fileName = QFileInfo(data.GetFileName().c_str()).fileName();
	if(fileName == Utilities::RPGuess2Name)
		plotData->Name = Utilities::RPGuess2Name;
	else if(fileName == Utilities::RPActualName)
		plotData->Name = Utilities::RPActualName;

	// Get the size of the vector.
	int totalPoints = data.GetData().size();

	// Change the total points if a non-zero upper bound
	// is specified. The upper bound should never be negative.
	assert(upper >= 0);
	totalPoints = ((upper > 0) && (upper < totalPoints))
		? upper : totalPoints;

	// Reserve the space in the arrays.
	plotData->X.reserve(totalPoints);
	plotData->Y.reserve(totalPoints);

	// Iterate through all the points in the shell data
	// and begin storing relevant values in the graph.
	for(int i = 0; i < totalPoints; i++)
		plotData->AddPoint(data.GetData()[i].Time,
		data.GetData()[i].Radius);

	// Register new plot data accordingly.
	this->RegisterPlot(plotData);
}