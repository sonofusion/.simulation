#pragma region Includes
#include "stdafx.h"
#include "FileProgressDialog.h"
#include <iostream>
using namespace DataVisualizerGUI;
#pragma endregion
const int FileProgressDialog::ProgressMaxWidth = 240;
const QString FileProgressDialog::LoadingFilesStr = "%1/%2 files loaded.";
const QString FileProgressDialog::StopLoadingStr = "Stop Loading";

FileProgressDialog::FileProgressDialog(int numberOfFiles, QWidget* parent) 
	: QObject(parent) {

		// Initialize the current progress counter value.
		this->CurrentProgress_ = 0;

		// Check if the number of files is a valid number.
		assert(numberOfFiles >= 0);
		if(numberOfFiles < 0) {
			Utilities::Print("Error in FileProgressDialog::FileProgressDialog(): "
				"number of files cannot be a negative number.");
			this->ProgressDialog_ = NULL;
			return;
		}

		// Initialize the progress dialog box.
		this->ProgressDialog_ = new QProgressDialog(
			LoadingFilesStr.arg("0", QString().setNum(numberOfFiles)),
			StopLoadingStr, 0, numberOfFiles, parent);

		// Set the modality, maximum width, the current value, and display the window.
		this->ProgressDialog_->setWindowModality(Qt::WindowModal);
		this->ProgressDialog_->setMaximumWidth(ProgressMaxWidth);
		this->ProgressDialog_->setMinimumDuration(0);
		this->ProgressDialog_->setValue(0);
		this->ProgressDialog_->setCancelButton(0); // NOTE: Temporarily disable.
		this->ProgressDialog_->show();

		// Connect the incrementation signals to the progress dialog.
		connect(this, SIGNAL(Incremented(int)), this->ProgressDialog_, SLOT(setValue(int)));
		connect(this, SIGNAL(Incremented(const QString&)), this->ProgressDialog_, SLOT(setLabelText(const QString&)));
}

FileProgressDialog::~FileProgressDialog() {

	// Delete the progress dialog
	// if it hasn't been deleted.
	if(this->ProgressDialog_)
		delete this->ProgressDialog_;
	this->ProgressDialog_ = NULL;
}

void FileProgressDialog::Increment() {

	// Localize all of the values necessary.
	int progressVal = this->ProgressDialog_->value();
	int currentProgress = this->CurrentProgress_++;
	int maximumProgress = this->ProgressDialog_->maximum();
	QString message = LoadingFilesStr.arg(QString().setNum(currentProgress), 
		QString().setNum(maximumProgress));

	// Check if an increase of one will exceed the maximum allowed value in
	// the progress bar. If it does not increment the current value up by one.
	if(currentProgress <= maximumProgress) {

		// Emit signals with newly assigned values
		emit Incremented(currentProgress);
		emit Incremented(message);
	}
}

void FileProgressDialog::Complete() {
	this->ProgressDialog_->setValue(this->ProgressDialog_->maximum());
}