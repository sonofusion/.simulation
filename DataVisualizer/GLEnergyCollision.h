// GLEnergyCollision.h
// Written by Joshua Mellott-Lillie
#pragma once

#include "GraphGL.h"
#include "DataCollisionEnergy.h"

using namespace DataReader;

namespace DataVisualizerGUI {
	/*!
	* This class takes in a DataCollisionEnergy
	* and outputs an OpenGL plot of points with
	* color for temperature.
	*/
	class GLEnergyCollision : public GraphGL
	{
	public:
		/*! 
		* Initializes the settings for the widget
		* @param parent The parent container.
		*/
		GLEnergyCollision(QWidget *parent = 0);
	
		// Destructs the memory allocation for the object.
		~GLEnergyCollision(void);

		/*!
		* Set the data to be read for this data collision energy.
		* @param data The data to be read.
		*/
		void setData(DataCollisionEnergy *data, int index);

		void changeDataSet(int index);
	protected:
	
		// This function draws the widget.
		void draw();
	};
}


