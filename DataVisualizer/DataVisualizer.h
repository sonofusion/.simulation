// DataVisualizer.h - Main UI class implementation.
// Written By Jesse Z. Zhong
//! \file
#ifndef __Data_Visualizer_Plot_GUI_H__
#define __Data_Visualizer_Plot_GUI_H__
#pragma region Includes
#include "stdafx.h"
#include <QMainWindow>
#include <QTimer>
#include <QSettings>
#include "DataVisualizerGUIEX.h"
#include "StartupUIEX.h"
#include "DataManager.h"
using namespace std;
#pragma endregion
/*!
  DataVisualizerGUI is charged with reading in simulation
  data, generating and displaying metadata, and plotting data.
*/
namespace DataVisualizerGUI {

	/*! 
	  Method used to ensure that unit tests are 
	  referenced are referenced from this library.
	  Refer to "Important note for Visual C++ users" in: 
	  https://code.google.com/p/googletest/wiki/Primer
	*/
	int PullInMyLibrary();

	/*! 
	  The main program window of Data Visualizer.
	*/
	class DataVisualizer : public QMainWindow {
		Q_OBJECT
	public:
	#pragma region Constants
	#pragma region Window Titles
		//! The display name of the application.
		static const string ApplicationName;

		//! The name of the company that is sponsoring this application's development.
		static const string OrganizationName;

		//! The message that will be displayed in the title of the startup dialog.
		static const string StartupTitle;
	#pragma endregion
	#pragma region Refresh Interval
		//! Default refresh interval used when no value is specified.
		static const int DefaultInterval;

		//! Specifies the lower range of the refresh interval.
		static const int MinInterval;

		//! Specifies the upper range of the refresh interval.
		static const int MaxInterval;
	#pragma endregion
	#pragma region Plot Dimensions
		//! Specifies the lower range for plot widths.
		static const int MinPlotSize;

		//! Specifies the upper range for plot widths.
		static const int MaxPlotSize;

		//! Specifies the pixel width a plot can be expanded or reduced by.
		static const int ResizeIncrement;
	#pragma endregion
	#pragma region Registry Items
		//! Registry key for the directory being used to read files.
		static const QString FileDirectory;

		//! Registry key for the refresh interval for iterating through plots.
		static const QString RefereshInterval;
	#pragma endregion
	#pragma endregion
	#pragma region Instance
		/*! 
		  Constructor
	  
		  Initializes all UI forms and their members and objects.
		  \param parent reference to a widget that will claim ownership of this QMainWindow.
		*/
		DataVisualizer(QWidget *parent = 0);

		/*! 
		  Destructor

		  Deallocates any objects not belonging
		  and destroyed by the Qt hierarchy.
		*/
		~DataVisualizer();
	#pragma endregion
	protected slots:
	#pragma region Private Slots
	#pragma region Data and Plots
		/*! 
		  Reads and stores data using the directory provided.

		  If there is a directory referenced in the registry, which can be checked using
		  \a DataVisualizer::ProgramSettings_, the path in \a DataVisualizer::DataManager_ 
		  will be set with that path, using \a DataManager::SetDirPath. The 
		  \a DataVisualizer::DataManager_ will then call \a DataManager::StoreData to parse 
		  and store the data from that directory. The slider bar range will be set according
		  the number of data sets stored, and the slider position will be set to zero. The plots 
		  are then cleared and redrawn with \a DataVisualizer::RefreshGraphs.

		  If there was no directory found, return is called.

		  \see DataManager::StoreData, DataManager::SetDirPath, DataVisualizer::RefreshGraphs
		*/
		void StoreData();

		/*! 
		  Redraws all plots with a certain stored data set.

		  If the passed \a value is within the bounds of the data set collection, 
		  [0, DataManager::GetMaxSets), the \a DataVisualizer::CurrentPlot_ data 
		  set index will be assigned with the \a value.

		  \param value index of the stored data set.
		*/
		void RefreshGraphs(int value);

		/*!
		* Sub-usage for changing the slider value, the update will occur for the spin box
		* as well
		*/
		void SubRefreshGraphs(int value);

		/*! \overload
  

		  Clears and redraws the plots with the current data set, indicated by 
		  \a DataVisualizer::CurrentPlot_. If the \a DataVisualizer::PlotsIterating_ 
		  animation state for the plots is true, the \a DataVisualizer::CurrentPlot_ 
		  data set index will be incremented up by one.
		*/
		void RefreshGraphs();

		/*! 
		  Allows for the change of states between iterating and not iterating through plots.

		  If the animation state, \a DataVisualizer::PlotsIterating_, is true (active), 
		  set it to false (inactive). Likewise, if the state is false, set it to true.
		  The labels are changed to reflect the current animation state: "Stop" for if
		  the state is true ("animation playing"), "Start" for if the state is false
		  ("animation paused").
		*/
		void ToggleIteratingPlots();
		
		/*! 
		  Increases the size of the plots and redraw.

		  Checks to ensure that the plot width, \a DataVisualizerUIEX::GetGraphWidth, 
		  does not exceed \a DataVisualizer::MaxPlotSize. If it does not,
		  \a DataVisualizerUIEX::ChangePlotWidths is called to increase the width by 
		  \a DataVisualizer::ResizeIncrement.

		  \see DataVisualizerUIEX::GetGraphWidth, DataVisualizerUIEX::ChangePlotWidths
		*/
		void EnlargePlots();
		
		/*! 
		  Decreases the size of the plots and redraw.

		  Checks to ensure that the plot width, \a DataVisualizerUIEX::GetGraphWidth,
		  does not fall under \a DataVisualizer::MinPlotSize. If it does not,
		  \a DataVisualizerUIEX::ChangePlotWidths is called to decrease the width by 
		  \a DataVisualizer::ResizeIncrement.

		  \see DataVisualizerUIEX::GetGraphWidth, DataVisualizerUIEX::ChangePlotWidths
		*/
		void ShrinkPlots();
	#pragma endregion
	#pragma region Preference Tab
		/*! 
		  Prompts the user for a directory that contains simulation data.

		  If there is a directory referenced in the registry, which can be checked using
		  \a DataVisualizer::ProgramSettings_, the path is used to open the file dialog
		  at that directory. The prompt awaits for user interaction. If the path specified
		  is invalid ("Cancel" is pressed), return is called. Otherwise, the new directory 
		  is written to the registry using \a DataVisualizer::ProgramSettings_. The text
		  boxes for the simulation directory in \a DataVisualizer::UI_ and 
		  \a DataVisualizer::StartupUI_ are changed to the new directory's path.
		  Finally, signals are emitted for \a DataVisualizer::ChangedDirectory methods.

		  \see DataVisualizer::ChangedDirectory
		*/
		void UserSetDirectory();
		
		/*! 
		  Changes the amount of milliseconds of delay there is 
		  between the drawing of one graph and that of the next.

		  Checks if the \a value passed is negative. If it is, return is called.
		  Otherwise, the \a DataVisualizer::PlotUpdateTimer_ is set to \a value.
		  The refresh interval slider's value in \a DataVisualizer::UI_ is also 
		  set to \a value to correct its position in the event \a value was not
		  "passed" by the slider itself. The reflect interval label is changed
		  to reflect the new \a value. Finally, the \a value is saved to the
		  registry via \a DataVisualizer::ProgramSettings_ for future reference.

		  \param value number of milliseconds the delay will be changed to.
		*/
		void ChangeInterval(int value);
	
		/*! 
		  Changes the type of data and units that are displayed.

		  \param mode 0 is Complex View. 1 is Simple View.
		*/
		void ChangeViewMode(int mode);
	#pragma endregion
	#pragma endregion
	signals:
	#pragma region Signals
		/*! 
		  Emits a signal whenever the file directory has been changed.
		*/
		void ChangedDirectory(const QString& value);

		/*! 
		  Emits a signal whenever the file directory has been changed.
		*/
		void ChangedDirectory();
	#pragma endregion
	protected:
	#pragma region Helper Methods
		/*! 
		  Manages the connection of signals and slots.

		  Creates the following UI connections: \n
		  Start button clicks toggle the "play" or "pause" state for the plots.
		  Value changed on slider causes the plots to be refreshed with that new value.
		*/
		void ConnectWidgets();
		
		/*! 
		  Sets up various UI elements for the program to use.

		  Enlarge ("+") and shrink("-") buttons for the plots are set to auto repeat. This allows
		  for the actions to continue when the buttons are held down. Keyboard shortcuts are set
		  for each of the buttons as well, along with individual tooltips. The registry is then
		  checked for if it contains a previously set refresh interval and data files directory.
		  If those items are found, they are set to their respective text labels in the program.
		*/
		void SetupUI();
	#pragma endregion
	#pragma region Program Forms
		DataVisualizerGUIEX UI_;	//!< The main UI form window for this program.
		StartupUIEX StartupUI_;		//!< Form used for first time setup.
		QDialog* StartupBox_;		//!< Dialog generated to house the startup form.
	#pragma endregion
	#pragma region Data Storage
		DataManager* DataManager_;		//!< Reads and stores data from bulks of snapshot, save points, collision energy, and RPK files.
		QSettings* ProgramSettings_;	//!< Reads and writes the system settings for the program.
	#pragma endregion
	#pragma region Plot Data
		int CurrentPlot_;			//!< Indicates the index of a data set that will be plotted.
		bool PlotsIterating_;		//!< Indicates whether plots are being cycled through (animated).
		QTimer* PlotUpdateTimer_;	//!< Timer used to check when to increment to the next plot.
	#pragma endregion
	};
}
#endif // !__Data_Visualizer_Plot_GUI_H__
