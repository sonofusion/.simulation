// FileProgressDialog.h - FileProgressDialog class declaration.
// Written By Jesse Z. Zhong
#ifndef __File_Progress_Dialog_H__
#define __File_Progress_Dialog_H__
#pragma region Includes
#include <QProgressDialog>
#include "Utilities.h"
#pragma endregion
namespace DataVisualizerGUI {
	/*! 
	  This class is a wrapper of QProgressDialog with
	   specialized methods for updating the displayed values.
	*/
	class FileProgressDialog : public QObject {
		Q_OBJECT

		//! Sets the max size of the progress bar.
		static const int ProgressMaxWidth;

		//! String used for printing the number of files loaded.
		static const QString LoadingFilesStr;

		//! String displayed on the cancel button of the dialog box.
		static const QString StopLoadingStr;
	public:
		/*! 
		  Constructor

		  \param numberOfFiles indicates how many files need to be read.
		  It serves to set the maximum value the progress bar can have.
		  \param parent is the parent widget that claims ownership of the dialog.
		*/
		FileProgressDialog(int numberOfFiles, QWidget* parent = NULL);

		/*! 
		  Destructor
		*/
		~FileProgressDialog();

	public slots:
		/*! 
		  Increment the current value of the progress bar up by one.
		*/
		void Increment();

		/*! 
		  Sets the progress bar to full/complete.
		*/
		void Complete();

	signals:
		/*! 
		  Indicates that the current value has been incremented up.
		*/
		void Incremented(int value);

		/*! 
		  Indicates the message produced when the current value is incremented up.
		*/
		void Incremented(const QString& message);

	protected:
		//! Instance of a dialog box with a progress bar.
		QProgressDialog* ProgressDialog_;

		//! Current progress value.
		//! NOTE: Maintaining a counter here avoids race conditions
		//! that will occur when attempting to increment QProgressDialog::value()
		//! from different threads at different times during runtime.
		int CurrentProgress_;

		//! Allow the class unit test to have access to the progress dialog.
		friend class FileProgressDialogTest;
	};
}

#endif // !__File_Progress_Dialog_H__