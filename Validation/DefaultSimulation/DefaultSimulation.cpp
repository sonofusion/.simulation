﻿// DefaultSimulation.cpp - Used for acceptance testing
// Written by Nicholas Kriner
#pragma region Includes
#include "stdafx.h"
#include "DefaultSimulation.h"

using namespace AcceptanceTesting;
#pragma endregion
#pragma region Constants
// Sample size
#ifdef QT_DEBUG
	const int DefaultSimulation::SAMPLE_SIZE = 3;
#else
	const int DefaultSimulation::SAMPLE_SIZE = 1500;
#endif

// Paths to test parameter sets and the test folder
const string DefaultSimulation::VALIDATION_PATH_NAME = "..";
const string DefaultSimulation::RESULTS_PATH_NAME = "../ValidationResults/";
const string DefaultSimulation::SNAPSHOT_OUTPUT_PATH_NAME = "../AcceptanceTestingSnapshots/";
const string DefaultSimulation::PARAMSET_STEADY_STATE = "../ParameterSets/SteadyState.json";
const string DefaultSimulation::PARAMSET_ADIABATIC_COMPRESSION_MONATOMIC = "../ParameterSets/AdiabaticCompression_monatomic.json";
const string DefaultSimulation::PARAMSET_ADIABATIC_COMPRESSION_DIATOMIC = "../ParameterSets/AdiabaticCompression_diatomic.json";
const string DefaultSimulation::PARAMSET_EXPERIMENTAL_VALIDATION = "../ParameterSets/ExperimentalValidation.json"; 
#pragma endregion
#pragma region Constructor/Destructor
/*! Constructor that uses a default InfoStruct

	@param[in]	outputDirectory	Needs to be a valid directory. One is provided in the .cpp
*/
DefaultSimulation::DefaultSimulation(){

	QDir snapshotDir;
	QDir resultsDir;

	// Set up the InfoStruct_ with a default set of values used for testing
	this->InfoStruct_ = this->InfoStruct_.DefaultInfoStruct().ToSimulationUnits();
			
	snapshotDir = QDir(QString::fromStdString(SNAPSHOT_OUTPUT_PATH_NAME));
	this->OutDir_ = SNAPSHOT_OUTPUT_PATH_NAME;

	resultsDir = QDir(QString::fromStdString(RESULTS_PATH_NAME));

	if(!snapshotDir.exists()){
		snapshotDir.mkdir(QString::fromStdString(SNAPSHOT_OUTPUT_PATH_NAME));
	}

	if(!resultsDir.exists()){
		resultsDir.mkdir(QString::fromStdString(RESULTS_PATH_NAME));
	}

	// Test to see if the snapshot testing directory has anything unrelated to testing
	// If it does, someone put something there, and don't delete it
	bool validTestDirectory;

	if(SNAPSHOT_OUTPUT_PATH_NAME.empty() == false){
		// See the function for reasons as to why it would not be a valid directory
		validTestDirectory = this->DeleteFilesFromTestDirectory();
		if(!validTestDirectory){
			// If the test directory chosen has other stuff, exit
			exit(1);
		}
	} else {
		// Exit if there is no place to put the snapshots
		exit(1);
	}
}

/*! Constructor that uses an InfoStruct provided by the user

	@param[in]	outputDirectory	Needs to be a valid directory. One is provided in the .cpp
*/
DefaultSimulation::DefaultSimulation(InfoStruct infoStruct){

	QDir snapshotDir;
	QDir resultsDir;

	// Set up the InfoStruct_ with a default set of values used for testing
	this->InfoStruct_ = infoStruct.ToSimulationUnits();
			
	snapshotDir = QDir(QString::fromStdString(SNAPSHOT_OUTPUT_PATH_NAME));
	this->OutDir_ = SNAPSHOT_OUTPUT_PATH_NAME;

	resultsDir = QDir(QString::fromStdString(RESULTS_PATH_NAME));

	if(!snapshotDir.exists()){
		snapshotDir.mkdir(QString::fromStdString(SNAPSHOT_OUTPUT_PATH_NAME));
	}

	if(!resultsDir.exists()){
		resultsDir.mkdir(QString::fromStdString(RESULTS_PATH_NAME));
	}

	// Test to see if the string passed in is a valid test directory,
	//    the test directory should be a persistent folder in the project
	bool validTestDirectory;
	if(SNAPSHOT_OUTPUT_PATH_NAME.empty() == false){
		// See the function for reasons as to why it would not be a valid directory
		validTestDirectory = this->DeleteFilesFromTestDirectory();
		if(!validTestDirectory){
			// If the test directory chosen has other stuff, exit
			exit(1);
		}
	} else {
		// Exit if there is no place to put the snapshots
		exit(1);
	}

	if((infoStruct.InitialRadius == infoStruct.AdiabaticRadius)
		&& (infoStruct.InitialRadius == infoStruct.AmbientRadius)
		&& (infoStruct.InitialRadius == infoStruct.IsothermalRadius)){
			this->SteadyState = true;
	} else {
		this->SteadyState = false;
	}
}

/*! Destructor; removes all files from the directory (or may leave some for manual inspection)
		  
*/
DefaultSimulation::~DefaultSimulation() {
			
	// 1 == Delete all files when done testing
	// 0 == Leave snapshots up for manual inspection
	#if 0
		this->DeleteFilesFromTestDirectory();
	#else
		this->OnlySnapshots();
	#endif
}
#pragma endregion
/*! Deletes old snapshots from the previous run of the test Simulation.
	
	\return true if deletion was successful; false if it was an inappropriate directory or a deletion error occurred
*/
bool DefaultSimulation::DeleteFilesFromTestDirectory(){

	QDir		removeDir(QString::fromStdString(this->OutDir_)); // Directory of the snapshot tests
	QStringList simTypes;											 // Extensions used by the simulation
	QStringList testFileList = removeDir.entryList();				 // Populates the list with all filenames
																	 //    in the test directory
	bool		simTestDirectory = false;							 // true if all files are used by the sim
	bool		successfulRemoval = false;							 // true if a file is successfully removed

	// RPActual = RPGuess = Snapshots = .dat
	// CollisionEnergy = .csv
	// Save points = .txt
	// . is for "." and ".." which are always found by QDir
	simTypes << ".dat" << ".txt" << ".csv" << ".";

	// While there is a file in the directory that hasn't been read
	for(QString fileName : testFileList){

		// Default to it not being a file used by the sim
		simTestDirectory = false;			

		// Check each of the extensions
		for(QString simType : simTypes){
			if(fileName.endsWith(simType, Qt::CaseInsensitive)){
				// If one of the file extensions matches the file types, set simTestDirectory
				//    to true and don't check the rest (of the extension types)
				simTestDirectory = true;
				break;
			}
		}

		// If a file not used by the simulation is found, return a blank directory string indicating
		//    the removal of files should not continue
		if(simTestDirectory == false){
			return false;
		}
	}

	// If this is a test directory, remove old test files
	if(simTestDirectory){

		// Remove all files in the test directory
		for(QString fileName : testFileList){
			if(!fileName.startsWith(".")){

				successfulRemoval = removeDir.remove(fileName);
				if(successfulRemoval == false){
					return false;
				}
			}
		}
	}

	// If everything occurred sucessfully, return the directory path indicating it is clear.
	return true;
}

/*! Deletes all files except snapshots from the test directory. Useful as .csv and .txt files are
	   not useful for testing. This should only be called by the destructor and is for debug purposes.
	
	\return true if deletion was successful; false if it was an inappropriate directory or a deletion error occurred
*/
bool DefaultSimulation::OnlySnapshots(){

	QDir		removeDir(QString::fromStdString(this->OutDir_)); // Directory of the snapshot tests
	QStringList testFileList = removeDir.entryList();				  // Populates the list with all filenames
																	  //    in the test directory
	bool		successfulRemoval = false;							  // true if a file is successfully removed

	// Remove all files in the test directory
	// As this is only called in the destructor, and all checks for valid/existing directory name
	//    are performed in the deleteFilesFromTestDirectory() during construction and snapshot generation,
	//    the directory is valid and if it didn't exist before construction it does now.
	for(QString fileName : testFileList){
		if(!(fileName.startsWith("Snapshot", Qt::CaseInsensitive) || fileName.startsWith("rpac", Qt::CaseInsensitive) || fileName.startsWith("."))){

			successfulRemoval = removeDir.remove(fileName);
			if(successfulRemoval == false){
				return false;
			}
		}
	}

	// If everything occurred sucessfully, return the directory path indicating it is clear.
	return true;
}

/*! Prints newlines. Quickest method of cross-platform "clear screen" for debugging.

*/
void DefaultSimulation::ClearScreen(){

	for(int i = 0; i < 20; i++){
		cout << "\n";
	}
}

/*! Returns the full pathname for the results directory

*/
string DefaultSimulation::GetResultsPath(){

	QDir dir;
	string dirName;

	// Gets the full path name of the file (needed for ParseInfoStruct)
	dir = QDir(QString::fromStdString(RESULTS_PATH_NAME));
	dirName = dir.absolutePath().toStdString();
		
	return dirName + "/";
}

/*! Returns the full pathname for the validation directory

*/
string DefaultSimulation::GetSnapshotDirectoryPath(){
	
	QDir dir;
	string dirName;

	// Gets the full path name of the file (needed for ParseInfoStruct)
	dir = QDir(QString::fromStdString(SNAPSHOT_OUTPUT_PATH_NAME));
	dirName = dir.absolutePath().toStdString();
		
	return dirName + "/";
}

/*! Returns a vector containing the DataSnapshots generated by the Simulation

	@param[in]	doubleList	List of doubles
	\return					Vector list of the DataSnapshots generated by the default Simulation
*/
vector<DataSnapshot> DefaultSimulation::GetSnapshots(){

	QDir				 testDir;  // Test directory
	QStringList			 fileList; // List of files in the directory
	DataSnapshot		 dSnap;	   // Temp variable to hold snapshot data
	string				 snapshot; // Name of a file that was read
	vector<DataSnapshot> dsList;   // List of snapshots to be returned

	testDir = QDir(QString::fromStdString(this->OutDir_));
	fileList = testDir.entryList();
	dSnap = DataSnapshot();
	snapshot = "";
	dsList = vector<DataSnapshot>();

	// Check all files in the directory
	for(QString fileName : fileList){

		// Only read Snapshot files
		if(fileName.startsWith("Snapshot", Qt::CaseInsensitive)){

			// DataSnapshot::Read requires an absolute path, not just the file name, in a string
			snapshot = (testDir.absolutePath() + "/" + fileName).toStdString();
			dSnap.Read(snapshot);

			// Add the snapshot read to the list of snapshots
			dsList.push_back(dSnap);
		}
	}

	return dsList;
}

#pragma region Vector Functions
/*! Takes in a list of doubles and returns the average of the list

	@param[in]	doubleList	List of doubles
	\return					average of the list of doubles
*/
double DefaultSimulation::AverageOfVector(vector<double> doubleList){
	
	double average = 0; // Average of the doubles
	double sum = 0;		// Sum of the doubles
	int n = 0;			// Number of doubles read

	// For every double in the list, add it to the sum and increment the counter
	for(double val : doubleList){
		n++;
		sum += val;
	}

	// Can't divide by 0
	if(n == 0){
		exit(1);
	}

	average = sum / ((double)n);

	return average;
}

/*! Takes in a list of doubles and returns the variance of the list

	@param[in]	doubleList	List of doubles
	\return					variance of the list of doubles
*/
double DefaultSimulation::VarianceOfVector(vector<double> doubleList){
	
	double variance = 0; // Variance
	double average = 0;  // Average
	int n = 0;			 // Number of doubles read

	// Finds the average of the doubles
	average = AverageOfVector(doubleList);

	// Sum (x - x̄)^2 over the double list
	for(double val : doubleList){
		n++;
		variance += pow((val - average), 2);
	}

	// Divided by n-1 instead of n due to Bessel's correction
	variance = variance / ((double)(n - 1));

	return variance;
}

/*! Takes in a list of doubles and returns their standard deviation. Uses Bessel's correction.

	@param[in]	doubleList	List of doubles
	\return					Standard deviation of the list of doubles
*/
double DefaultSimulation::StdevOfVector(vector<double> doubleList){

	return sqrt(VarianceOfVector(doubleList));
}
#pragma endregion

/*! Runs a Simulation using a default set of units.

	@param[in]	numSnapshots	Number of snapshots that will be generated by the simulation
*/
bool DefaultSimulation::ProduceSnapshots(int numSnapshots){

	EventType type;	// Used during simulation execution to progress

	// Try to delete files from the test directory. If the return value is false,
	//    something went wrong and execution should not continue. See the function
	//    above for some reasons why it may not delete files.
	if(DeleteFilesFromTestDirectory() == false){
		exit(1);
	}

	// InfoStruct_ holds a default set of units
	// outDir is set in the constructor
	// true == write
	Simulation simulation(this->InfoStruct_, this->OutDir_, true);

	// Run the simulation for n Snapshots
	for(unsigned long long updates = 0; updates < numSnapshots;) {
		if(type = simulation.NextStep()){
			if(type == UpdateSystemEvent) {
				// Increment the number of updates up by one.
				updates++;
			}
		}

		// Halt the simulation after it has rebounded.
		
		// If the radii are equal in the infostruct, SteadyState is being tested and
		//    rebounds are not supposed to occur: run for the actual number of snapshots requested
		if(!SteadyState){
			// Otherwise, only test up until the first rebound.
			if(simulation.HasRebounded())
				break;
		}
	}

	return true;
}

/*! Reads RPactual.dat to retrieve the pressures corresponding to each snapshot.

	\return Vector containing each pressure. The index of the pressure corresponds
			to the index of its snapshot

			i.e., dSnaps = getSnapshots();
				  pressures = getPressures();

				  Snapshot 76 == dSnaps.at(75), Pressure of snapshot 76 == pressures.at(75)
*/
vector<double> DefaultSimulation::GetPressures(){

	QDir					testDir;   // Test directory
	QStringList				fileList;  // List of files in the directory
	double					val;	   // Temp variable to hold snapshot data
	string					RPactual;  // Name of a file that was read
	DataRPK					dataFile;  // Holds the data in RPactual.dat
	vector<RpkDataPoint>	dataSet;
	vector<double>			dList;     // List of snapshots to be returned

	testDir = QDir(QString::fromStdString(this->OutDir_));
	fileList = testDir.entryList();
	val = 0;
	RPactual = "";
	dataFile = DataRPK();
	dataSet = vector<RpkDataPoint>();
	dList = vector<double>();

	// Check all files in the directory
	for(QString fileName : fileList){

		// Only read RPactual.dat
		if(fileName.startsWith("RPactual", Qt::CaseInsensitive)){

			// DataSnapshot::Read requires an absolute path, not just the file name, in a string
			RPactual = (testDir.absolutePath() + "/" + fileName).toStdString();
			dataFile.Read(RPactual);
			dataSet = dataFile.GetData();

			for(RpkDataPoint snapshot : dataSet){
				dList.push_back(snapshot.Pressure);
			}
		}
	}

	return dList;
}

/*! Uses the Ideal Gas Law to calculate Pressure
					
	local param,				R (Joules * Kelvin^-1 * mol^-1)
	@param[in]	temperature		
	@param[in]	moles			n (mols)
	@param[in]	volume			V (meters^3)
	\return	P = (nRT / V) in Pascals
*/
double DefaultSimulation::IdealGasLaw_GetPressure(double temperature, double mols, double volume){
	return ((mols * Constants::GAS_CONSTANT * temperature) / (volume));
}

/*! Uses the Ideal Gas Law to calculate Temperature

	local param,				R (Joules * Kelvin^-1 * mol^-1)
	@param[in]	pressure		P (Pascals)
	@param[in]	moles			n (mols)
	@param[in]	volume			V (meters^3)
	\return	T = (PV / nR) in Kelvins
*/
double DefaultSimulation::IdealGasLaw_GetTemperature(double pressure, double mols, double volume){
	return ((pressure * volume) / (mols * Constants::GAS_CONSTANT));
}

/*! Uses the Ideal Gas Law to calculate Volume

	local param,				R (Joules * Kelvin^-1 * mol^-1)
	@param[in]	pressure		P (Pascals)
	@param[in]	temperature		T (Kelvin)
	@param[in]	moles			n (mols)
	\return	V = (nRT / P) in meters^3
*/
double DefaultSimulation::IdealGasLaw_GetVolume(double pressure, double temperature, double mols){
	return ((mols * Constants::GAS_CONSTANT * temperature) / (pressure));
}

/*! Takes in a relative path name, converts it to a full path name, and returns an InfoStruct
	parsed from the file specified in the path. Should NOT be used outside of validation testing, where
	parameter set filenames should be predictable and will not change (and they should stay in the same place).

	e.g., say you're in "C:\Users\User\Documents\MoreDocs" and you want "C:\Users\User\Documents\Example.json",
	      ParseParameterSet("..\Example.json");
		  should be equivalent to
		  InfoStruct::ParseInfoStruct("C:\Users\User\Documents\Example.json");
*/
InfoStruct DefaultSimulation::ParseParameterSet(string fileName){

	QDir dir;
	string dirName;

	// Gets the full path name of the file (needed for ParseInfoStruct)
	dir = QDir(QString::fromStdString(fileName));
	dirName = dir.absolutePath().toStdString();
		
	return InfoStruct::ParseInfoStruct(dirName);
}