﻿// main.cpp
// Written by Nicholas Kriner
#include "stdafx.h"

#include <DefaultSimulation.h>

#include <math.h>
#include <cmath>
#include <Utilities.h>

#include <iostream>
#include <string>

using namespace AcceptanceTesting;

// Experimental Validation. This is a simplified acceptance test that will run
//    for n snapshots or until the first rebound, whichever occurs first.
//    The Temperature (Kelvin), Pressure (bars), and Volume (m^3) of the bubble are
//    printed to a .txt file. Average and stdev of each value are provided but
//    commented out.
int main(int argc, char *argv[]) {
	
	int i = 0;								// index
	int gasType = 0;						// Used to find gas 
	double stdevTemperature = 0;			// Standard deviation of the snapshot temperatures
	double stdevPressure = 0;				// Standard deviation of the snapshot pressures
	double stdevVolume = 0;					// Standard deviation of the snapshot volumes
	double averageTemperature = 0;			// Average temperature throughout all snapshots
	double averagePressure = 0;				// Average pressure throughout all snapshots
	double averageVolume = 0;				// Average of the volume through all snapshots
	double tempVolume = 0;					// Volume of a conical section of a snapshot, in cubic meters
	vector<DataSnapshot> dataSnapshots;		// Vector containing all DataSnapshots
	vector<double> temperatures;			// Vector containing the temperatures of the snapshots
	vector<double> pressures;				// Vector containing the pressures of the snapshots
	vector<double> volumes;					// Vector containing the volumes of the snapshots
	InfoStruct testStruct;					// Test InfoStruct

	temperatures = vector<double>();
	pressures	 = vector<double>();
	volumes	     = vector<double>();

	testStruct = DefaultSimulation::ParseParameterSet(DefaultSimulation::PARAMSET_EXPERIMENTAL_VALIDATION);

	// Create a simulation that will output to the test directory using the provided testStruct
	DefaultSimulation dSim(testStruct);
	
	// Produce snapshots in the test directory; Simulation will run until the first rebound or
	//    it hits the max sample size
	dSim.ProduceSnapshots(DefaultSimulation::SAMPLE_SIZE);
	
	// Read the snapshots in the test directory
	dataSnapshots = dSim.GetSnapshots();

	// Read the pressures from RPactual.dat
	pressures = dSim.GetPressures();

	// For each snapshot in the list of snapshots, add its average temperature to the list of temperatures
	//    and its volume to the list of volumes
	for(DataSnapshot dSnap : dataSnapshots){

		tempVolume = Utilities::ConicalSection(dSnap.GetBubbleRadius(), testStruct.ConeAngle, false);

		temperatures.push_back(dSnap.GetAvgTemperature());
		volumes.push_back(tempVolume);
	}

	// Formatting
	dSim.ClearScreen();
	cout << "! Snapshot generation complete.\n\n";

	// Find the average temperatures and pressures of the system (from the snapshots)
	//averageTemperature = DefaultSimulation::AverageOfVector(temperatures);
	//averagePressure    = DefaultSimulation::AverageOfVector(pressures);
	//averageVolume      = DefaultSimulation::AverageOfVector(volumes);

	// Find the standard deviation of the lists
	//stdevTemperature = DefaultSimulation::StdevOfVector(temperatures);
	//stdevPressure    = DefaultSimulation::StdevOfVector(pressures);
	//stdevVolume      = DefaultSimulation::StdevOfVector(volumes);

	#ifdef QT_DEBUG

		/* Template provided, but mostly commented out. */

		// Start listing data (first row is a header row)
		std::cout << "Temperatures\tPressures\tVolumes\n";
		for(int i = 0; i < temperatures.size(); i++){
			cout << temperatures[i] << "\t" << pressures[i] << "\t" << volumes[i] << "\n";
		}

		// Output the sample's averages
		//std::cout << "Average Temperature: " << averageTemperature << endl;
		//std::cout << "Average Pressure: " << averagePressure << endl;
		//std::cout << "Average Adiabatic Ratio: " << averageVolume << endl << endl;

		// Output the sample's standard deviations
		//std::cout << "Standard Deviation of Temperature: " << stdevTemperature << endl;
		//std::cout << "Standard Deviation of Pressure: " << stdevPressure << endl;
		//std::cout << "Standard Deviation of Adiabatic Ratios: " << stdevVolume << endl << endl;
	#else
		// Output the results directly to a text file inside the acceptance testing folder
		string fileName2 = (DefaultSimulation::GetResultsPath() + "ExperimentalValidationResults.txt");

		// Create a file stream with the new file name.
		ofstream file(fileName2);

		// Append each ratio into the file
		for(i = 0; i < temperatures.size(); i++){
			file << temperatures[i] << "\t" << pressures[i] << "\t" << volumes[i] << "\n";
		}	

		// Close file stream.
		file.close();
	#endif
}