﻿// main.cpp
// Written by Nicholas Kriner
#include "stdafx.h"

#include <DefaultSimulation.h>

#include <math.h>
#include <cmath>
#include <Utilities.h>

#include <iostream>
#include <string>

using namespace AcceptanceTesting;

// This is intended to check and make sure that the Ideal Gas Law holds true.
// The vector "volumes" is filled with the snapshot volumes of the bubble solely based on cone angle
//    and bubble radius
// The vector "calculatedVolumes" is filled with the predicted volumes using V = nRT / P (Ideal Gas Law)
// These two are then compared to ensure that they are similar.
int main(int argc, char *argv[]) {
	
	int i = 0;								// index
	bool isDiatomic = true;					// true == diatomic test; false == monatomic test
	double stdevTemperature = 0;			// Standard deviation of the snapshot temperatures
	double stdevPressure = 0;				//                                    pressures
	double stdevVolume = 0;					//                                    volumes
	double averageTemperature = 0;			// Average temperature throughout all snapshots
	double averagePressure = 0;				//         pressure
	double averageVolume = 0;				//         volume
	double calcPressure = 0;				// Calculated Pressure based on PV = nRT
	double tempVolume = 0;					// Volume of a conical section of a snapshot, in cubic meters
	vector<DataSnapshot> dataSnapshots;		// Vector containing all DataSnapshots
	vector<double> temperatures;			// Vector containing the temperatures of the snapshots
	vector<double> pressures;				//                       pressures
	vector<double> volumes;					//                       volumes
	vector<double> calculatedVolumes;		// Vector containing what the volumes are expected to be using PV = nRT
	InfoStruct testStruct;					// Test InfoStruct

	temperatures 	  = vector<double>();
	pressures		  = vector<double>();
	volumes			  = vector<double>();
	calculatedVolumes = vector<double>();

	// 1 == diatomic gas  (Hydrogen Gas)
	// 0 == monatomic gas (Hydrogen)
	#if 1
		testStruct = DefaultSimulation::ParseParameterSet(DefaultSimulation::PARAMSET_ADIABATIC_COMPRESSION_DIATOMIC);
		isDiatomic = true;
	#else
		testStruct = DefaultSimulation::ParseParameterSet(DefaultSimulation::PARAMSET_ADIABATIC_COMPRESSION_MONATOMIC);
		isDiatomic = false;
	#endif

	// Create a simulation that will output to the test directory using the provided testStruct
	DefaultSimulation dSim(testStruct);
	
	// Produce snapshots in the test directory; Simulation will run until the first rebound or
	//    it hits the max sample size
	dSim.ProduceSnapshots(DefaultSimulation::SAMPLE_SIZE);
	
	// Read the snapshots in the test directory
	dataSnapshots = dSim.GetSnapshots();

	// Read the pressures from RPactual.dat
	pressures = dSim.GetPressures();

	// For each snapshot in the list of snapshots, add its average temperature to the list of temperatures
	for(DataSnapshot dSnap : dataSnapshots){

		// The Scheduler and ParameterSets take in angles in degrees: these angles are later converted to
		//    radians for simulation purposes, but as dSim produces a copy of the testStruct and does not
		//    actually modify testStruct, testStruct's ConeAngle is still in degrees
		// Calculate the volume of a cone.
		tempVolume = (1.0/3.0) * Constants::PI * pow(tan(testStruct.ConeAngle * Constants::PI / 180), 2)
			         * pow(dataSnapshots[i++].GetBubbleRadius(), 3);

		temperatures.push_back(dSnap.GetAvgTemperature());
		volumes.push_back(tempVolume);
	}

	// RPactual lists pressures in the same order that snapshots are produced,
	//    so pressures.at(i) corresponds to dataSnapshots.at(i)
	for(i = 0; i < temperatures.size(); i++){
		
		// pressures read from RPactual.dat are in bars: need to be converted to pascals for PV = nRT
		calcPressure = (pressures[i] * Constants::P0);

		// calcPressure = Pascals, temperatures = Kelvin, and to get moles for the third 
		//    parameter, divide the integer particle count by the Avogadro constant
		// First one or two pressures are 0, if IdealGasLaw_GetVolume is used with P==0, the result is
		//    an infinite volume. So just ignore them and plot the rest.
		if(pressures[i] == 0){
			tempVolume = 0.0;
		} else {
			tempVolume = DefaultSimulation::IdealGasLaw_GetVolume(calcPressure, temperatures[i], (dataSnapshots[i].GetParticleCount() / Constants::AVOGADRO));
		}

		calculatedVolumes.push_back(tempVolume);
	}

	// Formatting
	dSim.ClearScreen();
	cout << "! Snapshot generation complete.\n\n";

	/* Most stuff is not needed as the main method of validation should be manual
		inspection to ensure the simulation's volumes match up with the predicted volumes. */

	// Find the average temperatures and pressures of the system (from the snapshots)
	averageTemperature = DefaultSimulation::AverageOfVector(temperatures);
	averagePressure    = DefaultSimulation::AverageOfVector(pressures);
	averageVolume      = DefaultSimulation::AverageOfVector(volumes);

	// Find the standard deviation of the lists
	stdevTemperature = DefaultSimulation::StdevOfVector(temperatures);
	stdevPressure    = DefaultSimulation::StdevOfVector(pressures);
	stdevVolume      = DefaultSimulation::StdevOfVector(volumes);

	#ifdef QT_DEBUG

		// Start listing data (first row is a header row)
		std::cout << "Temperatures\tPressures\tVolumes\n";
		for(int i = 0; i < temperatures.size(); i++){
			cout << temperatures[i] << "\t" << pressures[i] << "\t" << volumes[i] << "\n";
		}

		std::cout << endl << endl;

		// Output the sample's averages
		std::cout << "Average Temperature: " << averageTemperature << endl;
		std::cout << "Standard Deviation of Temperature: " << stdevTemperature << endl << endl;

		std::cout << "Average Pressure: " << averagePressure << endl;
		std::cout << "Standard Deviation of Pressure: " << stdevPressure << endl << endl;

		std::cout << "Average Volume: " << averageVolume << endl;
		std::cout << "Standard Deviation of Volumes: " << stdevVolume << endl << endl;
	#else

		// Output the results directly to a text file inside the acceptance testing folder
		string fileName2 = (DefaultSimulation::GetResultsPath() + "AdiabaticCompression" + 
			                ((isDiatomic == true) ? "Diatomic" : "Monatomic") + "Results.txt");

		// Create a file stream with the new file name.
		ofstream file(fileName2);

		// Append each ratio into the file
		for(i = 0; i < temperatures.size(); i++){
			file << temperatures[i] << "\t" << pressures[i] << "\t" << volumes[i] << "\t" << calculatedVolumes[i] << "\n";
		}

		// Close file stream.
		file.close();
	#endif
}