// main.cpp
// Written by Nicholas Kriner
#include "stdafx.h"

#include <DefaultSimulation.h>

#include <math.h>
#include <Utilities.h>

#include <iostream>
#include <string>

using namespace AcceptanceTesting;

// This is intended to test Temperature, Density, and Pressure when using a Constant driver
int main(int argc, char *argv[]) {
	
	int i = 0;							// index
	double standardDeviations = 0;		// Used to calculate distance between average and expected
	double adjustedPressure = 0;		// Pressure adjusted to account for surface tension
	double initialTemperature = 0;		// An initial temperature, read from the InfoStruct
	double initialPressure = 0;			// An initial pressure, read from the Infostruct
	double stdevTemperature = 0;		// Standard deviation of the snapshot temperatures
	double stdevPressure = 0;			//                                    pressures
	double stdevDensity = 0;			//                                    densities
	double averageTemperature = 0;		// Average temperature throughout all snapshots
	double averagePressure = 0;			//         pressure
	double averageDensity = 0;			//         densities
	vector<DataSnapshot> dataSnapshots; // Vector containing all DataSnapshots
	vector<double> temperatures;		// Vector containing the temperatures of the snapshots
	vector<double> pressures;			//                       pressures
	vector<double> densities;			//                       densities
	InfoStruct testStruct;				// Test InfoStruct

	testStruct = DefaultSimulation::ParseParameterSet(DefaultSimulation::PARAMSET_STEADY_STATE);

	initialTemperature = testStruct.Temperature;
	initialPressure    = testStruct.AmbientPressure;

	temperatures = vector<double>();
	pressures    = vector<double>();
	densities    = vector<double>();

	// Create a simulation that will output to the test directory using the provided testStruct
	DefaultSimulation dSim(testStruct);

	// Produce snapshots in the test directory; Simulation will run until the first rebound or
	//    it hits the max sample size
	dSim.ProduceSnapshots(dSim.SAMPLE_SIZE);

	// Read the snapshots in the test directory
	dataSnapshots = dSim.GetSnapshots();

	// Read the pressures from RPactual.dat
	pressures = dSim.GetPressures();

	// For each snapshot in the list of snapshots, add its average temperature to the list of temperatures
	for(DataSnapshot dSnap : dataSnapshots){

		temperatures.push_back(dSnap.GetAvgTemperature());
		densities.push_back(dSnap.GetAvgDensity());
	}

	// Formatting
	dSim.ClearScreen();
	cout << "! Snapshot generation complete.\n\n";

	// Find the average temperatures, pressures, and densities of the system (from the snapshots)
	stdevTemperature = DefaultSimulation::StdevOfVector(temperatures);
	stdevPressure    = DefaultSimulation::StdevOfVector(pressures);
	stdevDensity     = DefaultSimulation::StdevOfVector(densities);

	// Find the standard deviation of the lists
	averageTemperature = DefaultSimulation::AverageOfVector(temperatures);
	averagePressure    = DefaultSimulation::AverageOfVector(pressures);
	averageDensity     = DefaultSimulation::AverageOfVector(densities);

	// The console is used during debug mode. On release, which is used for fast simulation, output is written to
	//    the Validation directory.
	#ifdef QT_DEBUG
	
		// Start listing data (first row is a header row)
		std::cout << "Temperatures\tPressures\tDensities\n";
		for(int i = 0; i < temperatures.size(); i++){
			cout << temperatures[i] << "\t" << pressures[i] << "\t" << densities[i] << "\n";
		}

		std::cout << endl << endl;

		// Output the sample's averages
		std::cout << "Average Temperature: " << averageTemperature << endl;
		std::cout << "Standard Deviation of Temperature: " << stdevTemperature << endl << endl;

		std::cout << "Average Pressure: " << averagePressure << endl;
		std::cout << "Standard Deviation of Pressure: " << stdevPressure << endl << endl;

		std::cout << "Average Density: " << averageDensity << endl;
		std::cout << "Standard Deviation of Densities: " << stdevDensity << endl << endl;
	#else

		// Output the results directly to a text file inside the acceptance testing folder
		string fileName2 = (DefaultSimulation::GetResultsPath() + "SteadyStateResults.txt");

		// Create a file stream with the new file name.
		ofstream file(fileName2);
		
		// Append each ratio into the file: this data formatted to be placed in the provided excel file
		for(int i = 0; i < temperatures.size(); i++){
			file << temperatures[i] << "\t" << pressures[i] << "\t" << densities[i] << "\n";
		}
		
		// Close file stream.
		file.close();
	#endif

	// Additionally, provide various standard deviation and average results directly to the console
	
	standardDeviations = (averageTemperature - initialTemperature) / stdevTemperature;

	cout << "The initial temperature (" << initialTemperature << ")\nis (" << abs(standardDeviations);
	cout << ") standard deviations away from " << ((standardDeviations < 0) ? "(above)" : "(below)");
	cout << "\nthe average temperature (" << averageTemperature << ")\n\n";

	// adjusted pressure = average pressure - surface tension
	// surface tension = ((2 * liquid-specific surface tension constant) / ambient radius), then divide by Constants::P0
	//                                                                                      to go from Pascals to bars
	adjustedPressure = averagePressure - (2 * testStruct.LiquidSurfaceTension / testStruct.AmbientRadius / Constants::P0);

	standardDeviations = (adjustedPressure - initialPressure) / stdevPressure;

	cout << "The initial pressure (" << initialPressure << ")\nis (" << abs(standardDeviations);
	cout << ") standard deviations away from " << ((standardDeviations < 0) ? "(above)" : "(below)");
	cout << "\nthe average pressure (" << adjustedPressure << ")\n\n";
}