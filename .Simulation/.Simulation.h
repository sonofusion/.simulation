

#ifndef DOTSIMULATION_H
#define DOTSIMULATION_H

#include <QMainWindow>
#include "ui_.Simulation.h"

class DotSimulation : public QMainWindow
{
	Q_OBJECT

public:
	DotSimulation(QWidget *parent = 0);
	~DotSimulation();

private:
	Ui::DotSimulationClass ui;
};

#endif // DOTSIMULATION_H
