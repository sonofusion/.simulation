// DotSimulation

#ifndef __Dot_Simulation_H__
#define __Dot_Simulation_H__

#include <QMainWindow>
#include "ui_.Simulation.h"

class DotSimulation : public QMainWindow {
	Q_OBJECT

public:
	DotSimulation(QWidget *parent = 0);
	~DotSimulation();

private:
	Ui::DotSimulationClass ui;
};

#endif // !__Dot_Simulation_H__
