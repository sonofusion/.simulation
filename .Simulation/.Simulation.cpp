/*! 
  \mainpage .%Simulation Documentation
  <b>.Simulation</b> is a software package aimed at providing the appropriate tools necessary
  for simulating the effects of <a href="http://en.wikipedia.org/wiki/Sonoluminescence">
  Sonoluminescence</a>, "the emission of short bursts of light from imploding bubbles in 
  a liquid when excited by sound". At its core, the software package consists of three individual
  components that can be built and run independently of one another or incorporated together as a
  complete application. These components are the molecular dynamics and plasma simulations, the 
  simulations scheduler, and the visualizer. Each of these components are described further in detail 
  in the subsequent sections.

  \section modOverview Modules Overview
  Below is a diagram of the individual modules that make up the three major components of the
  package. The arrows indicate the dependence of modules to certain other modules when the package 
  is built as a single application. For instance, in order for Scheduler to schedule simulations, 
  it requires the existence of both the Molecular Dynamics and Plasma Simulations. Likewise, Data 
  Reader's parsing objects are necessary for both the Scheduler and Data Visualizer to read specific 
  file types.
  <br><br>
  \image html ModulesOutline.png

  \subsection schedulerOverview Scheduler

  The Scheduler is .Simulation's utility for starting simulations with user defined parameters, either
  through field inputs in the utility's UI or by reading a previously written JSON style parameters file.
  The utility has the ability to monitor different stages of a simulation, generate necessary output, and
  pause, continue, or stop a simulation with a click of a button.

  The code for the Scheduler is wrapped within SchedulerGUI. The main module class, SchedulerGUI::Scheduler
  is derived from Qt's main application window class, <a href="http://qt-project.org/doc/qt-4.8/qmainwindow.html">
  QMainWindow</a>. \aScheduler provides the module's main functionality of reading parameter files and starting
  simulations while supporting the UI's main features programmatically.

  \subsection datavizOverview Visualizer

  Data Visualizer is a utility designed for the visualization of simulation data sets generated from 
  both simulations.

  The encapsulating namespace for this module is DataVisualizerGUI, with DataVisualizerGUI::DataVisualizer 
  being the module's main class. DataVisualizerGUI::DataVisualizer inherits UI functionality from 
  <a href="http://qt-project.org/doc/qt-4.8/qmainwindow.html">QMainWindow</a>, Qt's main application window 
  class. 
  
  \aDataVisualizer declares an instance of DataVisualizerGUI::DataVisualizerGUIEX, an class extension of 
  the user interface. \aDataVisualizerGUIEX contains all of the plotting widgets, which are extensions 
  (DataVisualizerGUI::QCustomPlotEX) of QCPlot::QCustomPlot, a third-party Qt addition for drawing interactive
  graphs and plots.

  \subsection mdSimOverview Molecular Dynamics

  \subsection plasmaSimOverview Plasma

  \subsection dataReaderOverview Data Reader

  The Data Reader module is a small supporting API with the ability to read the various simulation input
  and output file types. Each of the file readers acts as a parser to interpret file data and a container 
  for storing data that has been read in. The file readers are derived from the DataReader::DataFile base 
  class, which provides the interface with common members for implementing the different types of file 
  readers. The classes for this module are housed with the DataReader namespace.

  The following are the file reader classes used for reading the different file types:
  
  DataReader::DataSnapshot reads simulation snapshot JavaScript Object Notation (JSON) style files.
  
  DataReader::DataSavepoint reads particle data from Comma-Separated Values (CSV) files.

  DataReader::DataCollisionEnergy reads particle collision energy data from CSV style files.

  DataReader::DataRPK reads predicted predicted bubble collapse data from CSV style files.

  DataReader::DataParameters reads simulation parameters from JSON formatted files.

  To instantiate any of the classes, you can call the constructor and pass the file name of the file you
  wish to parse. Alternatively, you may create an empty instance of the class and call the 
  DataReader::DataFile::Read with the file name. Both methods will create the file reader and attempt to
  read from the file you reference.

  Inline instantiation:
  \code{.cpp}
  DataParameters params("myParameters.json");
  \endcode

  Separated instantiation and reading:
  \code{.cpp}
  DataParameters params;
  params.Read("myParameters.json");
  \endcode
*/

#include "stdafx.h"
#include ".Simulation.h"

DotSimulation::DotSimulation(QWidget *parent)
	: QMainWindow(parent) {
	ui.setupUi(this);
}

DotSimulation::~DotSimulation() {

}
