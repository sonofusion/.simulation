#include "stdafx.h"
#include "qtglproject.h"
#include "GraphGL.h"

QtGLProject::QtGLProject(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	setWindowTitle(tr("Sample OpenGL Application"));
	this->setFocusPolicy(Qt::ClickFocus);
	this->ui.glWidget->setData(new DataSavepoint("sp.txt"));
}

QtGLProject::~QtGLProject()
{

}
