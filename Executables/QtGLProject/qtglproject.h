#ifndef QTGLPROJECT_H
#define QTGLPROJECT_H

#include <QMainWindow>
#include "ui_qtglproject.h"

class QtGLProject : public QMainWindow
{
	Q_OBJECT

public:
	QtGLProject(QWidget *parent = 0);
	~QtGLProject();

private:
	Ui::QtGLProjectClass ui;
};

#endif // QTGLPROJECT_H
