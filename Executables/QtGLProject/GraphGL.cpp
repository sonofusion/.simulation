#include "stdafx.h"
#include "GraphGL.h"
#include <GL/GLU.h>

GraphGL::GraphGL(QWidget *parent)
	: QGLWidget(QGLFormat(QGL::DoubleBuffer|QGL::Rgba), parent)
{
	this->setFocusPolicy(Qt::ClickFocus);
}


GraphGL::~GraphGL(void)
{

}

void GraphGL::paintGL()
{
	draw();
	//swapBuffers();
}

void GraphGL::resizeGL(int w, int h)
{

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	float aspect = w / h;
	
	glViewport(0, 0, w, h);
	glOrtho(100, -100, 0, 100, 1, -1);
	
	glMatrixMode(GL_MODELVIEW);

	glDisable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT);

	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
}