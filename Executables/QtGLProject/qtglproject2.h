#ifndef QTGLPROJECT2_H
#define QTGLPROJECT2_H

#include <QMainWindow>
#include "ui_qtglproject2.h"

class QtGLProject2 : public QMainWindow
{
	Q_OBJECT

public:
	QtGLProject2(QWidget *parent = 0);
	~QtGLProject2();

private:
	Ui::QtGLProject ui;
};

#endif // QTGLPROJECT2_H