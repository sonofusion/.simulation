#include "stdafx.h"
#include "GLParticleDist.h"
#include "GraphGL.h"
#include "GL/GLU.h"

#include <map>
#include <limits>

using namespace std;

GLParticleDist::GLParticleDist(QWidget *parent)
	: GraphGL(parent)
{
}


GLParticleDist::~GLParticleDist(void)
{

}

void GLParticleDist::setData(DataSavepoint *data)
{
	this->datafile = data;
}

void GLParticleDist::initializeGL()
{


	map<int, Vector3<float>*> gasColorMap;
	gasColorMap[1] = new Vector3<float>(0.5, 0.0, 0.0);
	gasColorMap[2] = new Vector3<float>(0.5, 0.5, 0.0);
	gasColorMap[3] = new Vector3<float>(0.5, 0.0, 0.5);
	gasColorMap[4] = new Vector3<float>(0.5, 0.5, 0.5);
	gasColorMap[5] = new Vector3<float>(1.0, 0.0, 0.0);
	gasColorMap[6] = new Vector3<float>(1.0, 0.5, 0.5);
	gasColorMap[7] = new Vector3<float>(1.0, 0.5, 0.5);
	gasColorMap[8] = new Vector3<float>(1.0, 0.5, 0.5);

	vector<ParticleData> data = this->datafile->GetData();
	
	glNewList(GLParticleDistList, GL_COMPILE);

	int scale = 100;
	float alphavalue = (float)(scale * scale) / data.size();
	int maxy = 0;
	int maxx = 0;
	int maxz = 0;

	foreach(ParticleData pd, data)
	{
		if(pd.Position.X > maxx)
			maxx = pd.Position.X;
		if(pd.Position.Y > maxy)
			maxy = pd.Position.Y;
		if(pd.Position.Z > maxz)
			maxz = pd.Position.Z;
	}

	glBegin(GL_POINTS);

	foreach(ParticleData pd, data)
	{
		Vector3<float> *color = gasColorMap[pd.GasType];
		glColor4f(color->X, color->Y, color->Z, 0.2f);
		glVertex2d(pd.Position.X, pd.Position.Z / maxz * scale);
	}

	glEnd();

	
	glEndList();

}

void GLParticleDist::draw()
{
	/* Clear buffers */
	glClear(GL_COLOR_BUFFER_BIT);
	glCallList(GLParticleDistList);
}
