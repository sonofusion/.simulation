#ifndef GRAPH_GL_PARTICLE_H
#define GRAPH_GL_PARTICLE_H

#include "GraphGL.h"
#include "DataSavepoint.h"

using namespace DataReader;

#define GLParticleDistList 23764

/*!
* This class takes in a DataSavepoint
* and outputs an OpenGL plot of points.
*/
class GLParticleDist : public GraphGL
{

public:
	/*! 
	* Initializes the settings for the widget
	* @param parent The parent container.
	*/
	GLParticleDist(QWidget *parent = 0);

	
	// Destructs the memory allocation for the object.
	~GLParticleDist(void);
	
	/*!
	* Set the data to be read for this savepoint.
	* @param data The data to be read.
	*/
	void setData(DataSavepoint *data);

	
protected:
	/*!
	* This function overwrites the OpenGL initialize 
	* function to load the data into an OpenGL list.
	*/
	void initializeGL();
	
	// This function draws the widget.
	void draw();
	

private:
	// This is the data to be read
	DataSavepoint *datafile;
};

#endif

