#include "stdafx.h"
#include "qtglproject.h"
#include "qtglproject2.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	QtGLProject w;
	w.show();
	int titleHeight = w.frameGeometry().height() - w.geometry().height();
	w.resize(400, 400 - titleHeight);
	w.setWindowTitle("GLParticleDistribution");

	QtGLProject2 w2;
	w2.show();
	int titleHeight2 = w2.frameGeometry().height() - w2.geometry().height();
	w2.resize(400, 400 - titleHeight2);
	w2.setWindowTitle("GLEnergyCollision");

	return a.exec();
}
