#ifndef GRAPH_GL_COLLISION_H
#define GRAPH_GL_COLLISION_H

#include "GraphGL.h"
#include "DataCollisionEnergy.h"

using namespace DataReader;

#define GLEnergyCollisionList 125874

/*!
* This class takes in a DataCollisionEnergy
* and outputs an OpenGL plot of points with
* color for temperature.
*/
class GLEnergyCollision : public GraphGL
{
public:
	/*! 
	* Initializes the settings for the widget
	* @param parent The parent container.
	*/
	GLEnergyCollision(QWidget *parent = 0);
	
	// Destructs the memory allocation for the object.
	~GLEnergyCollision(void);

	/*!
	* Set the data to be read for this data collision energy.
	* @param data The data to be read.
	*/
	void setData(DataCollisionEnergy *data);

protected:
	/*!
	* This function overwrites the OpenGL initialize 
	* function to load the data into an OpenGL list.
	*/
	void initializeGL();
	
	// This function draws the widget.
	void draw();

private:
	// This is the data to be read
	DataCollisionEnergy *datafile;
};

#endif
