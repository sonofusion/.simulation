#include "stdafx.h"
#include "GLEnergyCollision.h"


GLEnergyCollision::GLEnergyCollision(QWidget *parent)
	: GraphGL(parent)
{
}


GLEnergyCollision::~GLEnergyCollision(void)
{
}

void GLEnergyCollision::setData(DataCollisionEnergy *data)
{
	this->datafile = data;
}

void GLEnergyCollision::initializeGL()
{
	vector<CollisionData> data = this->datafile->GetData();

	glNewList(GLEnergyCollisionList, GL_COMPILE);
	int scale = 100;

	const int maxTest = 4;

	double minY[maxTest];
	double maxY[maxTest];
	double minZ[maxTest];
	double maxZ[maxTest];
	double maxTemp[maxTest];
	double minTemp[maxTest];

	for(int i = 0; i < maxTest; i++)
	{
		minY[i] = maxY[i] = data[0].Position.Y;
		minZ[i] = maxZ[i] = data[0].Position.Z;
		minTemp[i] = maxTemp[i] = data[0].Temperature;
	}

	double maxTemperature = 0;
	double minTemperature = 0;
	double outlierMin;

	double epsilon = 0.003;

	outlierMin = minTemperature = data[0].Temperature;


	foreach(CollisionData cd, data)
	{
		double pushMaxY = cd.Position.Y;
		double pushMaxZ = cd.Position.Z;
		double pushTemp = cd.Temperature;
		for(int i = 0; i < maxTest; i++)
		{
			if(pushMaxY > maxY[i])
			{
				double temp = maxY[i];
				maxY[i] = pushMaxY;
				pushMaxY = temp;
			}

			if(pushMaxZ > maxZ[i])
			{
				double temp = maxZ[i];
				maxZ[i] = pushMaxZ;
				pushMaxZ = temp;
			}

			if(pushTemp > maxTemp[i])
			{
				double temp = maxTemp[i];
				maxTemp[i] = pushTemp;
				pushTemp = temp;
			}
		}

		double pushMinY = cd.Position.Y;
		double pushMinZ = cd.Position.Z;
		double pushMinTemp = cd.Temperature;
		for(int i = maxTest - 1; i >= 0; i--)
		{
			if(pushMinY < minY[i])
			{
				double temp = minY[i];
				minY[i] = pushMinY;
				pushMinY = temp;
			}

			if(pushMinZ < minZ[i])
			{
				double temp = minZ[i];
				minZ[i] = pushMinZ;
				pushMinZ = temp;
			}

			if(pushMinTemp > minTemp[i])
			{
				double temp = minTemp[i];
				minTemp[i] = pushMinTemp;
				pushMinTemp = temp;
			}
		}

		if(cd.Temperature > maxTemperature) {
			if(cd.Temperature < 0.0002) {
				maxTemperature = cd.Temperature;
			} 
		}
		if(cd.Temperature < minTemperature)
			minTemperature = cd.Temperature;


	}

	double range = maxTemperature - minTemperature;

	
	double yRange = maxY[maxTest - 1] - minY[maxTest - 1];
	yRange /= scale * 2;
	double zRange = maxZ[maxTest - 1] - minZ[maxTest - 1];
	zRange /= scale;

	glBegin(GL_POINTS);

	foreach(CollisionData cd, data)
	{
		double tempColor = (cd.Temperature - minTemperature) / range;


		glColor4f(tempColor, 0, 1 - tempColor, 1.0);

		double posY = (cd.Position.Y - minY[maxTest - 1]) / yRange;
		double posZ = (cd.Position.Z - minZ[maxTest - 1]) / zRange;

		glVertex2d(posY - 100.0f, posZ);
	}

	glEnd();

	glEndList();
}

void GLEnergyCollision::draw()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glCallList(GLEnergyCollisionList);
}