#include "stdafx.h"
#include "qtglproject2.h"
#include "GraphGL.h"

QtGLProject2::QtGLProject2(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	setWindowTitle(tr("Sample OpenGL Application"));
	this->setFocusPolicy(Qt::ClickFocus);
	this->ui.glwidget->setData(new DataCollisionEnergy("E:\\Users\\Joshua\\Downloads\\Example1\\CollisionEnergy00000252.csv"));
}

QtGLProject2::~QtGLProject2()
{

}
