// main.cpp
// Written by Jesse Z. Zhong
#include "DataVisualizer.h"
#include <QApplication>
#include <QMainWindow>
#include <iostream>
#include <QWidget>
using namespace DataVisualizerGUI;

// Program entry point.
// This program is used to build and run
// the data visualizer module independently.
// Build settings for both Debug and Release
// mode are set specifically for the module.
int main(int argc, char *argv[]) {
	
	// Create an instance of QApplication
	// for controlling the flow of the program.
	QApplication application(argc, argv);

	// Create an instance of the main GUI.
	DataVisualizer mainWindow;

	// Initialize the applications resources.
	Q_INIT_RESOURCE(DataVisualizerGUI);

	try {
		// Test if the window takes up
		// all of the screen space or not.
		int desktopArea = QApplication::desktop()->width() *
			QApplication::desktop()->height();
		int widgetArea = mainWindow.width() * mainWindow.height();

		if (((float)widgetArea / (float)desktopArea) < 0.75f)
			mainWindow.show();
		else
			mainWindow.showMaximized();

	} catch (std::string exception) {

		// Display any caught exceptions.
		std::cout << exception;
	}

	// Execute and return the end state
	// of the application when it closes.
	return application.exec();
}