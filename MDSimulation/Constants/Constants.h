#ifndef __CONSTANTS_CONSTANTS_H__
#define __CONSTANTS_CONSTANTS_H__

#include <boost/math/constants/constants.hpp>
#include <cmath>
#include <limits>
#include <map>

#include "../Vector/Vector.hpp"
#include "./Helper.h"

namespace MDSimulation 
{
	/**
	 * Constants referenced in the molecular dynamics simulation.
	 */
	namespace Constants
	{
		/** Golden Ratio **/
		const double PHI = 1.6180339887;

		/** Avogadro's Number **/
		const double AVOGADRO = 6.02214129e23;

		/** Gas Constant **/
		const double GAS_CONSTANT = 8.3144621;
		
		/** Boltzmann Constant **/
		const double BOLTZMANN_CONSTANT = 1.3806488e-23;

		/** The number of dimensions in the simulation. */
		const int DIMENSIONS = 3;

		/** Time value for an event that will never occur. */
		const double NEVER = std::numeric_limits<double>::infinity();

		/** The mathematical constant π. */
		const double PI = boost::math::constants::pi<double>();

		/** A particle index representing an invalid particle. */
		const int NULL_PARTICLE = -1;

		/** Atmospheric pressure */
		const double P0 = 1.0e5;                   // atmospheric pressure

		/** Boltzmann's constant */
		const double KB = 1.380662e-23;            // Boltzmann's const

		/** ??? */
		const double GAMMA = 5.0 / 3.0;

		/** Van Der Waal's constant */
		const double ExcludedVolume = 0.00005105;  // Van Der Waal's constant

		/** Energy required to initiate fusion in Joules */
		const double FusionBarrier = 4.5e7 * KB * 3.0 / 2.0;
		/** Plank's constant in m^2 kg / s */
		const double Plank = 6.626068e-34;

		/** Unit Vectors for each direction. */
		const IntVector X(1, 0, 0);
		const IntVector Y(0, 1, 0);
		const IntVector Z(0, 0, 1);

		/** Standard basis vectors in R^3 */
		const IntVector STD_BASIS[3] =
		{
			X,
			Y,
			Z
		};

		/** Negates standard basis vectors in R^3 */
		const IntVector NEG_STD_BASIS[3] =
		{
			IntVector(-X),
			IntVector(-Y),
			IntVector(-Z)
		};

		const int LIQUID_TYPES = 5;

		/** Names of the possible liquids in the simulation */
		const std::string LIQUID_NAMES[LIQUID_TYPES] =
		{
			"lithium",
			"mercury",
			"water",
			"diesel",
			"ig-4"
		};

		const std::map<std::string, int> LIQUID_FROM_NAME
			= invert_array_mapping<std::string>(LIQUID_NAMES, LIQUID_TYPES);

		/**
		 * Kinematic viscosity of the fluids in m^2/s
		 */
		const double Viscosity[LIQUID_TYPES] =
		{
			1.245e-6, // Lithium
			0.114e-6, // Mercury
			1.004e-6, // Water
			2.000e-6, // Diesel
			118.0e-6  // IG-4 from MultiTherm
		};

		/**
		 * Surface tension of the liquids in N/m.
		 */
		const double SurfaceTension[LIQUID_TYPES] =
		{
			0.396,   // Lithium
			0.485,   // Mercury
			0.0728,  // Water
			23.8e-3, // Diesel
			23.8e-3  // IG-4 from MultiTherm (estimated from diesel)
		};

		/**
		 * Density of the liquids in kg/m^3.
		 */
		const double Density[LIQUID_TYPES] =
		{
			516,    // Lithium
			5430,   // Mercury
			999.07, // Water
			832.00, // Diesel
			863.0   // IG-4 from MultiTherm
		};

		/**
		 * Speed of sound of the liquids in m/s.
		 */
		const double SpeedOfSound[LIQUID_TYPES] =
		{
			4490, // Lithium
			1450, // Mercury
			1497, // Water
			1250, // Diesel
			1300  // IG-4 from MultiTherm (estimate)
		};
	}
}

#endif /* __CONSTANTS_H__ */
