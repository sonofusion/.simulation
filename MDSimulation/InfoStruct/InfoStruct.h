#ifndef __INFOSTRUCT_INFOSTRUCT_H__
#define __INFOSTRUCT_INFOSTRUCT_H__

#include "../Constants/AtomicProperties.h"
#include "../Constants/Constants.h"
#include "rapidjson/document.h"
#include "rapidjson/filestream.h"
#include "rapidjson/prettywriter.h"
#include <qdir>
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

namespace MDSimulation
{
	/**
	 * Possible driver modes for use in the RPK equation. These modes define what
	 * type of pressure function to use for the driving pressure.
	 */
	enum DriverMode
	{
		/* Sinusoidal driver. */
		Sinusoidal,
		/* Constant is a constant pressure function. */
		Constant
	};

	/**
	 * Defines the structure used to hold the configurable constants of the
	 * simulation. These are values read in from a file and must then be g
	 * to simulation units.
	 *
	 * @author Spenser Bauman
	 */
	class InfoStruct
	{
	public:

		// Convenient synonym for the number of elements.
		static const int ET = Constants::ELEMENT_TYPES;

		std::vector<double> AtomicParts;

		// The cone angle is half the apex angle of the cone.
		double ConeAngle;
		double ConeTan2;
		double ConeSolidAngle;
		double Density;
		double Frequency;
		double DriverPressure;
		double AmbientPressure;
		double AmbientRadius;
		double AdiabaticRadius;
		double InitialRadius;
		double IsothermalRadius;
		double Temperature;
		double InitialTime;
		double InitialVelocity;
		double WallTemperature;

		int LiquidType;
		double LiquidDensity;
		double LiquidSpeedOfSound;
		double LiquidViscosity;
		double LiquidSurfaceTension;

		double FusionBarrier;

		double AtomicDiameter[ET];
		double AtomicDiameterSquared[ET][ET];

		double AtomicMass[ET];
		double ReducedMass[ET][ET];

		double VWallThermal[ET];

		double BirdConstant[ET];

		DriverMode Driver;

		double CollisionThreshold;

		/**
		 * Empty constructor simply initializes the values to zero. They must be
		 * set by the function deserializing the input and stored in this
		 * container. Kept private as much of its data needs to be read in from
		 * a file before values can be initialized.
		 */
		InfoStruct();

		/**
		 * Converts the current <code>InfoStruct</code> to simulation units. Uses the
		 * information provided by the Units module to convert the given
		 * parameters to simulation units and returns a new <code>InfoStruct</code>
		 * containing the simulation applicable constants.
		 *
		 * NOTE: This method makes the assumption that the current object is
		 *       not in simulation units and is, in fact, using real world units.
		 *
		 * @return A new <code>InfoStruct</code> where the units are transformed
		 *         to simulation units.
		 */
		InfoStruct ToSimulationUnits() const;

		/**
		 * Parser function to read in a JSON formatted text file and convert it
		 * to an <code>InfoStruct</code>. This makes use of the Boost Property Tree library
		 * internally.
		 *
		 * @param fname The name of the file to be read.
		 * @return The generated <code>InfoStruct</code>.
		 */
		static InfoStruct ParseInfoStruct(const std::string& fname);

		/**
		 * Returns an InfoStruct with default values. (Used for testing)
		 *
		 * @return The generated <code>InfoStruct</code>.
		 */
		static InfoStruct DefaultInfoStruct();

		/**
		 * Make this easier to output.
		 */
		friend std::ostream& operator<<(std::ostream& stream, const InfoStruct& matrix);

		void Read(const rapidjson::Value& reader);

		bool operator==(const InfoStruct& rhs);

		template <typename Writer>
		void Serialize(Writer& writer) const;

	private:

		/**
		 * Initializes the tables for the current object using the current
		 * values contained therein.
		 */
		void InitTables();

		/**
		 * Copies the constants defined in the <code>Constants</code> namespace into
		 * the current <code>InfoStruct</code>.
		 */
		void CopyConstants();
	};

	template <typename Writer>
	void InfoStruct::Serialize(Writer& writer) const
	{
	
		const char* liquidName = Constants::LIQUID_NAMES[LiquidType].c_str();

		writer.StartObject();

		writer.String("Environment");
		writer.StartObject();

		// Write all of the other values.
		writer.String("ConeAngle")           , writer.Double(ConeAngle);
		writer.String("Temperature")         , writer.Double(Temperature);
		writer.String("WallTemp")            , writer.Double(WallTemperature);
		writer.String("AmbientRadius")       , writer.Double(AmbientRadius);
		writer.String("DriverPressure")      , writer.Double(DriverPressure);
		writer.String("Frequency")           , writer.Double(Frequency);
		writer.String("InitialTime")         , writer.Double(InitialTime);
		writer.String("InitialRadius")       , writer.Double(InitialRadius);
		writer.String("InitialVelocity")     , writer.Double(InitialVelocity);
		writer.String("AmbientPressure")     , writer.Double(AmbientPressure);	
		writer.String("AdiabaticRadius")     , writer.Double(AdiabaticRadius);
		writer.String("IsothermalRadius")    , writer.Double(IsothermalRadius);
		if( Driver == Constant ){
			writer.String("DriverMode")      , writer.String("constant");
		} else {
			writer.String("DriverMode")      , writer.String("sinusoidal");
		}
		writer.String("Liquid")              , writer.String(liquidName);
		writer.EndObject();
		// Write the atomic parts array.
		writer.String("Particles");
		writer.StartObject();
		writer.String("AtomicParts");
		writer.StartObject();
			int i = 0;
			double val = 0;
			const char *elemName;
			for(i = 0; i < AtomicParts.size(); i++){
				val = AtomicParts.at(i);
				elemName = Constants::ELEMENT_NAMES[i].c_str();
				if(val != 0){
					writer.String(elemName), writer.Double(val);
				}
			}
		writer.EndObject();
		writer.EndObject();
		writer.EndObject();
	}
}

#endif /* __INFO_STRUCT_H__ */
