
#include <algorithm>
#include <stdio.h>
#include <string>
#include <vector>
#include <boost/math/special_functions.hpp>

#include "../Constants/AtomicProperties.h"
#include "../Constants/Constants.h"
#include "../Exceptions/SimulationException.h"
#include "../Math/Functions.h"
#include "../Units/Units.h"
#include "InfoStruct.h"

using namespace std;

namespace MDSimulation
{
	using Constants::PI;

	// Default collision threshold is 3keV
	static const double DEFAULT_THRESHOLD = 3.0;

	// Conversion factor of Kev to joules
	static const double KEV_TO_J = 1.602177e-16;

	static DriverMode
	read_driver_mode(const std::string& str)
	{
		std::string mode_name(str);
		std::transform(
				mode_name.begin(), mode_name.end(), mode_name.begin(), ::tolower);

		if (mode_name == "constant")
		{
			return Constant;
		}
		else if (mode_name == "sinusoidal")
		{
			return Sinusoidal;
		}
		throw SimulationException("Unknown driver mode: '" + str + "'");
	}

	InfoStruct::InfoStruct()
	{
		this->AtomicParts = std::vector<double>();
		this->ConeAngle = 0;
		this->ConeTan2 = 0;
		this->ConeSolidAngle = 0;
		this->Density = 0;
		this->Frequency = 0;
		this->DriverPressure = 0;
		this->AmbientPressure = 0;
		this->AmbientRadius = 0;
		this->AdiabaticRadius = 0;
		this->InitialRadius = 0;
		this->IsothermalRadius = 0;
		this->Temperature = 0;
		this->InitialTime = 0;
		this->InitialVelocity = 0;
		this->WallTemperature = 0;
		this->LiquidType = 0;
		this->LiquidDensity = 0;
		this->LiquidSpeedOfSound = 0;
		this->LiquidViscosity = 0;
		this->LiquidSurfaceTension = 0;
		this->FusionBarrier = 0;
		this->Driver = Constant;
		this->CollisionThreshold = 0;
		this->CopyConstants();
		this->InitTables();
	}

	InfoStruct
	InfoStruct::ToSimulationUnits() const
	{
		InfoStruct converted;
		converted.LiquidType = this->LiquidType;

		converted.CopyConstants();

		// Conversion of liquid parameters
		converted.LiquidDensity      = this->LiquidDensity * Pow3(Units::L) / Units::M;
		converted.LiquidViscosity    = this->LiquidViscosity / Units::KinematicViscosity;
		converted.LiquidSpeedOfSound = this->LiquidSpeedOfSound * Units::T / Units::L;
		converted.LiquidSurfaceTension
			= this->LiquidSurfaceTension / Units::SurfaceTension;


		// Conversion of data read directly from the InfoStruct file.
		converted.AtomicParts = this->AtomicParts;
		converted.ConeAngle   = this->ConeAngle * Constants::PI / 180.0; // radians

		converted.Frequency   = this->Frequency * Units::T;

		converted.DriverPressure = this->DriverPressure * Constants::P0 / Units::Pressure;
		converted.AmbientRadius = this->AmbientRadius / Units::L;

		converted.AmbientPressure
			= this->AmbientPressure * Constants::P0 / Units::Pressure;

		converted.AdiabaticRadius = this->AdiabaticRadius * converted.AmbientRadius;
		converted.InitialRadius = this->InitialRadius / Units::L;
		converted.IsothermalRadius = this->IsothermalRadius * converted.AmbientRadius;

		converted.Temperature = this->Temperature / Units::Temperature;

		converted.InitialTime = this->InitialTime / Units::T;
		converted.InitialVelocity = this->InitialVelocity / (Units::L / Units::T);

		converted.WallTemperature = this->WallTemperature / Units::Temperature;

		const double tension_pressure = 2.0 * converted.LiquidSurfaceTension
									  / converted.AmbientRadius;

		// Density must factor in the pressure caused by surface tension to obtain
		// a stable bubble.
		converted.Density = (converted.AmbientPressure + tension_pressure)
						  * Pow3(this->AmbientRadius / this->InitialRadius) / converted.Temperature ;

		// Convert parameters relevant to fusion
		converted.FusionBarrier = this->FusionBarrier / Units::Energy;

		// Convert AtomicMass
		std::transform(this->AtomicMass, this->AtomicMass + ET,         // Input
					   converted.AtomicMass,                            // Output
					   [this](double m) { return m / this->AtomicMass[ET - 1]; });

		std::transform(this->AtomicDiameter, this->AtomicDiameter + ET,
					   converted.AtomicDiameter,
					   [this](double d) { return d / this->AtomicDiameter[ET-1]; });

		// Convert VWallThermal
		std::transform(converted.AtomicMass, converted.AtomicMass + ET,
					   converted.VWallThermal,
					   [&converted](double mass)
					   { return sqrt(2.0 * converted.WallTemperature / mass); });

		// Compute cone tan here, as it is not valid to compute before the
		// conversion.
		converted.ConeTan2 = Sqr(tan(converted.ConeAngle));
		converted.ConeSolidAngle = 2.0 * PI * (1.0 - cos(converted.ConeAngle));

		// Convert the collision threshold, which is in keV to simulation units.
		converted.CollisionThreshold = this->CollisionThreshold * KEV_TO_J
									 / Units::Energy;

		// Copy appropriate tables to be initialized from the converted data,
		// rather than original SI values.
		converted.InitTables();
		converted.Driver = this->Driver;

		return converted;
	}

	InfoStruct
	InfoStruct::ParseInfoStruct(const std::string& fname)
	{
		using namespace rapidjson;

		FILE* infile = fopen(fname.c_str(), "r");
		if (NULL == infile)
		{
			throw SimulationException("Could not open InfoStruct file.");
		}

		FileStream instream(infile);
		Document document;

		if (document.ParseStream<0, UTF8<> >(instream).HasParseError())
		{
			fclose(infile);
			throw SimulationException("Could not parse InfoStruct file.");
		}
		fclose(infile);

		InfoStruct is;
		double sum = 0.0;

		assert(document["Environment"].IsObject());
		Value& environment = document["Environment"];

		// Determine the liquid type
		std::string liquid_name = environment["Liquid"].GetString();
		std::transform(liquid_name.begin(), liquid_name.end(),
					   liquid_name.begin(), ::tolower);

		const double tension_pressure = 2.0 * is.LiquidSurfaceTension
			/ is.AmbientRadius;
	
		// Environment information.
		is.LiquidType          = Constants::LIQUID_FROM_NAME.at(liquid_name);
		is.Driver              = read_driver_mode(environment["DriverMode"].GetString());
		is.ConeAngle           = environment["ConeAngle"].GetDouble();
		is.Temperature         = environment["Temperature"].GetDouble();
		is.WallTemperature     = environment["WallTemp"].GetDouble();
		is.AmbientRadius       = environment["AmbientRadius"].GetDouble();
		is.DriverPressure      = environment["DriverPressure"].GetDouble();
		is.AmbientPressure     = environment["AmbientPressure"].GetDouble();
		is.Frequency           = environment["Frequency"].GetDouble();
		is.InitialTime         = environment["InitialTime"].GetDouble();
		is.InitialRadius       = environment["InitialRadius"].GetDouble();
		is.InitialVelocity     = environment["InitialVelocity"].GetDouble();
		is.AdiabaticRadius     = environment["AdiabaticRadius"].GetDouble();
		is.IsothermalRadius    = environment["IsothermalRadius"].GetDouble();

		// Use the default collision threshold if no value is specified.
		if (environment.HasMember("CollisionThreshold"))
		{
			is.CollisionThreshold = environment["CollisionThreshold"].GetDouble();
		}
		else
		{
			is.CollisionThreshold = DEFAULT_THRESHOLD;
		}

		assert(document["Particles"]["AtomicParts"].IsObject());
		Value& parts = document["Particles"]["AtomicParts"];

		for (int i = 0; i < Constants::ELEMENT_TYPES; ++i)
		{
			if (parts.HasMember(Constants::ELEMENT_NAMES[i].c_str()))
			{
				is.AtomicParts.push_back(
						parts[Constants::ELEMENT_NAMES[i].c_str()].GetDouble());
			}
			else
			{
				is.AtomicParts.push_back(0.0);
			}
		}

		// Sum the atomic percentages.
		sum = std::accumulate(is.AtomicParts.begin(), is.AtomicParts.end(), 0.0,
							  [](double x, double y) { return x + y; });
		assert(sum > 0.0);
		// Divide by the sum to ensure they total to 1.
		std::transform(is.AtomicParts.begin(), is.AtomicParts.end(),
					   is.AtomicParts.begin(),
					   [sum](double p) { return p / sum; });

		is.Density = (is.AmbientPressure + tension_pressure) * Pow3(is.AmbientRadius / is.InitialRadius) / is.Temperature; // TODO: Add surface tension.
		is.ConeTan2 = 0.0;

		// Copy the values from the tables
		is.CopyConstants();
		is.InitTables();

		return is;
	}

	InfoStruct
	InfoStruct::DefaultInfoStruct(){
		
		const string exampleParameters = "../../Validation/ParameterSets/ExampleParameters.json";

		QDir dir;
		string dirName;

		// Gets the full path name of the file (needed for ParseInfoStruct)
		dir = QDir(QString::fromStdString(exampleParameters));
		dirName = dir.absolutePath().toStdString();
		
		return InfoStruct::ParseInfoStruct(dirName);
	}

	void
	InfoStruct::CopyConstants()
	{
		LiquidDensity        = Constants::Density[LiquidType];
		LiquidViscosity      = Constants::Viscosity[LiquidType];
		LiquidSurfaceTension = Constants::SurfaceTension[LiquidType];
		LiquidSpeedOfSound   = Constants::SpeedOfSound[LiquidType];

		FusionBarrier        = Constants::FusionBarrier;

		std::copy(Constants::ATOMIC_DIAMETER_INI,
				  Constants::ATOMIC_DIAMETER_INI + ET,
				  this->AtomicDiameter);

		std::copy(Constants::ATOMIC_MASS_INI,
				  Constants::ATOMIC_MASS_INI + ET,
				  this->AtomicMass);
	}

	void
	InfoStruct::InitTables()
	{
		// create look-up tables for d2 and half-reduced-mass
		for (int i = 0; i < ET; i++)
		{
			for (int j = i; j < ET; j++)
			{
				this->AtomicDiameterSquared[i][j]
					= this->AtomicDiameterSquared[j][i]
					= Sqr(0.5 * (this->AtomicDiameter[i] + this->AtomicDiameter[j]));

				this->ReducedMass[i][j]
					= this->ReducedMass[j][i]
					= 0.5 * this->AtomicMass[i] * this->AtomicMass[j]
					/ (this->AtomicMass[i] + this->AtomicMass[j]);
			}
		}

		// Initialize the Bird constants
		for (int i = 0; i < ET; i++)
		{

			double mu = Constants::BIRD_MU_INI[i] * 1.0e-5 / Units::DynamicViscosity;
			this->BirdConstant[i] = 5.0
				* (Constants::BIRD_ALPHA_INI[i] + 1.0)
				* (Constants::BIRD_ALPHA_INI[i] + 2.0)
				* sqrt(1.0 / PI);

			// Having just temperature in there is valid, as Boltzmann's constant
			// would just be canceled out due to multiplying then dividing by
			// k for the energy conversion.
			this->BirdConstant[i] *=
				pow(Constants::BIRD_T_REF / Units::Temperature,
					Constants::BIRD_OMEGA_INI[i] + 0.5);

			this->BirdConstant[i] /=
				(16.0 * Constants::BIRD_ALPHA_INI[i]
				 * exp(boost::math::lgamma(4.0 - Constants::BIRD_OMEGA_INI[i])) * mu);
		}
	}

	std::ostream&
	operator<<(std::ostream& stream, const InfoStruct& is)
	{
		stream  << "ConeAngle: "           << is.ConeAngle           << '\n'
				<< "ConeTan2: "            << is.ConeTan2            << '\n'
				<< "ConeSolidAngle: "      << is.ConeSolidAngle      << '\n'
				<< "Density: "             << is.Density             << '\n'
				<< "Frequency: "           << is.Frequency           << '\n'
				<< "DriverPressure: "      << is.DriverPressure      << '\n'
				<< "AmbientRadius: "       << is.AmbientRadius       << '\n'
				<< "AdiabaticRadius: "     << is.AdiabaticRadius     << '\n'
				<< "InitialRadius: "       << is.InitialRadius       << '\n'
				<< "IsothermalRadius: "    << is.IsothermalRadius    << '\n'
				<< "Temperature: "         << is.Temperature         << '\n'
				<< "InitialTime: "         << is.InitialTime		 << '\n'
				<< "InitialVelocity: "     << is.InitialVelocity     << '\n'
				<< "WallTemperature: "     << is.WallTemperature     << std::endl;

		stream << "Atomic Parts: " << std::endl;
		for (unsigned int i = 0; i < is.AtomicParts.size(); ++i)
		{
			stream << "   " << Constants::ELEMENT_NAMES[i] << ": "
				   << is.AtomicParts[i] << std::endl;
		}

		return stream;
	}

	void
	InfoStruct::Read(const rapidjson::Value& reader)
	{
		assert(reader["ConeAngle"].IsDouble());
		assert(reader["ConeTan2"].IsDouble());
		assert(reader["ConeSolidAngle"].IsDouble());
		assert(reader["Density"].IsDouble());
		assert(reader["Frequency"].IsDouble());
		assert(reader["DriverPressure"].IsDouble());
		assert(reader["AmbientRadius"].IsDouble());
		assert(reader["AdiabaticRadius"].IsDouble());
		assert(reader["InitialRadius"].IsDouble());
		assert(reader["IsothermalRadius"].IsDouble());
		assert(reader["Temperature"].IsDouble());
		assert(reader["InitialTime"].IsDouble());
		assert(reader["InitialVelocity"].IsDouble());
		assert(reader["WallTemperature"].IsDouble());

		this->ConeAngle           = reader["ConeAngle"].GetDouble();
		this->ConeTan2            = reader["ConeTan2"].GetDouble();
		this->ConeSolidAngle      = reader["ConeSolidAngle"].GetDouble();
		this->Density             = reader["Density"].GetDouble();
		this->Frequency           = reader["Frequency"].GetDouble();
		this->DriverPressure      = reader["DriverPressure"].GetDouble();
		this->AmbientRadius       = reader["AmbientRadius"].GetDouble();
		this->AdiabaticRadius     = reader["AdiabaticRadius"].GetDouble();
		this->InitialRadius       = reader["InitialRadius"].GetDouble();
		this->IsothermalRadius    = reader["IsothermalRadius"].GetDouble();
		this->Temperature         = reader["Temperature"].GetDouble();
		this->InitialTime         = reader["InitialTime"].GetDouble();
		this->InitialVelocity     = reader["InitialVelocity"].GetDouble();
		this->WallTemperature     = reader["WallTemperature"].GetDouble();

		this->CopyConstants();
		this->InitTables();
	}

	bool InfoStruct::operator==(const InfoStruct& rhs) {
		bool result = true;
		result &= (this->ConeAngle == rhs.ConeAngle);
		result &= (this->ConeTan2 == rhs.ConeTan2);
		result &= (this->ConeSolidAngle == rhs.ConeSolidAngle);
		result &= (this->Density == rhs.Density);
		result &= (this->Frequency == rhs.Frequency);
		result &= (this->DriverPressure == rhs.DriverPressure);
		result &= (this->AmbientPressure == rhs.AmbientPressure);
		result &= (this->AmbientRadius == rhs.AmbientRadius);
		result &= (this->AdiabaticRadius == rhs.AdiabaticRadius);
		result &= (this->InitialRadius == rhs.InitialRadius);
		result &= (this->IsothermalRadius == rhs.IsothermalRadius);
		result &= (this->Temperature == rhs.Temperature);
		result &= (this->InitialTime == rhs.InitialTime);
		result &= (this->InitialVelocity == rhs.InitialVelocity);
		result &= (this->WallTemperature == rhs.WallTemperature);
		result &= (this->LiquidType == rhs.LiquidType);
		result &= (this->LiquidDensity == rhs.LiquidDensity);
		result &= (this->LiquidSpeedOfSound == rhs.LiquidSpeedOfSound);
		result &= (this->LiquidViscosity == rhs.LiquidViscosity);
		result &= (this->LiquidSurfaceTension == rhs.LiquidSurfaceTension);
		result &= (this->FusionBarrier == rhs.FusionBarrier);
		result &= (this->Driver == rhs.Driver);
		result &= (this->CollisionThreshold == rhs.CollisionThreshold);
		result &= (this->AtomicParts == rhs.AtomicParts);
		return result;
	}
}