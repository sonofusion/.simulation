// ShellData.h - Shell data structure implementation.
// Written by Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "ShellData.h"
using namespace DataReader;
#pragma endregion

ShellData::ShellData() {
	this->Radius = 0;
	this->NumberOfParticles = 0;
	this->Density = 0;
	this->Temperature = vector<double>();
	this->MaxTemperature = vector<double>();
	this->Velocity = vector<double>();
	this->Ionization = vector<double>();
	this->Percent = vector<double>();
	this->KineticEnergy = vector<double>();
	this->KineticEnergyCom = vector<double>();
	this->GasTypes = 0;
}

ostream& operator<<(ostream& out, ShellData& shellData) {

	out << "Radius: " << shellData.Radius << endl;
	out << "Number of Particles: " << shellData.NumberOfParticles << endl;
	out << "Density: " << shellData.Density << endl;

	out << "Temperature: { ";
	for(int i = 0; i < shellData.GasTypes; i++) {
		out << shellData.Temperature[i];
		if(i != (shellData.GasTypes - 1)) 
			out << ", ";
	}
	out << " }\n";

	out << "Velocity: { ";
	for(int i = 0; i < shellData.GasTypes; i++) {
		out << shellData.Velocity[i];
		if(i != (shellData.GasTypes - 1)) 
			out << ", ";
	}
	out << " }\n";

	out << "Ionization: { ";
	for(int i = 0; i < shellData.GasTypes; i++) {
		out << shellData.Ionization[i];
		if(i != (shellData.GasTypes - 1)) 
			out << ", ";
	}
	out << " }\n";

	out << "Percent: { ";
	for(int i = 0; i < shellData.GasTypes; i++) {
		out << shellData.Percent[i];
		if(i != (shellData.GasTypes - 1)) 
			out << ", ";
	}
	out << " }\n";

	out << "KineticEnergy: { ";
	for(int i = 0; i < shellData.GasTypes; i++) {
		out << shellData.KineticEnergy[i];
		if(i != (shellData.GasTypes - 1)) 
			out << ", ";
	}
	out << " }\n";

	out << "KineticEnergyCom: { ";
	for(int i = 0; i < shellData.GasTypes; i++) {
		out << shellData.KineticEnergyCom[i];
		if(i != (shellData.GasTypes - 1)) 
			out << ", ";
	}
	out << " }\n";

	return out;
}