# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

TEMPLATE = lib
TARGET = DataReader
DESTDIR = ./
QT += core opengl widgets gui printsupport
CONFIG += staticlib
DEFINES += QT_LARGEFILE_SUPPORT QT_OPENGL_LIB _CRT_SECURE_NO_WARNINGS
INCLUDEPATH += ./GeneratedFiles \
    .
LIBS += -L"."
PRECOMPILED_HEADER = stdafx.h
DEPENDPATH += .
UI_DIR += ./GeneratedFiles
RCC_DIR += ./GeneratedFiles 
LIBS += -lUtilities
INCLUDEPATH += ../Utilities

debug {
    CONFIG += debug
    OBJECTS_DIR += debug
    INCLUDEPATH += ./GeneratedFiles/Debug
    MOC_DIR += ./GeneratedFiles/Debug
    LIBS += L"../Utilities/debug"
}

release {
    CONFIG += release
    OBJECTS_DIR += release
    INCLUDEPATH += ./GeneratedFiles/Release
    MOC_DIR += ./GeneratedFiles/Release
    LIBS += L"../Utilities/release"
}

QMAKE_CXXFLAGS += -mmacosx-version-min=10.7 -std=gnu0x -stdlib=libc+

include(DataReader.pri)
