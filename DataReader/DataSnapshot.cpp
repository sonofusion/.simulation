// ParseSnapshot.h - Parse Snapshot File Class Method Implementations
// Written By Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "Utilities.h"
#include "DataSnapshot.h"
namespace Utils = Utilities;
#pragma endregion
using namespace DataReader;
#pragma region Snapshot Keys
const char* DataSnapshot::SS_SnapshotID = "snap_shot";
const char* DataSnapshot::SS_SimTime = "t";
const char* DataSnapshot::SS_Radius = "r";
const char* DataSnapshot::SS_Bins = "bins";
const char* DataSnapshot::SS_ParticleCount = "ParticleCount";
const char* DataSnapshot::SD_Radius = "r";
const char* DataSnapshot::SD_Particles = "n";
const char* DataSnapshot::SD_Density = "density";
const char* DataSnapshot::SD_Temperature = "temperature";
const char* DataSnapshot::SD_MaxTemperature = "max_temperature";
const char* DataSnapshot::SD_Velocity = "velocity";
const char* DataSnapshot::SD_Ionization = "ionization";
const char* DataSnapshot::SD_Percentage = "percentage";
const char* DataSnapshot::SD_KineticEnergy = "kinetic_energy";
const char* DataSnapshot::SD_KineticEnergyCom = "kinetic_energy_com";
#pragma endregion

DataSnapshot::DataSnapshot(const string& fileName) 
			: DataFile(fileName) {

				// Initialize all field members.
				this->SnapshotID_ = 0;
				this->SimulationTime_ = 0;
				this->BubbleRadius_ = 0;
				this->MeanVelocity_ = 0;
				this->MedianVelocity_ = 0;
				this->TotalCollisions_ = 0;
				this->TotalCellCrossings_ = 0;
				this->TotalWallCollisions_ = 0;
				this->TotalConeBoundaryCollisions_ = 0;
				this->FusionRate_ = 0;
				this->ShellData_ = vector<ShellData>();
				this->GasTypes_ = 0;
				//this->WallRadius_ = 0;
				this->WallVelocity_ = 0;
				this->Pressure_ = 0;
				this->MaxTemperature_ = 0;
				this->AvgTemperature_ = 0;
				this->MaxEnergy_ = 0;
				this->AvgEnergy_ = 0;
				this->MaxDensity_ = 0;
				this->AvgDensity_ = 0;
				this->KineticEnergy_ = 0;
				this->KineticEnergyCom_ = 0;
				this->ParticleCount_ = 0;
				this->Ionizations_ = vector<int>();
				
				// Extend the Ionizations_ vector to have an index
				//    for each gas type
				/*for( string gas : MDSimulation::Constants::ELEMENT_NAMES ){
					this->Ionizations_.push_back(0);
				}*/
				for( int h = 0; h < 8; h++ ){
					this->Ionizations_.push_back(0);
				}

				// Attempts to read the file
				//    if a file name is passed.
				if(fileName != "")
					this->Read(fileName);
		}

void DataSnapshot::Read(const string& fileName) {
	try {
		// Check if the file exists and load it.
		FILE* filePtr = fopen(fileName.c_str(), "r");
		if(filePtr == NULL)
			return;

		// Record the file name.
		this->FileName_ = fileName;

		// Traverse the file to find the size.
		fseek(filePtr, 0, SEEK_END);
		size_t size = ftell(filePtr);
		fseek(filePtr, 0, 0);

		// Create a character buffer for storing the file.
		char* buffer = new char[size];

		// Store the file into the buffer and close the file.
		FileReadStream readStream(filePtr, buffer, size);

		// Create a document object to deserialize the JSON.
		Document doc;

		// Attempt to read the JSON file and store its data.
		if(doc.ParseStream<0, UTF8<>>(readStream).HasParseError()) {

			// Throw and exception if the reading fails at any point.
			std::string exceptionMessage = "Error when Parsing file " + fileName;
			throw exceptionMessage.c_str();
		}

		// Begin pulling data from the file.
		this->SnapshotID_ = doc[SS_SnapshotID].GetInt();
		this->SimulationTime_ = doc[SS_SimTime].GetDouble();
		this->BubbleRadius_ = doc[SS_Radius].GetDouble();
		this->ParticleCount_ = doc[SS_ParticleCount].GetInt();

		// Check if the array exists.
		assert(doc[SS_Bins].IsArray());

		// Reserve space for the shells.
		int numBins;
		this->ShellData_.clear();
		this->ShellData_.reserve(numBins = doc[SS_Bins].Size());

		// Declare temporary variables for calculating metadata.
		double densSum(0);
		double tempSum(0);
		double kinSum(0);
		int gasCount(0);
		vector<int> ionizationSum = vector<int>();
		vector<int> gasTypeSum = vector<int>();

		// Extend the Ionizations_ vector to have an index
		//    for each gas type
		for( string gas : MDSimulation::Constants::ELEMENT_NAMES ){
			ionizationSum.push_back(0);
			gasTypeSum.push_back(0);
		}

		// Parse the shell data.
		Value& data = doc[SS_Bins];
		for(int i = 0; i < numBins; i++) {
			// Store the shell data.
			ShellData item = this->ReadShellData(data[i]);
			this->ShellData_.push_back(item);

			// Update info from each gas type of each shell.
			for(int k = 0; k < MDSimulation::Constants::ELEMENT_TYPES; k++) {

				// Weights the temperature by particle percentage
				tempSum += item.Percent[k] * item.Temperature[k];

				// Weights the kinetic energy by particle percentage
				kinSum  += item.Percent[k] * item.KineticEnergy[k];

				ionizationSum[k] += item.Ionization[k];

				// Test for the max temperature.
				this->MaxTemperature_ = max(this->MaxTemperature_,
											item.MaxTemperature[k]);

				// Add each gas percentage into gasTypeSum
				// A vector is used because each bin may have
				//    separate gas types (e.g. bin 13 has 1.0 H2 and 0 H,
				//    bin 157 has 0.2 H2 0.8 H) and the result should return 2 in this case
				// At the end, the each index in gasTypeSum is checked for a non-zero
				//    value and summed for the total gas types
				gasTypeSum[k] += item.Percent[k];
			}

			// Sum up the individual density values.
			densSum += item.Density;

			// Test for the max density.
			this->MaxDensity_ = max(this->MaxDensity_, item.Density);
		}

		// Perform calculations for averages.
		if(numBins != 0) {

			for( int i = 0; i < MDSimulation::Constants::ELEMENT_TYPES; i++ ){
				gasCount += (gasTypeSum[i] != 0) ? 1 : 0;
			}
			
			this->GasTypes_ = gasCount;
			this->KineticEnergy_ = kinSum;
			this->Ionizations_ = ionizationSum;
			this->AvgDensity_ = densSum / numBins;
			this->AvgTemperature_ = tempSum / numBins;
		}

		// Clear the buffer.
		delete[] buffer;

		// Delete the file pointer.
		fclose(filePtr);

		// Set file read status to true.
		this->DataStored_ = true;

	} catch(char* ex) {
		cout << ex << endl;
	}
}

int DataSnapshot::GetSnapshotID() const { 
	return this->SnapshotID_; 
}

double DataSnapshot::GetSimulationTime() const { 
	return this->SimulationTime_; 
}

double DataSnapshot::GetBubbleRadius() const { 
	return this->BubbleRadius_; 
}

double DataSnapshot::GetMeanVelocity() const { 
	return this->MeanVelocity_; 
}

double DataSnapshot::GetMedianVelocity() const { 
	return this->MedianVelocity_; 
}

unsigned long long DataSnapshot::GetTotalCollisions() const { 
	return this->TotalCollisions_; 
}

unsigned long long DataSnapshot::GetTotalCellCrossings() const { 
	return this->TotalCellCrossings_; 
}

unsigned long long DataSnapshot::GetTotalWallCollisions() const { 
	return this->TotalWallCollisions_; 
}

unsigned long long DataSnapshot::GetTotalConeBoundaryCollisions() const { 
	return this->TotalConeBoundaryCollisions_; 
}

double DataSnapshot::GetFusionRate() const { 
	return this->FusionRate_; 
}

int DataSnapshot::GetGasTypes() const { 
	return this->GasTypes_; 
}

vector<ShellData>& DataSnapshot::GetShellData() { 
	return this->ShellData_; 
}

double DataSnapshot::GetWallVelocity() const { 
	return this->WallVelocity_; 
}

double DataSnapshot::GetPressure() const { 
	return this->Pressure_; 
}

double DataSnapshot::GetMaxTemperature() const { 
	return this->MaxTemperature_; 
}

double DataSnapshot::GetAvgTemperature() const { 
	return this->AvgTemperature_; 
}

double DataSnapshot::GetMaxEnergy() const { 
	return this->MaxEnergy_; 
}

double DataSnapshot::GetAvgEnergy() const { 
	return this->AvgEnergy_; 
}

double DataSnapshot::GetMaxDensity() const { 
	return this->MaxDensity_; 
}

double DataSnapshot::GetAvgDensity() const { 
	return this->AvgDensity_; 
}

double DataSnapshot::GetKineticEnergy() const { 
	return this->KineticEnergy_; 
}

double DataSnapshot::GetKineticEnergyCom() const { 
	return this->KineticEnergyCom_; 
}

int DataSnapshot::GetParticleCount() const { 
	return this->ParticleCount_; 
}

vector<int> DataSnapshot::GetIonizations() const { 
	return this->Ionizations_; 
}

ShellData DataSnapshot::ReadShellData(Value& item) {
	try {
		// Create an instance of shell data.
		ShellData data = ShellData();

		// Store individual values first.
		data.Radius = item[SD_Radius].GetDouble();
		data.NumberOfParticles = item[SD_Particles].GetInt();
		data.Density = item[SD_Density].GetDouble();

		// Store the arrays from the file.
		Value& temperature = item[SD_Temperature];
		data.Temperature = Utils::ParseAsArray<double>(temperature);

		Value& maxtemperature = item[SD_MaxTemperature];
		data.MaxTemperature = Utils::ParseAsArray<double>(maxtemperature);

		Value& velocity = item[SD_Velocity];
		data.Velocity = Utils::ParseAsArray<double>(velocity);

		Value& ionization = item[SD_Ionization];
		data.Ionization = Utils::ParseAsArray<double>(ionization);

		Value& percent = item[SD_Percentage];
		data.Percent = Utils::ParseAsArray<double>(percent);

		Value& kinEnergy = item[SD_KineticEnergy];
		data.KineticEnergy = Utils::ParseAsArray<double>(kinEnergy);

		Value& kinEnergyCom = item[SD_KineticEnergyCom];
		data.KineticEnergyCom = Utils::ParseAsArray<double>(kinEnergyCom);

		// Store the number of types of gases.
		data.GasTypes = (int)data.Temperature.size();

		// Check for the integrity of the file by checking sizes.
		if((data.GasTypes != data.Velocity.size())
			|| (data.GasTypes != data.Ionization.size())
			|| (data.GasTypes != data.Percent.size())) {

				// Throw an exception.
				throw "Inconsistent number of gas types found.";
		}

		// Return the data.
		return data;
	} catch (char* ex) {
		cout << ex << endl;
	}
	// In the event that an exception is caught,
	// return an empty instance of ShellData.
	return ShellData();
}

ostream& operator<<(ostream& out, DataSnapshot& data) {

	out << "Snapshot " << data.GetSnapshotID() << " {" << endl;
	out << "SimTime:     " << data.GetSimulationTime() << endl;
	out << "BubbleRadius:  " << data.GetBubbleRadius() << endl;

	auto shellData = data.GetShellData();
	for(int i = 0, j = shellData.size(); i < j; i++)
		out << shellData[i] << endl;

	out << "MeanVelocity: " << data.GetMeanVelocity() << endl;
	out << "MedianVelo:   " << data.GetMedianVelocity() << endl;
	out << "TotalColli:   " << data.GetTotalCollisions() << endl;
	out << "TotCellCross: " << data.GetTotalCellCrossings() << endl;
	out << "TotWallColli: " << data.GetTotalWallCollisions() << endl;
	out << "TotBoundColl: " << data.GetTotalConeBoundaryCollisions() << endl;
	out << "FusionRate:   " << data.GetFusionRate() << endl;

	return out;
}