// ParticleData.h - Particle data structure implementation.
// Written by Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "ParticleData.h"
using namespace DataReader;
#pragma endregion

ParticleData::ParticleData() {
	this->Position = Vector3<double>();
	this->Velocity = Vector3<double>();
	this->GasType = 0;
}

ostream& operator<<(ostream& out, ParticleData& data) {
	out << "Position { " << data.Position << " }" << endl;
	out << "Velocity { " << data.Velocity << " }" << endl;
	out << "Gas Type: " << data.GasType << endl;
	return out;
}