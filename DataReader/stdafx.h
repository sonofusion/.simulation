// stdafx.h
// Project DataReader
#ifndef __Precompiled_Header__
#define __Precompiled_Header__
#define WIN32_LEAN_AND_MEAN

// C++
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>

// Rapid JSON
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/rapidjson.h>

// Undefine max
#undef max

#endif