// RpkDataPoint.h - RPK data point structure implementation.
// Written by Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "RpkDataPoint.h"
using namespace std;
using namespace DataReader;
#pragma endregion

RpkDataPoint::RpkDataPoint() {
	this->Time = 0;
	this->Radius = 0;
	this->Velocity = 0;
	this->Pressure = 0;
	this->DeltaPressure = 0;
}

ostream& operator<<(ostream& out, RpkDataPoint& rpkDP) {
	out << "Time:           " << rpkDP.Time << endl;
	out << "WallRadius:     " << rpkDP.Radius << endl;
	out << "WallVelocity:   " << rpkDP.Velocity << endl;
	out << "Pressure:       " << rpkDP.Pressure << endl;
	out << "PressureToTime: " << rpkDP.DeltaPressure << endl;
	return out;
}