// ParticleData.h - Particle data structure declaration.
// Written by Jesse Z. Zhong
#ifndef __Particle_Data_H__
#define __Particle_Data_H__
#pragma region Includes
#include <ostream>
#include "Vector3.h"
using namespace std;
#pragma endregion
namespace DataReader {
	/*! 
	  Stores a single particle data point.
	*/
	struct ParticleData {
		
		Vector3<double> Position;	//!< The position of the particle.
		Vector3<double> Velocity;	//!< The velocity of the particle.
		int GasType;				//!< The gas type of the particle.

		/*! 
		  Constructor
		*/
		ParticleData();
	};
}

/*! 
  Out stream operator overload for particle data.
*/
ostream& operator<<(ostream& out, DataReader::ParticleData& data);

#endif // !__Particle_Data_H__
