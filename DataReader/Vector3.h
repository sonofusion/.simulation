// Vector3.h - Vector 3D Struct Declaration
// Written By Jesse Z. Zhong
#ifndef __Vector_3D_H__
#define __Vector_3D_H__
#pragma region Includes
#include "stdafx.h"
using namespace std;
#pragma endregion
namespace DataReader {
	/*! 
	  3D Vector Structure
	*/
	template<typename DATA_TYPE = double>
	struct Vector3 {

		/*! 
		  Copy Constructor
		*/
		Vector3(const Vector3<DATA_TYPE>& source);

		/*! 
		  Hybrid Constructor
		*/
		Vector3(DATA_TYPE x = 0, 
			DATA_TYPE y = 0, DATA_TYPE z = 0);

		// The scalar components of a vector.
		DATA_TYPE X, Y, Z;

		/*! 
		  Returns a Zeroed Vector
		*/
		static Vector3 Zero();

		/*! 
		  Assignment operator overload.
		*/
		const Vector3& operator=(const Vector3& source);

		/*! 
		  Addition operator overload.
		*/
		const Vector3& operator+(const Vector3& operand);
		
		/*! 
		  Subtraction operator overload.
		*/
		const Vector3& operator-(const Vector3& operand);
	};

	// Out stream operator overload for vector 3D.
	template<typename T>
	ostream& operator<<(ostream& out, const Vector3<T>& data) {
		out << data.X << ", " << data.Y << ", " << data.Z;
		return out;
	}

	template<typename DATA_TYPE>
	Vector3<DATA_TYPE>::Vector3(const Vector3<DATA_TYPE>& source) {
		X = source.X;
		Y = source.Y;
		Z = source.Z;
	}

	template<typename DATA_TYPE>
	Vector3<DATA_TYPE>::Vector3(DATA_TYPE x, DATA_TYPE y, DATA_TYPE z) {
			X = x;
			Y = y;
			Z = z;
	}

	template<typename DATA_TYPE>
	static Vector3<DATA_TYPE> Vector3<DATA_TYPE>::Zero() {
		return Vector3();
	}

	template<typename DATA_TYPE>
	const Vector3<DATA_TYPE>& Vector3<DATA_TYPE>::operator=(const Vector3& source) {
		X = source.X;
		Y = source.Y;
		Z = source.Z;
		return *this;
	}

	template<typename DATA_TYPE>
	const Vector3<DATA_TYPE>& Vector3<DATA_TYPE>::operator+(const Vector3& operand) {
		X += operand.X;
		Y += operand.Y;
		Z += operand.Z;
		return *this;
	}

	template<typename DATA_TYPE>
	const Vector3<DATA_TYPE>& Vector3<DATA_TYPE>::operator-(const Vector3& operand) {
		X -= operand.X;
		Y -= operand.Y;
		Z -= operand.Z;
		return *this;
	}
}
#endif // End : Vector3D