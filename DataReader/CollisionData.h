// CollisionData.h - Collision data structure declaration.
// Written by Jesse Z. Zhong
#ifndef __Collision_Data_H__
#define __Collision_Data_H__
#pragma region Includes
#include <iostream>
#include "Vector3.h"
#pragma endregion
namespace DataReader {

	/*! 
	  Stores particle collision data.
	*/
	struct CollisionData {

		double Temperature;			//!< The temperature of the resulting collision.
		Vector3<double> Position;	//!< The position at which the collision occurs.
		double DeltaVelocity;		//!< The change in velocity that occurs at the collision point.
		double Energy;				//!< The expelled energy of the resulting collision.
		double Distance;			//!< The distance between the particles before they collide in the next step.
		int Gas1Type;				//!< The gas type of the first colliding particle.
		int Gas2Type;				//!< The gas type of the second colliding particle.
		
		/*! 
		  Constructor
		*/
		CollisionData();
	};
}

/*! 
  Out stream operator overload for collision data.
*/
ostream& operator<<(ostream& out, DataReader::CollisionData& data);

#endif // !__Collision_Data_H__
