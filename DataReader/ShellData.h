// ShellData.h - Shell data structure declaration.
// Written by Jesse Z. Zhong
#ifndef __Shell_Data_H__
#define __Shell_Data_H__
#pragma region Includes
#include <vector>
#include <ostream>
using namespace std;
#pragma endregion
namespace DataReader {
	/*! 
	  Stores the information pertaining to 
	  each shell layer in the simulated system.

	  The snapshot contains the metadata relevant to the bubble modeled in the
	  molecular dynamics simulation. The metadata is obtained by dividing the
	  bubble into shells, or layers, and calculating the data for each shell. The 
	  resulting data is then stored into the snapshot according to their respective 
	  shell layer. ShellData captures the metadata for these individual shells.
	*/
	struct ShellData {

		//! Indicates the radial position of the
		//! shell from the center of the bubble.
		double Radius;

		//! Indicates the number of 
		//! particles within this shell layer.
		int NumberOfParticles;

		//! Indicates the particle density
		//! within this shell layer of the bubble.
		double Density;

		//! Records the average temperature of the
		//! particles according to their gas type.
		vector<double> Temperature;

		//! Records the maximum temperature of the
		//! particles according to their gas type.
		vector<double> MaxTemperature;

		//! Records the average velocity of the
		//! particles according to their gas type.
		vector<double> Velocity;

		//! Records the ionization percentages of the
		//! particles according to their gas type.
		vector<double> Ionization;

		//! Indicates how much of the shell is made
		//! up by each type of gas by percentage.
		vector<double> Percent;

		//! Records the kinetic energy of the
		//! particles in the bubble
		vector<double> KineticEnergy;

		//! Records the kinetic energy '?' of the
		//! particles in the bubble
		vector<double> KineticEnergyCom;

		//! Indicates the number of types of
		//! gases found in this shell layer.
		int GasTypes;	

		/*! 
		  Constructor

		  Initializes all members.
		*/
		ShellData();
	};
}

/*! 
  Out stream operator overload for shell data.
*/
ostream& operator<<(ostream& out, DataReader::ShellData& shellData);
#endif // !__Shell_Data_H__