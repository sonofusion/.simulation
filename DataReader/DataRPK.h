// DataRPK.h - DataRPK Class Declaration
// Written By Jesse Z. Zhong
#ifndef __Data_RPK_H__
#define __Data_RPK_H__
#pragma region Includes
#include <vector>
#include "RpkDataPoint.h"
#include "DataFile.h"
using namespace std;
#pragma endregion
namespace DataReader {
	/*! 
	  Stores a set of RPK data points.
	*/
	class DataRPK : public DataFile {
	public:
		/*! 
		  Constructor
		*/
		DataRPK();

		/*! 
		  Init-Constructor
		*/
		DataRPK(const string& fileName);

		/*! 
		  Reads and stores data from an RPK file.
		*/
		virtual void Read(const string& fileName);

	#pragma region Accessors
		/*! 
		  Returns the list of RPK data.
		*/
		vector<RpkDataPoint>& GetData();
		
		/*! 
		  Returns RPK points containing members with 
		  the maximum values of the data points.
		*/
		RpkDataPoint GetMax() const;

		/*! 
		  Returns RPK points containing members with 
		  the minimum values of the data points.
		*/
		RpkDataPoint GetMin() const;
	#pragma endregion
	private:
		
		/*! 
		  Reads and parses for an RPK data point.
		*/
		RpkDataPoint ReadDataPoint(char* item);

		/*! 
		  Compares a new value with the current 
		  maximum and minimum and updates accordingly.
		*/
		void UpdateBounds(const RpkDataPoint& item);
		
		// Members
		vector<RpkDataPoint> RPKList_;	//!< Stores a list of RPK data points.
		RpkDataPoint Max_;		//!< Stores the maximum individual values across all data points.
		RpkDataPoint Min_;		//!< Stores the minimum individual values across all data points.
	};
}

/*! 
  Out stream operator overload for an RPK data set.
*/
ostream& operator<<(ostream& out, DataReader::DataRPK& data);
#endif
