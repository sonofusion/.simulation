// CollisionData.h - Collision data structure declaration.
// Written by Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "CollisionData.h"
using namespace std;
using namespace DataReader;
#pragma endregion

CollisionData::CollisionData() {
	this->Temperature = 0;
	this->Position = Vector3<double>();
	this->DeltaVelocity = 0;
	this->Energy = 0;
	this->Distance = 0;
	this->Gas1Type = 0;
	this->Gas2Type = 0;
}

ostream& operator<<(ostream& out, CollisionData& data) {
	out << "Temperature:   " << data.Temperature << endl;
	out << "Position { " << data.Position << " }" << endl;
	out << "DeltaVelocity: " << data.DeltaVelocity << endl;
	out << "Energy:        " << data.Energy << endl;
	out << "Distance:      " << data.Distance << endl;
	out << "Gas 1 Type:    " << data.Gas1Type << endl;
	out << "Gas 2 Type:    " << data.Gas2Type << endl;
	return out;
}