// DataFile.h - DataFile Class Declaration.
// Written By Jesse Z. Zhong
//! \file
#ifndef __DataFile_h__
#define __DataFile_h__
#pragma region Includes
#include "stdafx.h"
using namespace std;
#pragma endregion
/*!
  DataReader provides interfaces for parsing simulation output.
*/
namespace DataReader {
	/*! 
	  Base class for reading and storing simulation output.

	  DataFile provides a simple interface for implementing file 
	  parsers for different classes of files that are generated 
	  by the MD simulation. This base class includes items that 
	  are common to all file parsers: a file name, a read method, 
	  a read status, and a discernible file type. Get accessors 
	  for these items are also predefined in this class.
	*/
	class DataFile {
	public:
	#pragma region Constants
		//! List of delimiters that are used in files.
		static const char* Delimiters;
	#pragma endregion
		/*! 
		  Constructor

		  Initializes the file name and read status.
		  \param fileName is the name of the file whose data we are looking to store.
		*/
		DataFile(const string& fileName = "") {
			this->FileName_ = fileName;
			this->DataStored_ = false;
		}

		//! Lists the type of data the files are storing.
		enum EFileType {
			RPK,				//!< Rayleigh-Plesset predicted trend of the simulation.
			Snapshot,			//!< Periodic snapshot, in the form of metadata, of the MD simulation.
			Savepoint,			//!< Record of the position, velocity, and type of each particle in the system.
			CollisionEnergy,	//!< Record of the collision information of each particle in the system.
		};

		/*! \pure
		  Parses a file according to the
		  implementation of a derived file reader.
		*/
		virtual void Read(const string& fileName) = 0;

		//! Returns the name of the file the data was read from.
		string GetFileName() const { return this->FileName_; }

		//! Returns the type of data stored.
		EFileType GetType() const { return this->Type_; }

		//! Returns whether or not data has been stored.
		bool GetDataStored() const { return this->DataStored_; }

	protected:
		//! Name of the file that the data is read from.
		string FileName_;

		//! Indicates the type of data stored.
		EFileType Type_;

		//! Flag indicating if data has been stored.
		bool DataStored_;
	};
}

#endif // End: DataFile