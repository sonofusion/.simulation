// DataRDK.cpp - DataRDK Class Method Implementations
// Written By Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "DataRPK.h"
#include "Utilities.h"
using namespace DataReader;
namespace Utils = Utilities;
#pragma endregion

DataRPK::DataRPK() {
	this->RPKList_ = vector<RpkDataPoint>();
	this->Min_ = RpkDataPoint();
	this->Max_ = RpkDataPoint();
}

DataRPK::DataRPK(const string& fileName) 
	: DataFile(fileName) {
		this->Read(fileName);
}

void DataRPK::Read(const string& fileName) {
	try {
		// Check if the file exists and load it.
		FILE* filePtr = fopen(fileName.c_str(), "r");
		if(filePtr == NULL)
			return;

		// Record the file name.
		this->FileName_ = fileName;

		// Clear the list of RPK data points.
		this->RPKList_.clear();
	
		// Traverse the file to find the size.
		fseek(filePtr, 0, SEEK_END);
		size_t size = ftell(filePtr);
		fseek(filePtr, 0, 0);
	
		// Create a character buffer for storing the file.
		char* buffer = new char[size];
	
		// Store the file into the buffer.
		fread(buffer, size, 1, filePtr);
		fclose(filePtr);
	
		// Set pointers to the beginning and the end of a 
		// substring, where the end is marked by an end line.
		char* endOfLine = strchr(buffer, '\n');
		char* substring = buffer;

		// Skip first line (first two line is a header).
		for(int k = 0; k < 2; k++) {
			substring = endOfLine + 1;
			endOfLine = strchr(substring, '\n');
		}
	
		// Iterate through the buffer to acquire RPK data.
		do {
			// Attempt to parse substring for an RPK data point.
			RpkDataPoint item = this->ReadDataPoint(substring);
			this->RPKList_.push_back(item);
			this->UpdateBounds(item);
		
			// Update pointers with the next substring.
			substring = endOfLine + 1;
			endOfLine = strchr(substring, '\n');
		
		} while ((endOfLine != NULL) && (endOfLine != substring));
	
		// Deallocate the buffer.
		delete[] buffer;

		// Set file read status to true.
		this->DataStored_ = true;

	} catch(char* ex) {
		cout << ex << endl;
	}
}

RpkDataPoint DataRPK::ReadDataPoint(char* item) {
	RpkDataPoint data = RpkDataPoint();
	data.Time = atof(Utils::NullTest(strtok(item, Delimiters)));
	data.Radius = atof(Utils::NullTest(strtok(NULL, Delimiters)));
	data.Velocity = atof(Utils::NullTest(strtok(NULL, Delimiters)));
	data.Pressure = atof(Utils::NullTest(strtok(NULL, Delimiters)));
	data.DeltaPressure = atof(Utils::NullTest(strtok(NULL, Delimiters)));
	return data;
}

void DataRPK::UpdateBounds(const RpkDataPoint& item) {

	// Test for maximum values.
	this->Max_.Time = (this->Max_.Time > item.Time) 
		? this->Max_.Time : item.Time;
	this->Max_.Radius = (this->Max_.Radius > item.Radius) 
		? this->Max_.Radius : item.Radius;
	this->Max_.Velocity = (this->Max_.Velocity > item.Velocity)
		? this->Max_.Velocity : item.Velocity;
	this->Max_.Pressure = (this->Max_.Pressure > item.Pressure)
		? this->Max_.Pressure : item.Pressure;
	this->Max_.DeltaPressure = (this->Max_.DeltaPressure > item.DeltaPressure)
		? this->Max_.DeltaPressure : item.DeltaPressure;

	// Test for minimum values.
	this->Min_.Time = (this->Min_.Time < item.Time) 
		? this->Min_.Time : item.Time;
	this->Min_.Radius = (this->Min_.Radius < item.Radius) 
		? this->Min_.Radius : item.Radius;
	this->Min_.Velocity = (this->Min_.Velocity < item.Velocity)
		? this->Min_.Velocity : item.Velocity;
	this->Min_.Pressure = (this->Min_.Pressure < item.Pressure)
		? this->Min_.Pressure : item.Pressure;
	this->Min_.DeltaPressure = (this->Min_.DeltaPressure < item.DeltaPressure)
		? this->Min_.DeltaPressure : item.DeltaPressure;
}

vector<RpkDataPoint>& DataRPK::GetData() {
	return this->RPKList_;
}

RpkDataPoint DataRPK::GetMax() const {
	return this->Max_;
}
RpkDataPoint DataRPK::GetMin() const {
	return this->Min_;
}

ostream& operator<<(ostream& out, DataRPK& data) {
	for(int i = 0, j = data.GetData().size(); i < j; i++)
		out << i << " {\n" << data.GetData()[i] << "\n}" << endl;
	return out;
}