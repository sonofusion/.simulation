// DataSavepoint.cpp - DataSavepoint Class Method Implementations
// Written by Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "Utilities.h"
#include "DataSavepoint.h"
using namespace DataReader;
namespace Utils = Utilities;
#pragma endregion

DataSavepoint::DataSavepoint() {
	this->ParticleList_ = vector<ParticleData>();
	this->Min_ = ParticleData();
	this->Max_ = ParticleData();
}

DataSavepoint::DataSavepoint(const string& fileName) 
	: DataFile(fileName) {
		this->Read(fileName);
}

void DataSavepoint::Read(const string& fileName) {
	try {
		// Check if the file exists and load it.
		FILE* filePtr = fopen(fileName.c_str(), "r");
		if(filePtr == NULL) 
			return;

		// Record the file name.
		this->FileName_ = fileName;

		// Clear the list of data points.
		this->ParticleList_.clear();
		
		// Traverse the file to find the size.
		fseek(filePtr, 0, SEEK_END);
		size_t size = ftell(filePtr);
		fseek(filePtr, 0, 0);
		
		// Create a character buffer for storing the file.
		char* buffer = new char[size];
		
		// Store the file into the buffer.
		fread(buffer, size, 1, filePtr);
		fclose(filePtr);
		
		// Count the number of lines in the file
		// and allocate the vector with that number.
		// Note: EoF needs to be accounted for.
		//       +1 in order to make room for the EoF.
		int numOfLines = count(buffer, buffer + size, '\n');
		this->ParticleList_.reserve(numOfLines + 1);

		// Set pointers to the beginning and the end of a 
		// substring, where the end is marked by an end line.
		char* endOfLine = strchr(buffer, '\n');
		char* substring = buffer;

		// Iterate through the buffer to acquire particle data.
		do {
			// Attempt to parse substring for particle data.
			ParticleData item = this->ReadParticleData(substring);
			this->UpdateBounds(item);
			this->ParticleList_.push_back(item);
			
			// Update pointers with the next substring.
			substring = endOfLine + 1;
			endOfLine = strchr(substring, '\n');
			
		} while (endOfLine != NULL);
		
		// Deallocate the buffer.
		delete[] buffer;

		// Set file read status to true.
		this->DataStored_ = true;

	} catch(const string& error) {
		cout << error << endl;
	}
}

ParticleData DataSavepoint::ReadParticleData(char* item) {
	ParticleData data = ParticleData();
	data.Position.X = atof(Utils::NullTest(strtok(item, Delimiters)));
	data.Position.Y = atof(Utils::NullTest(strtok(NULL, Delimiters)));
	data.Position.Z = atof(Utils::NullTest(strtok(NULL, Delimiters)));
	data.Velocity.X = atof(Utils::NullTest(strtok(NULL, Delimiters)));
	data.Velocity.Y = atof(Utils::NullTest(strtok(NULL, Delimiters)));
	data.Velocity.Z = atof(Utils::NullTest(strtok(NULL, Delimiters)));
	data.GasType = atoi(Utils::NullTest(strtok(NULL, Delimiters)));
	return data;
}

void DataSavepoint::UpdateBounds(const ParticleData& item) {

	// Update maximum values.
	this->Max_.Position.X = (this->Max_.Position.X > item.Position.X) 
		? this->Max_.Position.X : item.Position.X;
	this->Max_.Position.Y = (this->Max_.Position.Y > item.Position.Y) 
		? this->Max_.Position.Y : item.Position.Y;
	this->Max_.Position.Z = (this->Max_.Position.Z > item.Position.Z) 
		? this->Max_.Position.Z : item.Position.Z;
	this->Max_.Velocity.X = (this->Max_.Velocity.X > item.Velocity.X) 
		? this->Max_.Velocity.X : item.Velocity.X;
	this->Max_.Velocity.Y = (this->Max_.Velocity.Y > item.Velocity.Y) 
		? this->Max_.Velocity.Y : item.Velocity.Y;
	this->Max_.Velocity.Z = (this->Max_.Velocity.Z > item.Velocity.Z) 
		? this->Max_.Velocity.Z : item.Velocity.Z;

	// Update minimum values.
	this->Min_.Position.X = (this->Min_.Position.X < item.Position.X) 
		? this->Min_.Position.X : item.Position.X;
	this->Min_.Position.Y = (this->Min_.Position.Y < item.Position.Y) 
		? this->Min_.Position.Y : item.Position.Y;
	this->Min_.Position.Z = (this->Min_.Position.Z < item.Position.Z) 
		? this->Min_.Position.Z : item.Position.Z;
	this->Min_.Velocity.X = (this->Min_.Velocity.X < item.Velocity.X) 
		? this->Min_.Velocity.X : item.Velocity.X;
	this->Min_.Velocity.Y = (this->Min_.Velocity.Y < item.Velocity.Y) 
		? this->Min_.Velocity.Y : item.Velocity.Y;
	this->Min_.Velocity.Z = (this->Min_.Velocity.Z < item.Velocity.Z) 
		? this->Min_.Velocity.Z : item.Velocity.Z;
}

vector<ParticleData>& DataSavepoint::GetData() {
	return this->ParticleList_;
}

ParticleData DataSavepoint::GetMax() const {
	return this->Max_;
}

ParticleData DataSavepoint::GetMin() const {
	return this->Min_;
}

ostream& operator<<(ostream& out, DataSavepoint& data) {
	for(int i = 0, j = data.GetData().size(); i < j; i++)
		out << i << " {\n" << data.GetData()[i] << "\n}" << endl;
	return out;
}