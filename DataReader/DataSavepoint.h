// DataSavepoint.h - DataSavepoint Class Declaration
// Written by Jesse Z. Zhong
#ifndef __Data_Savepoint_H__
#define __Data_Savepoint_H__
#pragma region Includes
#include "stdafx.h"
#include "ParticleData.h"
#include "DataFile.h"
using namespace std;
#pragma endregion
namespace DataReader {
	
	/*! 
	  Stores the data from a save point data set.
	*/
	class DataSavepoint : public DataFile {
	public:
		/*! 
		  Constructor
		*/
		DataSavepoint();

		/*! 
		  Init-Constructor
		*/
		DataSavepoint(const string& fileName);

		/*! 
		  Read and stores data from a save point file.
		*/
		virtual void Read(const string& fileName);

	#pragma region Accessors
		/*! 
		  Returns the list of particle data.
		*/
		vector<ParticleData>& GetData();

		/*! 
		  Returns particle points that contain the
		  overall maximum values of the data set.
		*/
		ParticleData GetMax() const;

		/*! 
		  Returns particle points that contain the
		  overall minimum values of the data set.
		*/
		ParticleData GetMin() const;
	#pragma endregion
	private:
		/*! 
		  Reads and parses for particle data.
		*/
		ParticleData ReadParticleData(char* item);

		/*! 
		  Compares a new value with the current
		  maximum and minimum and updates accordingly.
		*/
		void UpdateBounds(const ParticleData& item);

		// Members
		vector<ParticleData> ParticleList_;		//!< Vector list of particle list.
		ParticleData Max_;				//!< Maximum measured values of particles.
		ParticleData Min_;				//!< Minimum measured values of particles.
	};
}

/*! 
  Out stream operator overload for save point data.
*/
ostream& operator<<(ostream& out, DataReader::DataSavepoint& data);
#endif // !__Data_Savepoint_H__


