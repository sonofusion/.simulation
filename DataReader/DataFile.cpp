// DataFile.cpp - DataFile Class Implementation.
// Written By Jesse Z. Zhong
#include "stdafx.h"
#include "DataFile.h"
using namespace DataReader;

// List of delimiters used in files.
const char* DataFile::Delimiters = " ,\n";