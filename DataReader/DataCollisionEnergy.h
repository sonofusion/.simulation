// DataCollisionEnergy.h - Collision Energy Class Declaration
// Written By Jesse Z. Zhong
#ifndef __Data_Collision_Energy_H__
#define __Data_Collision_Energy_H__
#pragma region Includes
#include <vector>
#include "CollisionData.h"
#include "DataFile.h"
using namespace std;
#pragma endregion
namespace DataReader {
	/*! 
	  Stores a set of collision energy data.
	*/
	class DataCollisionEnergy : public DataFile {
	public:
		
		/*! 
		  Constructor
		*/
		DataCollisionEnergy();

		/*! 
		  Init-Constructor
		*/
		DataCollisionEnergy(const string& fileName);
		
		/*! 
		  Reads and stores data from a collision energy file.
		*/
		virtual void Read(const string& fileName);
		
	#pragma region Accessors
		/*! 
		  Returns a list of collision data.
		*/
		vector<CollisionData>& GetData();
 
		/*! 
		  Returns collision data that contains the 
		  overall maximum values of the data set.
		*/
		CollisionData GetMax() const;

		/*! 
		  Returns collision data that contains the 
		  overall minimum values of the data set.
		*/
		CollisionData GetMin() const;
	#pragma endregion
	private:
		/*! 
		  Reads and parses for collision energy data.
		*/
		CollisionData ReadCollisionData(char* item);

		/*! 
		  Compares a new value with the current
		  maximum and minimum and updates accordingly.
		*/
		void UpdateBounds(const CollisionData& item);

		// Members
		vector<CollisionData> CollisionList_;	//!< Stores a list of collision data.
		CollisionData Max_;		//!< Maximum individual values accross all data points.
		CollisionData Min_;		//!< Minimum individual values accross all data points.
	};
}

/*! 
  Out stream operator overload for collision energy data.
*/
ostream& operator<<(ostream& out, DataReader::DataCollisionEnergy& data);
#endif
