// RpkDataPoint.h - RPK data point structure declaration.
// Written by Jesse Z. Zhong
#ifndef __RPK_Data_Point_H__
#define __RPK_Data_Point_H__
#include <iostream>
using namespace std;
namespace DataReader {

	/*! 
	  Stores a single data point of an RPK set of values.
	*/
	struct RpkDataPoint {
		
		double Time;			//!< The time elapsed in the simulation.
		double Radius;			//!< The radius from the center of the bubble to the wall.
		double Velocity;		//!< The velocity at which the bubble wall is expanding or compressing.
		double Pressure;		//!< The pressure measured within the bubble.
		double DeltaPressure;	//!< The change in pressure of the bubble.

		/*! 
		  Constructor
		*/
		RpkDataPoint();
	};
}

/*! 
  Out stream operator overload for RPK data points.
*/
ostream& operator<<(ostream& out, DataReader::RpkDataPoint& rpkDP);

#endif // !__RPK_Data_Point_H__
