// DataSnapshot.h - DataSnapshot class declaration.
// Written By Jesse Z. Zhong
//! \file
#ifndef __Data_Snapshot_H__
#define __Data_Snapshot_H__
#pragma region Includes
#include "stdafx.h"
#include "ShellData.h"
#include "DataFile.h"
#include "..\MDSimulation\Constants\AtomicProperties.h"
using namespace std;
using namespace rapidjson;
#pragma endregion
namespace DataReader {
	/*! 
	  Stores simulation snapshot data.
	*/
	class DataSnapshot : public DataFile {
	public:
		/*! \name Snapshot Keys
		  The following is a list of all of the keys
		  used by the read method to look up values
		  within the JSON formatted snapshot files.
		*/
	#pragma region Snapshot Keys
		//! \{
		static const char* SS_SnapshotID;		//!< Snapshot ID
		static const char* SS_SimTime;			//!< Simulation time
		static const char* SS_Radius;			//!< Radius
		static const char* SS_Bins;				//!< Shell data
		static const char* SS_ParticleCount;    //!< Particle count
		static const char* SD_Radius;			//!< Shell radius
		static const char* SD_Particles;		//!< Shell particles
		static const char* SD_Density;			//!< Shell density
		static const char* SD_Temperature;		//!< Shell temperature
		static const char* SD_MaxTemperature;	//!< Shell temperature
		static const char* SD_Velocity;			//!< Shell velocity
		static const char* SD_Ionization;		//!< Shell ionization
		static const char* SD_Percentage;		//!< Shell gas percentages
		static const char* SD_KineticEnergy;    //!< Shell kinetic energy
		static const char* SD_KineticEnergyCom; //!< Shell center of mass kinetic energy 
		//! \}
	#pragma endregion
	#pragma region Instance
		/*! 
		  Constructor

		  Initializes members and attempts to 
		  read from a file if a file name is passed.
		*/
		DataSnapshot(const string& fileName = "");

		/*! 
		  Parses and stores data from a snapshot file.
		*/
		virtual void Read(const string& fileName);
	#pragma endregion
	#pragma region Accessors
		//! Returns the snapshot ID. \see SnapshotID_
		int GetSnapshotID() const;

		//! Returns the simulation time. \see SimulationTime_
		double GetSimulationTime() const;

		//! Returns the bubble radius. \see BubbleRadius_
		double GetBubbleRadius() const;

		//! Returns the average velocity. \see MeanVelocity_
		double GetMeanVelocity() const;

		//! Returns the median velocity. \see MedianVelocity_
		double GetMedianVelocity() const;

		//! Returns the total number of collisions. \see TotalCollisions_
		unsigned long long GetTotalCollisions() const;

		//! Returns the total number of cell crossings. \see TotalCellCrossings_
		unsigned long long GetTotalCellCrossings() const;

		//! Returns the total number of wall collisions. \see TotalWallCollisions_
		unsigned long long GetTotalWallCollisions() const;

		//! Returns the total number of cone boundary collisions. \see TotalConeBoundaryCollisions_
		unsigned long long GetTotalConeBoundaryCollisions() const;

		//! Returns the rate at which fusion occurs. \see FusionRate_
		double GetFusionRate() const;

		//! Returns the number of gas types. \see GasTypes_
		int GetGasTypes() const;

		//! Returns the list of shell data.
		vector<ShellData>& GetShellData();

		//! Returns the wall velocity. \see WallVelocity_
		double GetWallVelocity() const;

		//! Returns the pressure. \see Pressure_
		double GetPressure() const;

		//! Returns the maximum temperature. \see MaxTemperature_
		double GetMaxTemperature() const;

		//! Returns the average temperature. \see AvgTemperature_
		double GetAvgTemperature() const;

		//! Returns the maximum energy level. \see MaxEnergy_
		double GetMaxEnergy() const;

		//! Returns the average energy level. \see AvgEnergy_
		double GetAvgEnergy() const;

		//! Returns the maximum density measured. \see MaxDensity_
		double GetMaxDensity() const;

		//! Returns the average density measured. \see AvgDensity_
		double GetAvgDensity() const;

		//! Returns the kinetic energy measured. \see KineticEnergy_
		double GetKineticEnergy() const;

		//! Returns the kinetic energy '?' measured. \see KineticEnergyCom_
		double GetKineticEnergyCom() const;

		//! Returns the particle count measured. \see ParticleCount_
		int GetParticleCount() const;

		//! Returns the number of ionizations, where the index is the gas type \see Ionizations_
		vector<int> GetIonizations() const;
	#pragma endregion
	protected:
	#pragma region Helper Methods
		// Reads and parses shell data.
		ShellData ReadShellData(Value& item);
	#pragma endregion
	#pragma region Data Members
		//! The numerical identification by which this
		//! snapshot can be recognized and ordered by.
		int SnapshotID_;

		//! The time at which this snapshot was taken.
		double SimulationTime_;

		//! The radius, measured from the center
		//! of the gas bubble to the bubble wall.
		double BubbleRadius_;

		//! The average velocity of the particles.
		double MeanVelocity_;

		//! The median velocity of the particles.
		double MedianVelocity_;

		//! The number of active particle collisions.
		unsigned long long TotalCollisions_;

		//! [details pending]
		unsigned long long TotalCellCrossings_;

		//! [details pending]
		unsigned long long TotalWallCollisions_;

		//! [details pending]
		unsigned long long TotalConeBoundaryCollisions_;

		//! [details pending]
		double FusionRate_;

		//! The list of shells that the bubble is composed of.
		vector<ShellData> ShellData_;

		//! The number of gas types in this snapshot.
		int GasTypes_;

		// Metadata fields.

		//! The velocity at which the wall is 
		//! traveling in relations to the origin.
		double WallVelocity_;

		//! The average pressure throughout
		//! the bubble during this snapshot.
		double Pressure_;

		//! The maximum temperature measured
		//! within the bubble during this snapshot.
		double MaxTemperature_;

		//! The average temperature throughout
		//! the bubble during this snapshot.
		double AvgTemperature_;

		//! The maximum energy level measured
		//! within the bubble during this snapshot.
		double MaxEnergy_;

		//! The average energy level throughout
		//! the bubble during this snapshot.
		double AvgEnergy_;

		//! The maximum density measured within
		//! the bubble during this snapshot.
		double MaxDensity_;

		//! The average density throughout
		//! the bubble during this snapshot.
		double AvgDensity_;

		//! The kinetic energy of particles in
		//! the bubble during this snapshot
		double KineticEnergy_;

		//! The kinetic energy '?' of particles in
		//! the bubble during this snapshot
		// TODO This variable is currently unused
		double KineticEnergyCom_;

		//! The current particle count during this snapshot
		int ParticleCount_;

		//! Number of ionizations
		//! during this snapshot
		// TODO decide whether to keep this
		vector<int> Ionizations_;
	#pragma endregion
	};
}

/*! 
  Out stream operator overload for snapshots.
*/
ostream& operator<<(ostream& out, DataReader::DataSnapshot& data);
#endif// !__Data_Snapshot_H__
