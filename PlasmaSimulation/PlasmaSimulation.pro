# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

TEMPLATE = app
TARGET = PlasmaSimulation
DESTDIR = ../Debug
CONFIG += debug console
DEFINES += _CONSOLE
INCLUDEPATH += .
LIBS += -L"."
DEPENDPATH += .
MOC_DIR += ./GeneratedFiles/debug
OBJECTS_DIR += debug
UI_DIR += ./GeneratedFiles
RCC_DIR += ./GeneratedFiles
