// ProjectNames.h
// Written by Jesse Z. Zhong
#ifndef __Project_Names_H__
#define __Project_Names_H__
#include <QString>
namespace Utilities {
	/*! 
	  Program Titles
	*/

	// The organization that is sponsoring the project.
	const QString OrganizationName = "Quantum Potential Corporation";

	// The name of the software package.
	const QString ApplicationName = ".Simulation";


	/*! 
	  File Names
	*/

	// File name of the RP guess.
	const QString RPGuess2Name = "RPguess2.dat";

	// File name of the RP actual.
	const QString RPActualName = "RPactual.dat";

}
#endif // !__Project_Names_H__