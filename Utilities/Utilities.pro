# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------
cache()
TEMPLATE = lib
TARGET = Utilities
DESTDIR = ./
QT += core opengl widgets gui printsupport
CONFIG += staticlib
DEFINES += QT_LARGEFILE_SUPPORT QT_OPENGL_LIB
INCLUDEPATH += ./GeneratedFiles \
    . \
    ../Dependencies/RapidJSON/include
LIBS += -L"."
DEPENDPATH += .
UI_DIR += ./GeneratedFiles
RCC_DIR += ./GeneratedFiles

debug {
    CONFIG += debug
    OBJECTS_DIR += debug
    INCLUDEPATH += ./GeneratedFiles/Debug
    MOC_DIR += ./GeneratedFiles/Debug
}

release {
    CONFIG += release
    OBJECTS_DIR += release
    INCLUDEPATH += ./GeneratedFiles/Release
    MOC_DIR += ./GeneratedFiles/Release
}

QMAKE_CXXFLAGS += -mmacosx-version-min=10.7 -std=c++0x

include(Utilities.pri)
