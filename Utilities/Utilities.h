// Util.h - Util Class Declaration.
// Written By Jesse Z. Zhong
#ifndef __Utilities_h__
#define __Utilities_h__
#pragma region Includes
#include <string>
#include <vector>
#include <iostream>

// Qt
#include <QRect>
#include <QPoint>

// Rapid JSON
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/rapidjson.h>
using namespace std;
using namespace rapidjson;

// All encompassing include section.
#include "ProjectNames.h"
#pragma endregion
// List of useful functions.
namespace Utilities {
#pragma region Debug
	// Prints a message in debug mode.
	static inline void Print(const string& message) {
		// Only prints if the application is in debug mode.
		#ifdef _DEBUG
			cout << message << endl;
		#endif
	}
#pragma endregion
#pragma region Constants
	// Golden Ratio
	const double PHI = 1.6180339887;

	// Pi
	const double PI = 3.14159265359;

	// Avogadro's Number
	const double AVOGADRO = 6.02214129e+23;

	// Gas Constant
	const double GAS_CONSTANT = 8.3144621;
#pragma endregion
#pragma region Cone Volume
	//! Returns the radian value of the degrees passed in
	static inline double DegreesToRadians( double degrees ){ return ((degrees * PI) / 180); }

	/*! Returns the volume of a conical section of a sphere, given the radius of the cone (rho) and
	    the angle of the cone in degrees (phi)
		@param[in]	radius				Radius of the cone
		@param[in]	coneAngle			Angle of the cone
		@param[in]	useRadians			if coneAngle is in radians, use true; if coneAngle is in degrees, use false
		\return	Returns the volume of a conical section of a sphere
	*/
	static inline double ConicalSection( double radius, double coneAngle, bool coneAngleIsInRadians ){

		double localConeAngle = 0;

		// If the coneAngle is not in radians, convert it from degrees to radians
		if( !coneAngleIsInRadians ){
			localConeAngle = DegreesToRadians( coneAngle );
		} else {
			localConeAngle = coneAngle;
		}

		double integrationPart1 = 0;
		double integrationPart2 = 0;
		double integrationFinal = 0;

		// Used Wolfram Alpha integration to produce an equation for the volume of 
		//    the conical section of a bubble

		integrationPart1 = ( -4.0 / 15.0 ) 
						   * pow( radius, 5 )
						   * pow( sin(localConeAngle / 2), 4)
						   * ( cos(localConeAngle) + 2 );

		integrationPart2 = 3.0
						   * pow( radius, 3 )
						   * ( cos(localConeAngle) - 1 );

		integrationFinal = 2 * PI * (integrationPart1 - integrationPart2);

		return integrationFinal;
	}
#pragma endregion
#pragma region File Parsing
	// Creates an Array Out of Non-Array 
	// Document Values from Rapid JSON
	template<class T>
	vector<T> ParseAsArray(const Value& values) {

		// Create Return Vector
		vector<T> tempVector;

		// Iterate through the Values and Store int the Vector
		for(Value::ConstMemberIterator it = values.MemberBegin();
			it != values.MemberEnd(); it++) {

				// Check for T's Type

				// Double
				if((typeid(T) == typeid(double)) && it->value.IsDouble())
					tempVector.push_back(it->value.GetDouble());

				// Signed Integer
				else if(((typeid(T) == typeid(int)) || (typeid(T) == typeid(double))) && it->value.IsInt())
					tempVector.push_back((T)it->value.GetInt());

				// Unsigned Integer
				else if((typeid(T) == typeid(unsigned int)) && it->value.IsUint())
					tempVector.push_back(it->value.GetUint());

				// Unsigned Long Long
				else if((typeid(T) == typeid(unsigned long long)) && it->value.IsUint64())
					tempVector.push_back(it->value.GetUint64());

				// TODO: Add More Type Support; Type Support for Value Object
		} 

		// Return the Vector
		return tempVector;
	}

	// Test if a c-array is null; returns the c-array if it
	// is not null and returns a new zero array if it is null.
	static inline char* NullTest(char* cArray) {
		if(cArray != NULL)
			return cArray;
		return new char('0');
	}
#pragma endregion
#pragma region Mouse Bounds Checking Against UI
	// Check if a point's x component is within bounds.
	static inline bool CheckBoundariesX(const QPoint& point, const QRect& rectangle) {

		// Localize data.
		int width = rectangle.width();
		int pointX = point.x();
		int rectX = rectangle.x();

		// Create an instance of a bound test result.
		return (width < 0) ? ((pointX < rectX) && (pointX >= width)) 
			: ((pointX >= rectX) && (pointX <= width));
	}

	// Check if a point's y component is within bounds.
	static inline bool CheckBoundariesY(const QPoint& point, const QRect& rectangle) {

		// Localize data.
		int height = rectangle.height();
		int pointY = point.y();
		int rectY = rectangle.y();

		// Create an instance of a bound test result.
		return (height < 0) ? ((pointY < rectY) && (pointY >= height)) 
			: ((pointY >= rectY) && (pointY <= height));
	}

	// Method used to check if a point is within a certain rectangle's area.
	static inline bool CheckBoundaries(const QPoint& point, const QRect& rectangle) {
		return CheckBoundariesX(point, rectangle) && CheckBoundariesY(point, rectangle);
	}

	// Method used to check if a point is within a certain rectangle's area
	// and correct it if it exceeds any of the boundaries of the rectangle.
	static inline QPoint& CheckPoint(const QPoint& point, const QRect& rectangle) {

		// Create a copy of the point.
		QPoint* newPoint = new QPoint(point);

		// Check along the x axis.
		if(point.x() < rectangle.x())
			newPoint->setX(rectangle.x());
		else {
			int upperBound = 0;
			if(point.x() > (upperBound = (rectangle.x() + rectangle.width())))
				newPoint->setX(upperBound);
		}

		// Check along the y axis.
		if(point.y() < rectangle.y())
			newPoint->setY(rectangle.y());
		else {
			int upperBound = 0;
			if(point.y() > (upperBound = (rectangle.y() + rectangle.height())))
				newPoint->setY(upperBound);
		}

		// Return the copy.
		return *newPoint;
	}
#pragma endregion
}
#endif // !__Utilities_h__