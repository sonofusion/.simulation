// Scheduler.h - SchedulerGUI main window declaration.
// Written by Jesse Z. Zhong & Nicholas Kriner
#ifndef __Scheduler_GUI_H__
#define __Scheduler_GUI_H__
#pragma region Includes
#include "stdafx.h"
#include "ui_SchedulerGUI.h"

#include "rapidjson/document.h"
#include "rapidjson/filestream.h"
#include "rapidjson/prettywriter.h"

#include <qmessagebox.h>
#include <qsettings.h>
#include <qdialogbuttonbox.h>
#include <qpushbutton.h>
#include <qdatetime.h>

#include <GasCombo.h>
#include <Utilities.h>
#include <ProjectNames.h>
#include <SimulationWorker.h>
#include <SimulationController.h>
#include <ChangeConfirmation.h>

#include <limits>

#include <InfoStruct\InfoStruct.h>
#include <Constants\AtomicProperties.h>
using namespace std;
using namespace MDSimulation;
#pragma endregion

// TODO: - Possibly add the ability to revert specific changes


/*!
  SchedulerGUI is a user interface that allows users
  to scheduler and run molecular dynamics simulations.
*/
namespace SchedulerGUI {

	//! Used by ChangeConfirmation to signify which action the user wishes to take
	enum Options {Accept, Reject, DoNothing};

	/*! 
	  The main window of Scheduler.
	*/
	class Scheduler : public QMainWindow {
		Q_OBJECT
	public:
	#pragma region Constants
		//! The display name of the application.
		static const QString Scheduler::ModuleName;

		//! Name of the Registry entry with the name of the most recently edited file.
		static const QString Scheduler::MostRecentlyUsedFile;

		//! Name of the Registry entry with the name of the most recently used directory.
		static const QString Scheduler::MostRecentDirectory;

		//! Blank spaces. Used for formatting.
		static const string EmptySpaces;
	#pragma endregion
		/*! 
			Constructor

			Sets up the UI elements.
			Also makes sure that there is a module name, organization name, and that everything is initialized.
		*/
		Scheduler();
		
		/*! 
			Destructor
		*/
		~Scheduler();

	protected slots:
	#pragma region Protected Slots
		/*! 
		  Sets the directory to which simulation output will be written to.
		*/
		void SetOutputDir();

		/*! 
		  Prompts the user for a parameter file and loads the data from it to the UI.
		*/
		void LoadParameters();

		/*! 
		  Checks to see if there was a file used before, loads it if there was.
		*/
		void LoadRecentFile();

		/*! 
		  Writes the parameters set in the UI to a file at a location specified by the user.
		*/
		void SaveParameters();
		
		/*! 
		  Called when a file is loaded and the UI needs to be changed.

		  When loading a file, Scheduler::InfoStruct_ should be set beforehand.
			  i.e.
				  this->InfoStruct_ = this->InfoStruct_.ParseInfoStruct( fileName );
				  this->UpdateUI();
		  Parameter, Liquid, Driver and Gas boxes are updated based on the values in the InfoStruct_
		*/
		void UpdateUI();

		/*! 
		  Disables frequency editing if the new mode is Constant.
		*/
		void UpdateDriverMode(const QString& newMode);

		/*! 
		  Called whenever a GasSpinBox is edited. Sets bool GasCompCorrect_ to 
		  true if the sum of GasSpinBox values == 1.0; else it is set to false.
		*/
		void UpdateGasComp();

		
		/*! 
		  Called whenever a parameter is edited. If all parameters have valid data,
		  bool AllBoxesFilled_ is set to true; otherwise, it stays (or becomes) false

		  Checks to see if the parameter entered into the lineEdit is valid.
		  If it is, keep it; otherwise, warn the user and empty it.
		  Also checks to see if all boxes have valid data, if they do, 
		  AllBoxesFilled_ is set to true.
		*/   
		void UpdateParameters();

		/*! 
		  Changes the text in the output directory line edit.
		*/
		void ChangeOutDirBox(const QString& dirPath);

		/*! 
		  Starts or stops the simulation.
		*/
		void ToggleRunSim();

		/*! 
		  Plays or pauses an initialized simulation.
		*/
		void TogglePlaySim();
		
		/*! 
		  Toggles the simulation start button between two different strings of 
		  text: "Start" and "Stop" depending on the running state of the simulation.
		*/
		void ToggleStartButtonName(const bool state);
		
		/*! 
		  Toggles the simulation play button so that switches 
		  icons/images depending on the state the simulation is running in.
		*/
		void TogglePlayButtonName(const bool state);

		/*! 
		  Writes to the log box in the UI.
		*/
		void WriteToLog(const QString& message);
	#pragma endregion
	protected:
	#pragma region Initialization and Helper Methods
		/*! 
		  Initializes the mapping of symbols->full element names.
		*/
		void InitMapList();
		
		/*! 
		  Initializes the Atomic Parts list; adds all elements to ComboBoxes and links
		  ComboBoxes and SpinBoxes to relevant signals in the Scheduler UI.
		*/
		void InitGasComboBoxes();
		
		/*! 
		  Initializes the Liquid ComboBox so that it has the list of liquids
		*/
		void InitLiquidComboBox();
		
		/*! 
		  Connects widgets in the UI to relevant signals.
		*/
		void ConnectWidgets();
		
		/*! 
		  Checks the symbol->fullname mapping; if symbol has a full
		  name pairing, return the fullname; else, return the symbol.
		*/
		QString FullName(const string& symbol);
		
		/*! 
		  Checks to make sure that a parameter box has a valid (for the localization) double value.
		*/
		bool ValidParam(QLineEdit &paramBox);
		
		/*! 
		  Resets the Gas ComboBox + SpinBox combo to default, 0-values.
		*/
		void ResetGasComboBoxes();
		
		/*! 
		  Checks two InfoStructs to see if any values are different. Returns true if there is a difference.
		*/
		bool InfoStructDifference(InfoStruct structA, InfoStruct structB);
		
		/*!
		  Produces a UI window that displays the differences between the previous values and current values.
		*/
		int ShowInfoStructDifference(InfoStruct structA, InfoStruct structB);
		
		/*!
		  Returns the values in the GUI in the form of an InfoStruct

		  This is mainly intended to test if InfoStruct_ is equal to the current parameters (before running
		  the simulation or before saving). If you need an InfoStruct, force the user to save first and
		  perform data validation (e.g. making sure the gases add up to 1.0) in the SaveParameters() function,
		  then use InfoStruct_ (as it is updated after saves or loads and should have valid data. The struct 
		  returned by this function *is not* guaranteed to be valid and can crash the simulation).
		 */
		InfoStruct GUIToInfoStruct();
		
		/*! 
		  Returns true if the difference between two double values is larger than a given
		  epsilon value, using a constant epsilon value.
		*/
		bool DoubleChanged(double a, double b);
	#pragma endregion
	#pragma region UI Internals
		/*! 
		  Settings for use in the scheduler's saving and loading of parameters.

		  Utilizes \a Scheduler::ApplicationName as the application name and
		  \a Scheduler::OrganizationName as the company name
		*/
		QSettings* SchedulerSettings_;

		// Members
		SimulationController* SimControl_;	//!< Used to create, run, and control a simulation instance.
		map<string, QString> FullGasNames_;	//!< Map of elements' symbols->fullnames.
		vector<GasCombo> GasList_;			//!< Vector list of the gas Combo/Spin boxes.
		bool GasCompCorrect_;	//!< True if the gases add up to 1.0, false otherwise.
		bool AllBoxesFilled_;	//!< True if all boxes have valid data, false otherwise.
		QString LogString_;		//!< String used for writing messages to and forwarding to the UI's action log box.
		Ui::SchedulerGUI UI_;	//!< Instance of the UI/form.
		InfoStruct InfoStruct_;	//!< Parameters object used to read and write variables to/from .json files.
	#pragma endregion
		friend class ChangeConfirmation;
	};
}
#endif // !__Scheduler_GUI_H__