// stdafx.h
// Project SchedulerGUI
#ifndef __Precompiled_Header__
#define __Precompiled_Header__
#define WIN32_LEAN_AND_MEAN

// Qt Libraries
#include <QMainWindow>
#include <QObject>
#include <QDebug>
#include <QString>
#include <QFileDialog>
#include <QScrollBar>
#include <QMessageBox>

// C++ Libraries
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <ctime>
#include <time.h>

// JSON Parser
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/rapidjson.h>

#endif