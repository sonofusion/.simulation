// ChangeConfirmation.cpp - ChangeConfirmationGUI main window implementation.
// Written by Nicholas Kriner
#pragma region Includes
#include "stdafx.h"
#include "ChangeConfirmation.h"
using namespace SchedulerGUI;
#pragma endregion
#pragma region Constants
// Blank spaces. Used for formatting. Must be as long as the longest parameter name.
//                                                  "EquilibriumRadius"
const string ChangeConfirmation::EmptySpaceNames  = "                   ";
// Blank spaces. Used for formatting. Must be as long as the longest parameter value.
//                                                  "0.000475556"
const string ChangeConfirmation::EmptySpaceValues = "            ";
#pragma endregion
#pragma region Constructor/Destructor
ChangeConfirmation::ChangeConfirmation(InfoStruct oldStruct, InfoStruct newStruct, int *changeAccepted){

	// Create a pointer to the boolean passed in;
	//    the original value will be set to true if the user accepts changes, false otherwise
	this->Accept_ = changeAccepted;

	// Set up the UI & Widgets
	this->UI_.setupUi(this);
				
	// Connect signals and slots.
	this->ConnectWidgets();

	// Produces the GUI
	ShowInfoStructDifference(oldStruct, newStruct);
}

ChangeConfirmation::~ChangeConfirmation(){

}
#pragma endregion
#pragma region Protected Slots
void ChangeConfirmation::AcceptChanges(){
	*(this->Accept_) = Accept;
	this->close();
}

void ChangeConfirmation::RevertChanges(){
	*(this->Accept_) = Reject;
	this->close();
}

void ChangeConfirmation::CancelSaving(){
	*(this->Accept_) = DoNothing;
	this->close();
}

#pragma endregion
#pragma region Initialization and Helper Methods
/*! 
  Checks to see which values in two InfoStructs are different.

*/
void ChangeConfirmation::ShowInfoStructDifference(InfoStruct structA, InfoStruct structB){

	QListWidgetItem*        tempItem;		// Used to add to the list of changes.
	QList<QListWidgetItem*> tempList;       // List of changes in item format
	
	QString elemName = "";					// Element names; used when displaying differences
											//    in the gas list

	QString driverA = 0;                    // InfoStruct A's driver
	QString driverB = 0;                    //            B's driver
	QString liquidA = 0;                    //            A's liquid
	QString liquidB = 0;                    //            B's liquid

	// 0th entry is the value of InfoStruct A, 1st is the value of InfoStruct B
	double ConeAngle[2]                = {structA.ConeAngle,           structB.ConeAngle};
	double Temperature[2]              = {structA.Temperature,         structB.Temperature};
	double WallTemp[2]                 = {structA.WallTemperature,     structB.WallTemperature};
	double EquilibriumRadius[2]        = {structA.AmbientRadius,       structB.AmbientRadius};
	double DriverPressure[2]           = {structA.DriverPressure,      structB.DriverPressure};
	double Frequency[2]                = {structA.Frequency,           structB.Frequency};
	double InitialTime[2]			   = {structA.InitialTime,         structB.InitialTime};
	double InitialRadius[2]            = {structA.InitialRadius,       structB.InitialRadius};
	double InitialVelocity[2]          = {structA.InitialVelocity,     structB.InitialVelocity};
	double AmbientPressure[2]          = {structA.AmbientPressure,     structB.AmbientPressure};
	double AdiabaticRadius[2]          = {structA.AdiabaticRadius,     structB.AdiabaticRadius};
	double IsothermalRadius[2]         = {structA.IsothermalRadius,    structB.IsothermalRadius};
	int LiquidType[2]                  = {structA.LiquidType,          structB.LiquidType};
	DriverMode Driver[2]               = {structA.Driver,              structB.Driver};
	std::vector<double> AtomicParts[2] = {structA.AtomicParts,         structB.AtomicParts};

	/*
		Goes through each item in the InfoStruct and sees if there is a difference between the two.
		If there is, then signify a change by adding relevant info (name of what was changed,
		original value, and new value) to the list of changes. Consolas is used as it is
		fixed-width and can be aligned with ease.
	*/
	if(DoubleChanged(ConeAngle[0], ConeAngle[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("ConeAngle",
													   ConeAngle[0],
													   ConeAngle[1]));
		tempList.append(tempItem);
	}

#pragma region Additional Value Comparisons
	if(DoubleChanged(Temperature[0], Temperature[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("Temperature",
													   Temperature[0],
													   Temperature[1]));
		tempList.append(tempItem);
	}

	if(DoubleChanged(WallTemp[0], WallTemp[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("WallTemp",
													   WallTemp[0],
													   WallTemp[1]));
		tempList.append(tempItem);
	}

	if(DoubleChanged(EquilibriumRadius[0], EquilibriumRadius[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("EquilibriumRadius",
													   EquilibriumRadius[0],
													   EquilibriumRadius[1]));
		tempList.append(tempItem);
	}

	if(DoubleChanged(DriverPressure[0], DriverPressure[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("DriverPressure",
													   DriverPressure[0],
													   DriverPressure[1]));
		tempList.append(tempItem);
	}

	if(DoubleChanged(Frequency[0], Frequency[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("Frequency",
													   Frequency[0],
													   Frequency[1]));
		tempList.append(tempItem);
	}

	if(DoubleChanged(InitialTime[0], InitialTime[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("InitialTime",
													   InitialTime[0],
													   InitialTime[1]));
		tempList.append(tempItem);
	}

	if(DoubleChanged(InitialRadius[0], InitialRadius[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("InitialRadius",
													   InitialRadius[0],
													   InitialRadius[1]));
		tempList.append(tempItem);
	}

	if(DoubleChanged(InitialVelocity[0], InitialVelocity[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("InitialVelocity",
													   InitialVelocity[0],
													   InitialVelocity[1]));
		tempList.append(tempItem);
	}

	if(DoubleChanged(AmbientPressure[0], AmbientPressure[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("AmbientPressure",
													   AmbientPressure[0],
													   AmbientPressure[1]));
		tempList.append(tempItem);
	}

	if(DoubleChanged(AdiabaticRadius[0], AdiabaticRadius[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("AdiabaticRadius",
													   AdiabaticRadius[0],
													   AdiabaticRadius[1]));
		tempList.append(tempItem);
	}

	if(DoubleChanged(IsothermalRadius[0], IsothermalRadius[1])){
		tempItem = new QListWidgetItem(BeautifyOutput("IsothermalRadius",
													   IsothermalRadius[0],
													   IsothermalRadius[1]));
		tempList.append(tempItem);
	}

	if(LiquidType[0] != LiquidType[1]){
		liquidA = Constants::LIQUID_NAMES[LiquidType[0]].c_str();
		liquidB = Constants::LIQUID_NAMES[LiquidType[1]].c_str();
		tempItem = new QListWidgetItem(BeautifyOutput("LiquidType",
													   liquidA,
													   liquidB));
		tempList.append(tempItem);
	}

	if(Driver[0] != Driver[1]){
		driverA = (Driver[0] == Constant) ? "constant" : "sinusoidal";
		driverB = (Driver[1] == Constant) ? "constant" : "sinusoidal";
		tempItem = new QListWidgetItem(BeautifyOutput("Driver",
													  driverA,
													  driverB));
		tempList.append(tempItem);
	}
#pragma endregion

	// Checks to see if the lists of gas particles are the same
	if(AtomicParts[0] != AtomicParts[1]){
		// If they aren't, then list which ones changed
		for(int i = 0; i < AtomicParts[0].size(); i++){
			// example old list = {0,   0.1,   0,   0, 0.4,  0.5}
			// example new list = {0.8, 0.2,   0,   0,   0,    0}
			// The purpose of this if statement is to ignore cases where they're both 0
			if(((AtomicParts[0].at(i) != 0) || (AtomicParts[1].at(i) != 0))
				 && (AtomicParts[0].at(i) != AtomicParts[1].at(i))){
				
				elemName = QString::fromStdString(Constants::ELEMENT_NAMES[i]);
				tempItem = new QListWidgetItem(BeautifyOutput(elemName,
															  AtomicParts[0].at(i),
															  AtomicParts[1].at(i)));
				tempList.append(tempItem);
			}
		}
	}

	// For each set of values that were changed from InfoStruct A to B, add that set to the output list
	for(QListWidgetItem* valueChanged : tempList){
		this->UI_.changeList->addItem(valueChanged);
	}

	tempList.clear();    // deallocate all temp items (list itself is not dynamically allocated)
}

bool ChangeConfirmation::DoubleChanged(double a, double b){

	double epsilon = FLT_EPSILON; // epsilon = threshold where the difference between
								  //           values is significant enough to be a change
								  // (DBL_EPSILON was/is incorrectly reporting that initial time
	                              //      and radius were changing even though they weren't,
								  //      FLT_EPSILON produces desirable output.)
	
	if(abs(a - b) < epsilon){
		return false;
	} else {
		return true;
	}
}

QString ChangeConfirmation::BeautifyOutput(QString paramName, double paramA,  double paramB){

	QString toBeReturned = "";                           // String to be returned.
	QString spaceBefore  = "";							 // Spaces before the param name
	QString spaceAfter   = "";							 // Spaces after the ':'
	
	int nameLength  = paramName.length();                // Length of the parameter name
	int paramLength = QString::number(paramA).length();  // Length of the first parameter's
														 //    value in string format

	// Number of spaces that should be printed before the parameter name, aligns ':'
	// If the length of the parameter value is longer than the constant blank space string,
	//    don't print spaces. If this isn't checked, it's possible for an array index out
	//    of bounds error on the EmptySpace substring and will crash the program.
	if(nameLength > EmptySpaceNames.length()){
		spaceBefore = "";
	} else {
		spaceBefore = QString::fromStdString(EmptySpaceNames.substr(nameLength));
	}

	// Number of spaces that should be printed before the parameter value, aligns '->'
	if(paramLength > EmptySpaceValues.length()){
		spaceAfter = "";
	} else {
		spaceAfter = QString::fromStdString(EmptySpaceValues.substr(paramLength));
	}

	toBeReturned = spaceBefore + paramName + " : " + spaceAfter
				   + QString::number(paramA) + " -> " + QString::number(paramB);

	return toBeReturned;
}

QString ChangeConfirmation::BeautifyOutput(QString paramName, QString paramA, QString paramB){

	QString toBeReturned = "";
	QString spaceBefore  = "";
	QString spaceAfter   = "";
	
	int nameLength  = paramName.length();
	int paramLength = paramA.length();

	if(nameLength > EmptySpaceNames.length()){
		spaceBefore = "";
	} else {
		spaceBefore = QString::fromStdString(EmptySpaceNames.substr(nameLength));
	}

	if(paramLength > EmptySpaceValues.length()){
		spaceAfter = "";
	} else {
		spaceAfter = QString::fromStdString(EmptySpaceValues.substr(paramLength));
	}

	toBeReturned = spaceBefore + paramName + " : " + spaceAfter + paramA + " -> " + paramB;

	return toBeReturned;
}

void ChangeConfirmation::ConnectWidgets(){

	// Accept button
	connect(this->UI_.acceptButton, SIGNAL(clicked()), this, SLOT(AcceptChanges()));

	// Reject button
	connect(this->UI_.revertButton, SIGNAL(clicked()), this, SLOT(RevertChanges()));
}
#pragma endregion