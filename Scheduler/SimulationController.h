// SimulationController.h - SimulationController class implementation.
// Written by Jesse Z. Zhong
#ifndef __Simulation_Controller_H__
#define __Simulation_Controller_H__
#pragma region Includes
#include <QThread>
#include <InfoStruct/InfoStruct.h>
#include "SimulationWorker.h"
using namespace std;
using namespace MDSimulation;
#pragma endregion
namespace SchedulerGUI {
	/*! 
	  Handles the running of the simulation with
	  all the necessary requirements and conditions.
	*/
	class SimulationController : public QObject {
		Q_OBJECT
	public:
		/*! 
		  Constructor
		*/
		SimulationController(QObject* parent = 0);

		/*! 
		  Destructor

		  Terminate any running loops and kill the thread.
		*/
		~SimulationController();

	public slots:
		/*! 
		  Attempts to start a new instance of the simulation.

		  \param parameters is the parameters needed to create an instance of the simulation.
		  \param outputDir is the directory where the simulation output will bee stored into.
		  \return 
		*/
		bool Start(const InfoStruct& parameters, const QString& outputDir);

		/*! 
		  \overload

		  \param parameters is the parameters needed to create an instance of the simulation.
		*/
		bool Start(const InfoStruct& parameters);

		/*! 
		  Attempts to halt the simulation temporarily.

		  \return
		*/
		bool Pause();

		/*! 
		  Attempts to resume a paused simulation.

		  \return
		*/
		bool Resume();

		/*! 
		  Attempts to stop the simulation completely.

		  \return
		*/
		bool Stop();

	#pragma region Accessors
	public:
		/*! 
		  Sets the directory the simulation will output to.
		*/
		void SetOutpuDir(const QString& dirPath);
		
		/*! 
		  Returns the directory the simulation is outputting to.
		*/
		QString GetDirectory() const;
		 
		/*! 
		  Returns the address to the simulation worker.
		*/
		SimulationWorker* GetSimWorker();
	#pragma endregion
	protected:
	#pragma region Simulation Members
		//! Thread used for running the simulation.
		QThread* SimThread_;

		//! Worker in charged of running the simulation.
		//! NOTE: QObjects cannot be copied or passed.
		//! However, by design, pointers of QObjects are legal.
		SimulationWorker* SimWorker_;

		//! The directory where the simulation
		//! output will be written to.
		QString OutputDir_;
	#pragma endregion
	};
}

#endif // !__Simulation_Controller_H__