// SimulationWorker.h - Simulation worker declaration.
// Written by Jesse Z. Zhong
#ifndef __Simulation_Worker_H__
#define __Simulation_Worker_H__
#pragma region Includes
#include <QThread>
#include <QReadWriteLock>
#include <Simulation/Simulation.h>
#include <InfoStruct/InfoStruct.h>
using namespace std;
using namespace MDSimulation;
#pragma endregion
namespace SchedulerGUI {
	/*! 
	  Stores the results of the simulation
	  after the simulation has been terminated.
	  TODO: Implement.
	*/
	struct SimulationResult {

	};



	/*! 
	  Worker object for running an instance of the simulation.
	*/
	class SimulationWorker : public QObject {
		Q_OBJECT
	protected slots:
		/*! 
		  Event loop that handles the starting, 
		  running, and closing of simulation instances.

		  In the event that there is already an instance of the
		  simulation running, a halt will be called when a new
		  simulation is requested via \a SimulationWorker::StartSim.
		  When the simulation is successfully halted, the method will
		  return to the head of the event loop and check if a new 
		  simulation was called for.
		*/
		void DoWork();

		/*! 
		  Stops the SimulationWorker's event loop.

		  Changes the state of the SimulationWorker, 
		  forcing the event loop to break and work to stop.
		*/
		void StopWork();
		
		/*! 
		  Requests for a new instance of the simulation to be run.

		  Stores the passed parameters and output directory into the worker and
		  changes the state of the worker to alert the event loop to start a new
		  instance of the simulation. If the parameters passed are empty, an error
		  will be emitted and the method will return.

		  \param parameters is an instance of InfoStruct that contains the information necessary to start a simulation.
		  \param outDir is the directory where the output files will be saved to.
		*/
		void StartSim(const InfoStruct& parameters, 
			const QString& outDir);

		/*! 
		Changes the running state of the simulation.

		The running state indicates whether a live simulation, a fully initialized
		simulation, is currently updating or not. SimulationWorker::ChangeRunState()
		can alter the running state the simulation is in.

		\param state is the run state that the simulation will be changed to.
		\see SimulationWorker::RunState_
		*/
		void ChangeRunState(bool state);

		/*! 
		  Changes the live state of the simulation.

		  The live state differs from the running state in that it
		  indicates whether or not an instance of the simulation exists
		  or not, and not whether a simulation is currently being updated.
		  \param state is the live state that the simulation will be changed to.
		  \see SimulationWorker::LiveState_
		*/
		void ChangeLiveState(bool state);

		/*! 
		  Breaks out of the simulation loop if it is running.

		  Changes the state of the simulation in order to break
		  out of a currently running simulation's update loop.
		  This has no effect if there is no simulation instantiated.
		*/
		void HaltSimulation();
		
		/*! 
		  Runs a new instance of a simulation.

		  Attempts to instantiate a simulation with the parameters
		  and output directory stored within the worker. If the instantiation
		  is successful, the method continues to update the simulation through
		  its steps, until informed to do otherwise.
		*/
		void RunSim();
	 
		/*! 
		  Writes parameters and simulation constants to
		  a stream so that they can be written to a log.
		*/
		void InitLogMsg();

		/*! 
		  Used to write log messages to a signal.

		  This method is used internally to send messages to objects connected
		  to SimulationWorker::LogMsgReady() signal. Its purpose is to broadcast
		  changes in the simulation. SimulationWorker::SetWillWriteLog() can toggle
		  whether or not these messages will be broadcast.
		  \param msg is the string that will be communicated to a log.
		*/
		void WriteLogMsg(const QString& msg);
	signals:
		/*! 
		  This signal is emitted when the simulation is initialized.

		*/
		void Initialized();

		/*! 
		  This signal is emitted when params and simulation constants have been 
		  established in the simulation and written to a string for display.
		*/
		void LogMsgReady(const QString& logMessage);

		/*! 
		  This signal is emitted when a save point is being made.

		*/
		void SavePointCreated(unsigned long long savePointNumber);
		void SavePointCreated();

		/*! 
		  This signal is emitted when the state of the simulation is changed.

		  \param state is the state of the simulation. 'true' is running. 'false' is paused.
		*/
		void ChangedRunState(const bool state);
		void ChangedRunState();

		/*! 
		  This signal is emitted when a new instance of a simulation is
		  started or if an existing simulation is completely terminated.

		  \param state is indicates if a new simulation has started, "true", or an existing one is terminated, "false".
		*/
		void ChangedLiveState(const bool state);
		void ChangedLiveState();

		/*! 
		  This signal is emitted when the event loop terminates on its own.

		*/
		void ResultReady(const SimulationResult& result);

		/*! 
		  This signal is emitted when the worker process ends.
		*/
		void Finished();

		/*! 
		  This signal is emitted when their is an error in the worker.
		*/
		void Error(const QString& message);

	public:
		/*! 
		  Constructor
		*/
		SimulationWorker(QObject* parent = 0);
		
		/*! 
		  Returns the running state of the simulation.

		  The running state indicates whether a live simulation, a 
		  fully initialized simulation, is currently updating or not.
		*/
		bool GetRunState();

		/*! 
		  Returns whether or not an instance of
		  the simulation exists in memory or not.

		  The live state differs from the running state in that it
		  indicates whether or not an instance of the simulation exists
		  or not, and not whether a simulation is currently being updated.
		*/
		bool GetLiveState();

		// Sets if the a log message will be written
		// whenever a new instance of the simulation starts.
		void SetWillWriteLog(bool value);

		// Returns if the a log message will be written
		// whenever a new instance of the simulation starts.
		bool GetWillWriteLog();

	protected:
		//! Local copy of the  parameters
		//! used to instantiate the simulation.
		InfoStruct Parameters_;

		//! Local copy of the directory path
		//! used to store the simulation output.
		QString OutputDir_;

		//! Indicates whether the event loop is active or not.
		bool IsWorking_;

		//! Flag that prompts the worker to 
		//! start a new instance of the simulation.
		bool StartNew_;

		//! Indicates if parameters are loaded and an instance of the 
		//! simulation has been created, whether it is running or not.
		bool LiveState_;

		//! The run state of the simulation. 
		//! 'true' means the simulation is running.
		//! 'false' means the simulation has paused.
		bool RunState_;

		//! Halts the simulation when set to true.
		bool HaltSim_;

		//! Indicates if parameters and simulation constants
		//! will be written to a string so they can be displayed.
		bool WillWriteLog_;

		//! Mutex used for safeguarding state
		//! reads and writes between threads.
		QReadWriteLock Mutex_;

		/*! 
		  Provide SimulationController with exclusive
		  access to the protected members of this class.
		*/
		friend class SimulationController;
	};
}
#endif // !__Simulation_Thread_H__
