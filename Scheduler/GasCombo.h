// GasCombo.h - Gas combo class declaration.
// Written by Jesse Z. Zhong
#ifndef __Gas_Combo_H__
#define __Gas_Combo_H__
#pragma region Includes
#include <QComboBox>
#include <QDoubleSpinBox>
using namespace std;
#pragma endregion
namespace SchedulerGUI {

	/*! 
	  Object used for grouping together a
	  spin box and a combo box that are used for 
	  picking gases that will be in the simulation.
	*/
	class GasCombo {
	public:
		/*! 
		  Constructor
		*/
		GasCombo();

		/*! 
		  Initializes the gas combo.
		*/
		void Init();
		
		// Widgets
		QComboBox* ComboBox_;		//!< Combobox that allows users to pick from a list of gases.
		QDoubleSpinBox* SpinBox_;	//!< Spinbox that allows users to pick the percentage of a gas.
	};
}
#endif // !__Gas_Combo_H__
