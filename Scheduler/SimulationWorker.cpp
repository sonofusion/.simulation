// SimulationWorker.cpp - Simulation worker implementation.
// Written by Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "SimulationWorker.h"
#include <Simulation/Simulation.h>
using namespace SchedulerGUI;
#pragma endregion

void SimulationWorker::DoWork() {

	// Main event loop.
	while(this->IsWorking_) {

		// Localize flags; no messing around.
		this->Mutex_.lockForRead();
		bool startNew = this->StartNew_;
		this->StartNew_ = false;
		this->Mutex_.unlock();

		// Check if a new instance of
		// the simulation was called for.
		if(startNew) {

			// Inform any logs that the
			// simulation is being initialized.
			this->WriteLogMsg(
				"Initializing Molecular Dynamics Simulation.<br>" \
				" Please stand by..<br>");

			// Begin initializing and running
			// an instance of the simulation.
			this->RunSim();
		}
	}

	// Emit signal informing that the process is finished.
	emit Finished();
}

void SimulationWorker::StopWork() {
	this->Mutex_.lockForWrite();
	this->IsWorking_ = false;
	this->Mutex_.unlock();
}

void SimulationWorker::StartSim(const InfoStruct& parameters, 
	const QString& outDir) {

		// Check for validity of the parameters.
		// Empty parameters will be rejected.
		if(InfoStruct() == parameters) {
			emit Error("Cannot initialize simulation with empty parameters.");
			return;
		}

		// Set in the new parameters.
		this->Parameters_ = parameters;

		// Set in the output directory.
		this->OutputDir_ = outDir;

		// Set flags for starting a new simulation.
		this->Mutex_.lockForWrite();

		// Halt any simulation that might be running.
		this->HaltSim_ = true;

		// Signal for a new simulation.
		this->StartNew_ = true;

		this->Mutex_.unlock();
}

void SimulationWorker::ChangeRunState(bool state) {

	// Mutex fools! Why not avoid a race condition?
	this->Mutex_.lockForWrite();
	this->RunState_ = state;
	bool liveState = this->LiveState_;
	this->Mutex_.unlock();

	// Broadcast the state the simulation is in.
	this->WriteLogMsg(
		! liveState
		? "The simulation has finished.<br>"
		: (state 
			? "The simulation is running.<br>" 
			: "The simulation has paused.<br>")
		);

	// Emit the signal indicating the state has changed.
	emit ChangedRunState(state);
	emit ChangedRunState();
}

void SimulationWorker::ChangeLiveState(bool state) {

	// Mutex fools! Why not avoid a race condition?
	this->Mutex_.lockForWrite();
	this->LiveState_ = state;
	this->Mutex_.unlock();

	// Emit the signal indicating the state has changed.
	emit ChangedLiveState(state);
	emit ChangedLiveState();
}

void SimulationWorker::HaltSimulation() {
	this->Mutex_.lockForWrite();
	this->HaltSim_ = true;
	this->Mutex_.unlock();
}

void SimulationWorker::RunSim() {
	
	// Create an instance of the simulation with the parameters.
	Simulation simulation(this->Parameters_.ToSimulationUnits(), this->OutputDir_.toStdString());
	emit Initialized();

	// Write parameters to a log message.
	if(this->WillWriteLog_)
		this->InitLogMsg();

	// Mutex to avoid possible race conditions
	// during simulation run state changes.
	this->Mutex_.lockForWrite();
	this->HaltSim_ = false;
	this->Mutex_.unlock();
	this->ChangeLiveState(true);
	this->ChangeRunState(true);

	// Start and update the simulation.
	int afterRebound = 0;
	for(unsigned long long updates = 0;;) {

		// Break out of the simulation loop
		// if the halt simulation flag is true;
		this->Mutex_.lockForRead();
		if(this->HaltSim_) {

			// Release critical region before
			// breaking out of the update loop.
			this->Mutex_.unlock();
			break;
		}

		// Check for the run state.
		if(this->RunState_) {

			// Release critical region.
			this->Mutex_.unlock();

			// Advance the simulation by one step.
			EventType type = simulation.NextStep();

			// Check if the event type of the step is an update.
			if(type == UpdateSystemEvent) {

				// Create the file path with the output directory and the update number.
				// NOTE: Fast concatenation made possible by QStringBuilder with QT_USE_QSTRINGBUILDER.
				char fileNameChr[256] = {'\0'};
				sprintf(fileNameChr, "/SavePoint%.6llu.txt", updates + 1);
				QString fileName = this->OutputDir_ + QString(fileNameChr);

				// Create a file stream with the new file name.
				ofstream file(fileName.toStdString());

				// Write particle positions, velocities, and types to the stream.
				for(auto it = simulation.begin(), 
					end = simulation.end(); it != end; it++) {
						file << it->Position[0] << ','
							<< it->Position[1] << ','
							<< it->Position[2] << ','
							<< it->Velocity[0] << ','
							<< it->Velocity[1] << ','
							<< it->Velocity[2] << ','
							<< it->Type        << '\n';
				}

				// Close file stream.
				file.close();

				// Increment the number of updates up by one.
				updates++;

				// Emit signal informing that a save point was created.
				emit SavePointCreated(updates);
				emit SavePointCreated();

				// Halt the simulation after a certain number of rebounds.
				if(simulation.HasRebounded() && ++afterRebound == 10)
					break;
			}
		}
		// Release critical region.
		else this->Mutex_.unlock();
	}

	// Changes the state of the simulation to not running.
	// Also, resets the simulation halting/interrupting flag.
	this->Mutex_.lockForWrite();
	this->HaltSim_ = false;
	this->Mutex_.unlock();
	this->ChangeLiveState(false);
	this->ChangeRunState(false);

	// Broadcast that the simulation has stopped.
	this->WriteLogMsg("The simulation has stopped.");

	// Return the result of the simulation run.
	// TODO: Implement SimulationResult and correct this emission.
	SimulationResult result;
	emit ResultReady(result);
}

void SimulationWorker::InitLogMsg() {

	// Define the break HTML tag.
	const char* br = "<br>";

	// Lock for reading parameters.
	this->Mutex_.lockForRead();

	// Write parameters and model units to the stream.
	stringstream logInfo;
	logInfo
		<< "\nInitialization Information" << br << br
		<< this->Parameters_ << endl

		<< "Units::L = " << Units::L << " m" << br
		<< "Units::T = " << Units::T << " s" << br
		<< "Units::M = " << Units::M << " kg" << br

		<< "Units::Pressure = " << Units::Pressure << " Pa<br>"
		<< "Units::DynamicViscosity = " << Units::DynamicViscosity << " Pa*s" << br
		<< "Units::SurfaceTension = " << Units::SurfaceTension << " ???" << br;

	// Write the squared atomic diameter values for each particle type.
	logInfo << "AtomicDiameterSquared" << br;
	logInfo.precision(12); // Set the possible precision to 12 places.
	for(int i = 0; i < Constants::ELEMENT_TYPES; i++) {
		for (int j = 0; j < Constants::ELEMENT_TYPES; ++j) {
			logInfo << this->Parameters_.AtomicDiameterSquared[i][j];
		}
		logInfo << br;
	}

	// Write the bird constants for each particle type.
	for(int i = 0; i < Constants::ELEMENT_TYPES; i++)
		logInfo << this->Parameters_.BirdConstant[i] << br;

	// Write the atomic mass for each particle type.
	for(int i = 0; i < Constants::ELEMENT_TYPES; i++)
		logInfo << this->Parameters_.AtomicMass[i] << br;

	// Write the reduced mass for each particle type.
	for(int i = 0; i < Constants::ELEMENT_TYPES; i++) {
		for (int j = 0; j < Constants::ELEMENT_TYPES; ++j) {
			logInfo << this->Parameters_.ReducedMass[i][j];
		}
		logInfo << br;
	}

	// 
	for(int i = 0; i < Constants::ELEMENT_TYPES; i++)
		logInfo << this->Parameters_.VWallThermal[i] << br;

	// Emit signal, with a log message, when
	// the simulation is fulling initialized.
	QString logMsg = QString(logInfo.str().c_str());
	emit LogMsgReady(logMsg);

	// Release lock.
	this->Mutex_.unlock();
}

void SimulationWorker::WriteLogMsg(const QString& msg) {
	if(this->WillWriteLog_)
		emit LogMsgReady(msg);
}

SimulationWorker::SimulationWorker(QObject* parent) 
	: QObject(parent) {
		this->Parameters_ = InfoStruct();
		this->IsWorking_ = true;
		this->StartNew_ = false;
		this->RunState_ = false;
		this->HaltSim_ = false;
		this->LiveState_ = false;
		this->WillWriteLog_ = true;
}

bool SimulationWorker::GetRunState() {
	bool state = false;
	this->Mutex_.lockForRead();
	state = this->RunState_;
	this->Mutex_.unlock();
	return state;
}

bool SimulationWorker::GetLiveState() {
	bool isLive = false;
	this->Mutex_.lockForRead();
	isLive = this->LiveState_;
	this->Mutex_.unlock();
	return isLive;
}

void SimulationWorker::SetWillWriteLog(bool value) {
	this->Mutex_.lockForWrite();
	this->WillWriteLog_ = value;
	this->Mutex_.unlock();
}

bool SimulationWorker::GetWillWriteLog() {
	bool willWriteLog = false;
	this->Mutex_.lockForRead();
	willWriteLog = this->WillWriteLog_;
	this->Mutex_.unlock();
	return willWriteLog;
}