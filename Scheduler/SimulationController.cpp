// SimulationController.h - SimulationController class declaration.
// Written By Jesse Z. Zhong
#pragma region Includes
#include "stdafx.h"
#include "SimulationController.h"
#include <Simulation/Simulation.h>
#include <Utilities.h>
#include <QDir>
using namespace SchedulerGUI;
#pragma endregion
#pragma region Macros
// Using string builder when concatenating two QStrings.
#ifndef QT_USE_QSTRINGBUILDER
#define QT_USE_QSTRINGBUILDER
#endif // !QT_USE_QSTRINGBUILDER
#pragma endregion

SimulationController::SimulationController(QObject* parent)
	: QObject(parent) {

		// Initialize members.
		//this->OutputDir_ = QString();

		// Initialize simulation worker and thread.
		// NOTE: QObjects cannot be moved to another
		// thread if they are a child of another object.
		this->SimWorker_ = new SimulationWorker();
		this->SimThread_ = new QThread(this);
		this->SimWorker_->moveToThread(this->SimThread_);

		// 
		connect(this->SimThread_, SIGNAL(started()), 
			this->SimWorker_, SLOT(DoWork()));

		// Set the thread and worker to clean
		// up after themselves when terminated.
		connect(this->SimWorker_, SIGNAL(Finished()),
			this->SimThread_, SLOT(quit()));
		connect(this->SimWorker_, SIGNAL(Finished()),
			this->SimWorker_, SLOT(deleteLater()));
		connect(this->SimThread_, SIGNAL(finished()), 
			this->SimThread_, SLOT(deleteLater()));

		// Begin processing the thread tasks.
		this->SimThread_->start(QThread::Priority::NormalPriority);
}

SimulationController::~SimulationController() {

	// Stop updating the event loop.
	this->SimWorker_->StopWork();

	// Quit thread and wait for cleanup.
	this->SimThread_->quit();

	// In the event of a deadlock,
	// forcibly terminate the program.
	if(!this->SimThread_->wait(4000)) {

		this->SimThread_->terminate();
	}
}

#pragma region Simulation Controls
bool SimulationController::Start(const InfoStruct& parameters, const QString& outputDir) {

	// Check if the directory exists.
	if(!QDir(this->OutputDir_).exists()) {
		string msg = this->OutputDir_.toStdString() + " is not a valid directory.";
		Utilities::Print(msg);
		return false;
	}

	// Check if the simulation is running or not.
	// If it is, continue running. Otherwise, attempt
	// start the simulation with any set parameters.
	if(!this->SimWorker_->GetLiveState()) {
		
		// Attempt to run the simulation with new parameters.
		this->SimWorker_->StartSim(parameters, this->OutputDir_ = outputDir);

		// Return true if the simulation was started.
		return true;
	}

	// Return false if the simulation 
	// could not be run or is already running.
	return false;
}

bool SimulationController::Start(const InfoStruct& parameters) {
	return this->Start(parameters, this->OutputDir_);
}

bool SimulationController::Pause() {

	// Check if the simulation is running.
	// If it is, perform the necessary procedures
	// that will force it to stop temporarily.
	if(this->SimWorker_->GetRunState()) {

		// Set the running state to false.
		this->SimWorker_->ChangeRunState(false);

		// Return true if the simulation was paused.
		return true;
	}

	// Return false if the simulation was not paused
	// or had not been running in the first place.
	return false;
}

bool SimulationController::Resume() {

	// Checks if there is an instance
	// of the simulation that exists.
	if(this->SimWorker_->GetLiveState() 
		&& !this->SimWorker_->GetRunState()) {

			// Set the running state to true.
			this->SimWorker_->ChangeRunState(true);

			// Return true if the simulation has resumed.
			return true;
	}

	// Return false if the an instance of the simulation 
	// did not exist or if the simulation is already running.
	return false;
}

bool SimulationController::Stop() {

	// Checks if the simulation is running.
	// If it is, terminate the necessary
	// processes that will bring it to a stop.
	if(this->SimWorker_->GetLiveState()) {
		
		// Brings the simulation to a complete
		// stop if it is currently running.
		this->SimWorker_->HaltSimulation();

		// Return true if the simulation was stopped.
		return true;
	}

	// Return false if the simulation could not be
	// stopped or was not running in the first place.
	return false;
}
#pragma endregion
#pragma region Accessors
void SimulationController::SetOutpuDir(const QString& dirPath) { 
	this->OutputDir_ = dirPath;
}

QString SimulationController::GetDirectory() const { 
	return this->OutputDir_; 
}

SimulationWorker* SimulationController::GetSimWorker() {
	return this->SimWorker_;
}
#pragma endregion