// main.cpp
// Written by Jesse Z. Zhong
#include "stdafx.h"
#include "Scheduler.h"
using namespace std;
using namespace SchedulerGUI;
// Program entry point.
// This program is used to test the
// simulation scheduler module independently.
// Build settings for both Debug and Release
// mode are set specifically for the module.
int main(int argc, char *argv[]) {
	QApplication application(argc, argv);
	Scheduler mainWindow;
	Q_INIT_RESOURCE(SchedulerGUI);
	mainWindow.show();
	return application.exec();
}
