// ChangeConfirmation.h - ChangeConfirmationGUI main window declaration.
// Written by Nicholas Kriner
#ifndef __Change_Confirmation_GUI_H__
#define __Change_Confirmation_GUI_H__
#pragma region Includes
#include "stdafx.h"
#include "ui_ChangeConfirmationGUI.h"

#include "Scheduler.h"

using namespace std;
#pragma endregion
namespace SchedulerGUI {
	/*! 
	  Takes in, as input, two InfoStructs and prompts the user to Accept 
	  and change, Reject and revert, or Cancel and do nothing about the changes.
	*/
	class ChangeConfirmation : public QDialog {
		Q_OBJECT
	public:
	#pragma region Constants
		//! Blank spaces used for aligning the parameter names
		static const string ChangeConfirmation::EmptySpaceNames;
		//! Blank spaces used for aligning the parameter values
		static const string ChangeConfirmation::EmptySpaceValues;
	#pragma endregion
		/*!
		  Constructor for the change confirmation GUI. Tells the user what changes were made between
		  the last save/load and the current parameter set.

		  @param[in]	oldStruct		InfoStruct from the previous save or load
		  @param[in]	newStruct		InfoStruct built from the current parameter set
		  @param[out]	changeAccepted	Set to true if the user accepts the changes; set to false otherwise
		*/
		ChangeConfirmation(InfoStruct oldStruct, InfoStruct newStruct, int *changeAccepted);

		/*!
		  Destructor
		*/
		~ChangeConfirmation();

	protected slots:
	#pragma region Protected Slots
		/*!
		  Accepts changes and closes the window.

		  Notes that changes should be accepted and exits the confirmation dialog
		*/
		void AcceptChanges();
		
		/*!
		  Rejects changes.

		  Notes that changes should be rejected (i.e. reverted) and exits the confirmation dialog
		*/
		void RevertChanges();

		// Cancels the saving process.
		void CancelSaving();
	#pragma endregion
	protected:
	#pragma region Initialization and Helper Methods
		/*! 
		  Checks to see which values in two InfoStructs are different.
		*/
		void ShowInfoStructDifference(InfoStruct structA, InfoStruct structB);
		
		/*! 
		  Returns true if the difference between two double values is larger than a given
		  epsilon value, using a constant epsilon value.

		*/
		bool DoubleChanged(double a, double b);

		/*!
		  Returns a string in the format " parameter_name : previous_value -> new value "

		  The string is formatted such that all parameters in the list will have ':' and '->'
		  at the same column.
		*/
		QString BeautifyOutput(QString paramName, double paramA,  double paramB);
		
		/*! \overload
		  Returns a string in the format " parameter_name : previous_value -> new value "

		  The string is formatted such that all parameters in the list will have ':' and '->'
		  at the same column. Same process as above, but this uses parameters that are already QStrings.
		*/
		QString BeautifyOutput(QString paramName, QString paramA, QString paramB);

		/*! 
		  Connects widgets in the UI to relevant signals.

		  Creates the appropriate connections for various UI elements and their functions.
		*/
		void ConnectWidgets();
	#pragma endregion
	#pragma region UI Internals
		int* Accept_;	//!< Set to true if the changes are accepted; false otherwise
		Ui::ChangeConfirmationGUI UI_;	//!< Instance of the UI/form.
	#pragma endregion
	};
}
#endif // !__ChangeConfirmation_GUI_H__