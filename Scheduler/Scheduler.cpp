// Scheduler.cpp - SchedulerGUI main window implementation.
// Written by Jesse Z. Zhong & Nicholas Kriner
#pragma region Includes
#include "stdafx.h"

#include "Scheduler.h"

#include <Simulation/Simulation.h>
using namespace SchedulerGUI;
#pragma endregion
#pragma region Constants
const QString Scheduler::ModuleName = "Scheduler";
const QString Scheduler::MostRecentlyUsedFile = ModuleName + "MostRecentlyUsedFile";
const QString Scheduler::MostRecentDirectory = ModuleName + "MostRecentDirectory";
const string Scheduler::EmptySpaces = "                ";
#pragma endregion
#pragma region Macros
// Using string builder when concatenating two QStrings.
#ifndef QT_USE_QSTRINGBUILDER
#define QT_USE_QSTRINGBUILDER
#endif // !QT_USE_QSTRINGBUILDER
#pragma endregion
#pragma region Constructor/Destructor
Scheduler::Scheduler() {

		// Set up the UI & Widgets
		this->UI_.setupUi(this);		

		// Sets up the symbol->fullname mapping
		this->InitMapList();

		// Connect the gas ComboBoxes and SpinBoxes to the UI + populate them with element names
		this->InitGasComboBoxes();
		this->InitLiquidComboBox();

		// Assign the Organization and Module names to the scheduler's QSettings
		assert(!(Utilities::OrganizationName.isEmpty()));
		assert(!(ModuleName.isEmpty()));
		this->SchedulerSettings_ = new QSettings(Utilities::OrganizationName,
					                                ModuleName, this);

		// Retrieve the most recently used output directory.
		QString recentDir;
		recentDir = this->SchedulerSettings_->value(MostRecentDirectory,"").toString();
		if(!recentDir.isEmpty()) {
			this->ChangeOutDirBox(recentDir);
		}

		// Instantiate an instance of the simulation controller.
		this->SimControl_ = new SimulationController(this);

		// Connect signals and slots.
		this->ConnectWidgets();

		// Instantiate an empty InfoStruct.
		this->InfoStruct_ = InfoStruct();

		// The InfoStruct's driver is explicitly set to Sinusoidal to avoid possible
		//    loading inconsistencies. The InfoStruct's atomic parts list is set to
		//    be populated with all 0's for the same reason
		this->InfoStruct_.Driver = Sinusoidal;
		for(int i = 0; i < Constants::ELEMENT_TYPES; i++) {
			this->InfoStruct_.AtomicParts.push_back(0);
		}

		// Set data checking booleans to a default of 'false'
		this->AllBoxesFilled_ = false;
		this->GasCompCorrect_ = false;
}

Scheduler::~Scheduler(){

}
#pragma endregion
#pragma region Protected Slots
void Scheduler::SetOutputDir() {

	// Prompts user for the directory they
	// wish to use for storing simulation output.
	QString directoryName = QFileDialog::getExistingDirectory();

	// Checks if a directory path was specified.
	if(directoryName != NULL) {

		// Update the line edit.
		this->ChangeOutDirBox(directoryName);

		// Write the change to the action log.
		this->WriteToLog("Simulation Directory set to<br>" + directoryName);

		// Create or update a registry entry to indicate there is a new directory path
		// SchedulerSettings_ is asserted to have a non-empty company and application name
		//    in the constructor
		this->SchedulerSettings_->setValue(MostRecentDirectory, directoryName);
	}
}

void Scheduler::LoadParameters() {

	// Prompt user for the file name for a parameters file.
	QString fileName = QFileDialog::getOpenFileName(0, tr("Select file to open"), QString(), tr("JSON Files (*.json)"));

	// Check if a valid file name
	// was specified by the user.
	if(fileName != NULL) {

		// Check for the extension of the file name.
		if(fileName.endsWith(".json", Qt::CaseInsensitive)) {

			// Parse the file into the program, via InfoStruct.
			this->InfoStruct_ = this->InfoStruct_.ParseInfoStruct(fileName.toStdString());
			
			// Update the UI elements with the newly read values.
			this->UpdateUI();

			this->UpdateParameters();

			// Write the read and parse even to the log.
			this->WriteToLog("Parameter JSON file loaded from<br>" + fileName);

			// Create or update a registry entry to indicate there is a new most recently used file
			// this->SchedulerSettings_->setValue(MostRecentlyUsedFile, fileName);
		}
	}
}

void Scheduler::LoadRecentFile() {

	// Holds the name of the most recently edited file.
	QString     recentFileName = this->SchedulerSettings_->value(MostRecentlyUsedFile,"").toString();
	QFile       recentFile(recentFileName);			// Used to check if the file actually exists
	QFileInfo   fileInfo(recentFileName);			// Strips the path from a file name; used for output
	QMessageBox fileMessage(this);					// Used to output warnings or updates to the user

	// Make the warning / update messages modal.
	fileMessage.setModal(true);

	// If the file isn't empty and ends with .json, attempt to parse the info using InfoStruct
	if(!recentFileName.isEmpty()){
		// If the most recently edited file no longer exists, warn the user
		if(!recentFile.exists()){
			fileMessage.setText("No recent file exists.");
			fileMessage.setModal(true);
			fileMessage.setWindowTitle("Warning!");
			fileMessage.exec();
		// Else, and only if the file is a .json file, load the file
		} else if(recentFileName.endsWith(".json", Qt::CaseInsensitive)) {
			// Parse
			this->InfoStruct_ = this->InfoStruct_.ParseInfoStruct(recentFileName.toStdString());

			// Update the UI with new parameter, driver, gas and liquid info
			this->UpdateUI();

			// Update Action Log
			this->WriteToLog("Parameter JSON file loaded from<br>" + recentFileName);

			// Create or update a registry entry to indicate there is a new most recently used file
			this->SchedulerSettings_->setValue(MostRecentlyUsedFile, recentFileName);

/*			fileMessage.setText(fileInfo.fileName() + " loaded."); // "File name loaded."
			fileMessage.setWindowTitle("Update");
			fileMessage.exec(); replaced by action log messages */
		} 
	}
}

void Scheduler::SaveParameters() {

	int i = 0;												// index
	int val = 0;											// temp val
	int shouldContinue = DoNothing;							// default action is to do nothing (incase window is exited)
	int gasType = 0;										// holds the index of a gas
	double sum = 0;											// holds the sum of atomic particles
	double particleCount = 0;								// holds a gases atomic particle count
	const int elementListSize = Constants::ELEMENT_TYPES;	// constant size of the list of elements
	double particleList[elementListSize] = {0};				// double array that will hold the particles 
															//    of each gas

	QFile       schedFile;									// QFile - used to write the .json object to a file
	QTextStream schedStream(&schedFile);					// Stream used to write to the file
	InfoStruct  tempStruct;									// Holds values to be written to the .json file
	QMessageBox revertBox(this);							// Tells the user that changes were reverted

	// Name of the most recently used file
//	QString     saveAs = this->SchedulerSettings_->value(MostRecentlyUsedFile,"").toString();
	
	// Uses the current date/time to create a name
	QString		dateFormat = "yyyy-mmm-dd_hh-mm-ss";
	QDateTime	date = QDateTime::currentDateTime();
	QString		saveAs = "ParamSet__";
				saveAs += date.toString("yyyy-MMM-dd__HH-mm-ss");
		

	// Updates the parameters; this function also sets AllBoxesFilled_ to be true if all parameter
	//    boxes have valid input.
	this->UpdateParameters();
	if(this->AllBoxesFilled_ == false){

		// If not all parameter boxes are filled, tell the user and return to the Scheduler UI
		QMessageBox::question(this, "Parameter Input Not Valid", 
			"One or more of the parameter boxes are empty. Please input valid data for each parameter.",
			QMessageBox::Ok);

		return;
	}

	tempStruct = this->GUIToInfoStruct();

	for(GasCombo gasCombo : this->GasList_) {
		sum += gasCombo.SpinBox_->value();                    // Sum of particles. Should be 1.0
	}

	// Make sure that the sum of gases is not 0
	if(sum == 0){

		QMessageBox::question(this, "Gas Input Not Valid", 
			"The sum of gas particles cannot be 0! Please input valid data for the gases.",
			QMessageBox::Ok);

		return;
	} else if(sum != 1.0){

		// The gas list is required to be normalized. If it's not, ask the user if they would like it to be
		QMessageBox::StandardButton result;
		result = QMessageBox::question(this, "Gas Values Not Normalized", 
			"The sum of gas particles is equal to : " + QString::number(sum) + "\n"
			+ "Would you like to normalize the data to 1.0? Each gas particle value will "
			+ "be changed to (value / " + QString::number(sum) + ")",
			QMessageBox::Yes | QMessageBox::No);
		
		// If the user says no, tell them to change the values to be normalized
		if(result == QMessageBox::No){

			QMessageBox needNormalizedData(this);
			needNormalizedData.setModal(true);
			needNormalizedData.setWindowTitle("Error");
			QString errorMessage = "A normalized set of gas particles is required. Please either manually";
			        errorMessage += " change the gas particle values to sum to 1.0 or accept normalization.";
			needNormalizedData.setText(errorMessage);
			needNormalizedData.exec();

			return;
		}
	}

	// Normalize the gas list if the user accepts it.
	for(i = 0; i < elementListSize; i++) {
		tempStruct.AtomicParts[i] = (tempStruct.AtomicParts[i] / sum);
	}

	// If there is a difference in the InfoStructs, ask the user if the changes are acceptable
	if(InfoStructDifference(this->InfoStruct_, tempStruct)){
		// If the user accepts the changes presented by the dialog box, shouldContinue = true
		shouldContinue = this->ShowInfoStructDifference(this->InfoStruct_, tempStruct);
	} else {
		// Otherwise, if there were no changes, the user clicked save without changing anything.
		// Tell the user that nothing has been changed, and continue with the save (presumably
		//    to save the same parameter set under a different name).
		QMessageBox::question(this, "Notice", 
			"Nothing has been changed.",
			QMessageBox::Ok);

		shouldContinue = Accept;
	}

	// If the user doesn't accept or reject (just closed the change confirmation window), then
	//    return without saving or reverting any changes.
	if(shouldContinue == DoNothing){
		return;
	}

	// If they reject the changes, revert them; default value of shouldContinue is Accept, so 
	//    if there is no difference in the infostructs the previous two blocks of code won't execute,
	//    shouldContinue will be Accept, and this block of code will be skipped.
	if(shouldContinue == Reject) {

		// Formatting
		revertBox.setModal(true);
		revertBox.setWindowTitle("Notice");
		revertBox.setText("Changes reverted.");
		revertBox.exec();

		// InfoStruct_ is changed when files are loaded or when they are saved to contain the updated values,
		//    never during editing. Therefore, as UpdateUI populates the parameter boxes with values
		//    based on InfoStruct_, calling UpdateUI() without accepting changes will revert the parameters
		//    to their previous values.
		this->UpdateUI();

		return;
	}

	// If they did accept the changes, or there were no changes, continue as normal

	// Prompt user for a directory and file name to save the parameters to.
	QString fileName = QFileDialog::getSaveFileName(0, tr("Save Parameters"), 
		saveAs, tr("JSON Files (*.json)"));

	// If the file is valid, write the JSON object and change InfoStruct_
	if((fileName != NULL) && (fileName.endsWith(".json", Qt::CaseInsensitive))) {	

		schedFile.setFileName(fileName);                                      // Set the fileName as a QFile
		schedFile.open(QIODevice::WriteOnly | QIODevice::Text);               // Open the QFile for writing
		FileStream f(freopen(fileName.toStdString().c_str(), "w", stdout));   // Redirect stdout to write to the QFile
		PrettyWriter<FileStream> writer(f);                                   // Create a JSON Writer
		tempStruct.Serialize(writer);                                         // Write the tempStruct to the QFile
		freopen("CON", "w", stdout);										  // Redirects stdout back to console.
		this->InfoStruct_ = tempStruct;										  // Updates InfoStruct_ with the saved values
		this->UpdateUI();

		// Create or update a registry entry to indicate there is a new most recently used file
		this->SchedulerSettings_->setValue( MostRecentlyUsedFile, fileName );
		this->WriteToLog("Parameter JSON file saved to<br>" + fileName);
	}
}

void Scheduler::UpdateUI() {

	int i = 0;              // index
	int numGasesRead = 0;	// index; counts number of gases (non-zero values on the atomic parts list) read
	double temp = 0;		// temp value
	int liquidType = 0;     // index corresponding to the liquid type used in the constant list of liquids
	int driverType = 0;		// see above, for drivers

	// Set driver to Sinusoidal so frequency can be edited
	this->UI_.DriverModeComboBox->setCurrentIndex( Sinusoidal );

	// Get parameters from the InfoStruct
	this->UI_.ConeAngleLineText->setText(QString::number(this->InfoStruct_.ConeAngle));
	this->UI_.TemperatureLineText->setText(QString::number(this->InfoStruct_.Temperature));
	this->UI_.WallTemperatureLineText->setText(QString::number(this->InfoStruct_.WallTemperature));
	this->UI_.EquilibriumRadiusLineText->setText(QString::number(this->InfoStruct_.AmbientRadius)); 
	this->UI_.DriverPressureLineText->setText(QString::number(this->InfoStruct_.DriverPressure));
	this->UI_.FrequencyLineText->setText(QString::number(this->InfoStruct_.Frequency));
	this->UI_.InitialTimeLineText->setText(QString::number(this->InfoStruct_.InitialTime));
	this->UI_.InitialRadiusLineText->setText(QString::number(this->InfoStruct_.InitialRadius));
	this->UI_.InitialVelocityLineText->setText(QString::number(this->InfoStruct_.InitialVelocity));
	this->UI_.AmbientPressureLineText->setText(QString::number(this->InfoStruct_.AmbientPressure));
	this->UI_.AdiabaticRadiusLineText->setText(QString::number(this->InfoStruct_.AdiabaticRadius));
	this->UI_.IsothermalRadiusLineText->setText(QString::number(this->InfoStruct_.IsothermalRadius));

	// If the driver is Sinusoidal, set it to Sinusoidal. As there are only two driver modes,
	//    if it's not Sinusoidal, default to Constant;
	// MUST be after the parameter setting, as frequency is uneditable while
	//                 the driver is set to constant
	driverType = (this->InfoStruct_.Driver == Sinusoidal) ? Sinusoidal : Constant;
	this->UI_.DriverModeComboBox->setCurrentIndex(driverType);
	
	// Sets the comboBox index to the index of the liquid type retrieved
	liquidType = this->InfoStruct_.LiquidType;
	this->UI_.LiquidTypeComboBox->setCurrentIndex(liquidType);

	// For each possible element: see if the InfoStruct has a value for
	//    the atomic parts of that element; add it into the appropriate combo box
	// The Gas Combo/Spinboxes are reset to default values before loading
	this->ResetGasComboBoxes();
	for(i = 0; i < this->InfoStruct_.AtomicParts.size(); i++) {
		temp = this->InfoStruct_.AtomicParts.at(i);
		if(temp != 0) {
			this->GasList_.at(numGasesRead).ComboBox_->setCurrentIndex(i);
			this->GasList_.at(numGasesRead).SpinBox_->setValue(temp);

			// if a gas was set, increment the counter so the boxes are filled sequentially
			numGasesRead++;
		}
	}

	this->UpdateParameters();
}

void Scheduler::UpdateDriverMode(const QString& newMode) {

	QString isConstant("constant");

	if(newMode.toLower() == isConstant) {
		this->UI_.FrequencyLineText->setDisabled(true);
	} else {
		this->UI_.FrequencyLineText->setEnabled(true);
	}
}

void Scheduler::UpdateGasComp() {

	double sum = 0;                                     // Sum of the spin boxes
	double max = this->UI_.GasSpinBox1->maximum();      // Max possible number. The max is assumed
														//    to be uniform across all gases (1.0)

	// Sum the values of all doubles in the gas boxes.
	sum += this->UI_.GasSpinBox1->value();
	sum += this->UI_.GasSpinBox2->value();
	sum += this->UI_.GasSpinBox3->value();
	sum += this->UI_.GasSpinBox4->value();
	sum += this->UI_.GasSpinBox5->value();
	sum += this->UI_.GasSpinBox6->value();
	sum += this->UI_.GasSpinBox7->value();
	sum += this->UI_.GasSpinBox8->value();

	// If the sum of all gases is exactly 1.0, it's true; false otherwise
	this->GasCompCorrect_ = ( sum == max ) ? true : false;
}

void Scheduler::UpdateParameters() {

	// Sum of the boxes filled
	int boxesFilled = 0;

	// If a box has a text with a length >0, that means it has data. Currently
	//    a temporary check and in the future validity will be determined by
	//    actual value (i.e. nonnegative and reasonable), instead of text length.
	if(ValidParam(*this->UI_.ConeAngleLineText))
		boxesFilled++;
	if(ValidParam(*this->UI_.TemperatureLineText))
		boxesFilled++;
	if(ValidParam(*this->UI_.WallTemperatureLineText))
		boxesFilled++;
	if(ValidParam(*this->UI_.EquilibriumRadiusLineText))
		boxesFilled++;
	if(ValidParam(*this->UI_.DriverPressureLineText))
		boxesFilled++;
	if(ValidParam(*this->UI_.FrequencyLineText))
		boxesFilled++;
	if(ValidParam(*this->UI_.InitialTimeLineText))
		boxesFilled++;
	if(ValidParam(*this->UI_.InitialRadiusLineText))
		boxesFilled++;
	if(ValidParam(*this->UI_.InitialVelocityLineText))
		boxesFilled++;
	if(ValidParam(*this->UI_.AmbientPressureLineText))
		boxesFilled++;
	if(ValidParam(*this->UI_.AdiabaticRadiusLineText))
		boxesFilled++;
	if(ValidParam(*this->UI_.IsothermalRadiusLineText))
		boxesFilled++;
	
	// If all 12 boxes are filled, then allBoxesFilled is set to true; false otherwise
	this->AllBoxesFilled_ = (boxesFilled == 12) ? true : false;
}

void Scheduler::ChangeOutDirBox(const QString& dirPath) {

	this->UI_.SimulationDirectoryBox->setText(dirPath);
}

void Scheduler::ToggleRunSim() {

	// Check the state of the simulation.
	if(this->SimControl_->GetSimWorker()->GetLiveState()) {

		// Prompt the user asking if he, she, or it is sure
		// that he, she, or it wants to stop the simulation.
		QMessageBox::StandardButton result;
		result = QMessageBox::question(this, "Stop Simulation", 
			"Are you sure you want to stop the simulation?",
			QMessageBox::Yes | QMessageBox::No);

		// If the user's response is yes, proceed to halt the simulation.
		if(result == QMessageBox::Yes) {

			// Tell the controller to terminate the simulation.
			this->SimControl_->Stop();
		} 
		// Skip subsequent behavior if "No" is picked.
		else return;

	} else {

		// Check if the directory specified in the UI exists.
		QString path;
		if(QDir(path = this->UI_.SimulationDirectoryBox->toPlainText()).exists()) {

			// All parameter boxes need to be filled before the simulation can start
			if(this->AllBoxesFilled_ == 0){

				QMessageBox errorMessage(this);
				errorMessage.setModal(true);
				errorMessage.setWindowTitle("Error");
				errorMessage.setText("One or more of the parameter boxes are empty. Please input valid data for each parameter.");
				errorMessage.exec();

				return;
			}

			// Check if the current InfoStruct has been filled out.
			InfoStruct tempStruct = this->GUIToInfoStruct();
			if(InfoStructDifference(tempStruct, this->InfoStruct_)){
				QMessageBox::StandardButton result;
				result = QMessageBox::question(this, "Parameters Not Updated", 
					"Please save parameters before starting the simulation. Would you like to save now?",
					QMessageBox::Yes | QMessageBox::No);
				if(result == QMessageBox::No){
					return;
				} else {
					this->SaveParameters();
					return;
				}
			}

			// Request for the controller to start an instance 
			// of the simulation with the user defined InfoStruct.
			this->SimControl_->Start(this->InfoStruct_, path);
		}
	}

	// Prevent the user from clicking on the button until the simulation has changed.
	this->UI_.StartButton->setText("Please Wait...");
	this->UI_.StartButton->setDisabled(true);
}

void Scheduler::TogglePlaySim() {

	// Check if there is a live simulation.
	if(this->SimControl_->GetSimWorker()->GetLiveState()) {

		// Check if there the simulation is running.
		if(this->SimControl_->GetSimWorker()->GetRunState()) {

			// Request for the controller to pause the simulation.
			this->SimControl_->Pause();

		} else {

			// Request for the controller to resume the simulation.
			this->SimControl_->Resume();
		}
	}
}

void Scheduler::ToggleStartButtonName(const bool state) {
	
	this->UI_.StartButton->setText((state) ? "Stop" : "Start");
	this->UI_.StartButton->setEnabled(true);
}

void Scheduler::TogglePlayButtonName(const bool state) {

	// Change the button image depending on the state.
	this->UI_.PlayPauseButton->setIcon(QIcon(state 
		? ":/SimulationGUI/Resources/PauseButton.png" 
		: ":/SimulationGUI/Resources/PlayButton.png"));
}

void Scheduler::WriteToLog(const QString& message) {
	
	// Grab the local time.
	time_t localTime = time(NULL);

	// Append the time to the log string.
	char* timeCString = asctime(localtime(&localTime));
	timeCString[strlen(timeCString) - 1] = 0;
	this->LogString_ += "<b>" + QString(timeCString) + "</b>:<br>" + message + "<br>";

	// Append the new message to the log string and reset the scroll bar.
	this->UI_.ActionLogBox->setText(this->LogString_);
	this->UI_.ActionLogBox->verticalScrollBar()->
		setValue(this->UI_.ActionLogBox->verticalScrollBar()->maximum());
}
#pragma endregion
#pragma region Initialization and Helper Methods
void Scheduler::InitMapList() {
	
	// Create a temporary map that will be copied into the Scheduler's map
	map<string,QString> initMap;

	// List of symbols and their respective names
	initMap["H"]  = "Hydrogen";
	initMap["D"]  = "Deuterium";
	initMap["H2"] = "Hydrogen Gas";
	initMap["D2"] = "Deuterium Gas";
	initMap["He"] = "Helium";
	initMap["Ne"] = "Neon";
	initMap["Ar"] = "Argon";
	initMap["Xe"] = "Xenon";

	// Copy the temporary mapping into the Scheduler's mapping
	this->FullGasNames_.insert(initMap.begin(), initMap.end());
}

void Scheduler::InitGasComboBoxes() {

	int i = 0;			 // Iterator
	GasCombo gasCombo;   // Holder variable that is used to populate Combo/Spin boxes

	// Add each ComboBox + SpinBox to the vector list of ComboBox + SpinBox pairings
	//    Currently using a finite list
#pragma region Gas Combo Initialization
	gasCombo.ComboBox_ = this->UI_.GasComboBox1;
	gasCombo.SpinBox_  = this->UI_.GasSpinBox1;
	this->GasList_.push_back(gasCombo);

	gasCombo.ComboBox_ = this->UI_.GasComboBox2;
	gasCombo.SpinBox_  = this->UI_.GasSpinBox2;
	this->GasList_.push_back(gasCombo);

	gasCombo.ComboBox_ = this->UI_.GasComboBox3;
	gasCombo.SpinBox_  = this->UI_.GasSpinBox3;
	this->GasList_.push_back(gasCombo);

	gasCombo.ComboBox_ = this->UI_.GasComboBox4;
	gasCombo.SpinBox_  = this->UI_.GasSpinBox4;
	this->GasList_.push_back(gasCombo);

	gasCombo.ComboBox_ = this->UI_.GasComboBox5;
	gasCombo.SpinBox_  = this->UI_.GasSpinBox5;
	this->GasList_.push_back(gasCombo);

	gasCombo.ComboBox_ = this->UI_.GasComboBox6;
	gasCombo.SpinBox_  = this->UI_.GasSpinBox6;
	this->GasList_.push_back(gasCombo);

	gasCombo.ComboBox_ = this->UI_.GasComboBox7;
	gasCombo.SpinBox_  = this->UI_.GasSpinBox7;
	this->GasList_.push_back(gasCombo);

	gasCombo.ComboBox_ = this->UI_.GasComboBox8;
	gasCombo.SpinBox_  = this->UI_.GasSpinBox8;
	this->GasList_.push_back(gasCombo);
#pragma endregion

	// For each Gas ComboBox + SpinBox combination in the list, connect it to relevant signals
	for(GasCombo gCombo : this->GasList_){

		// Connects Gas SpinBoxes to UpdateGasComp() (checks to see if sum is 1.0)
		connect(gCombo.SpinBox_, SIGNAL(editingFinished()), this, SLOT(UpdateGasComp()));

		// For each gas name in the constant list, see if it has a matching full element name:
		//    if it does, use the full name; else, just use the symbol itself
		for(string gasName : Constants::ELEMENT_NAMES){
			gCombo.ComboBox_->addItem(FullName(gasName));
		}

		// Set each ComboBox to a different gas name (aesthetic)
		// If i > possible number of
		gCombo.ComboBox_->setCurrentIndex(i++);
	}
}

void Scheduler::InitLiquidComboBox(){

	// For each liquid in the list of liquids, add it to the liquid ComboBox
	for(string liquidName : Constants::LIQUID_NAMES){
		this->UI_.LiquidTypeComboBox->addItem(QString::fromStdString(liquidName));
	}
}

void Scheduler::ConnectWidgets() {

	// Connect the "Change Directory to .." Button to the SetSimulationDirectory() Method
	connect(this->UI_.ChangeDirectoryToButton,      SIGNAL(clicked()), this, SLOT(SetOutputDir()));

	// Connect the "Load Parameters from File" Button to the LoadParamFromFile() Method
	connect(this->UI_.LoadRecentParametersButton,   SIGNAL(clicked()), this, SLOT(LoadRecentFile()));

	// Connect the "Load Parameters from File" Button to the LoadParamFromFile() Method
	connect(this->UI_.LoadParametersFromFileButton, SIGNAL(clicked()), this, SLOT(LoadParameters()));

	// Connect the "Save Current Parameters" Button to the SaveParamToFile() Method
	connect(this->UI_.SaveCurrentParametersButton,  SIGNAL(clicked()), this, SLOT(SaveParameters()));
	
	// Connect the Driver combo box to the enabling/disabling of Frequency parameter editing
	connect(this->UI_.DriverModeComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(UpdateDriverMode(QString)));

	// Sets it so that UpdateParameters() is called whenever a LineEdit is done being edited
	connect(this->UI_.ConeAngleLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters()));
	connect(this->UI_.TemperatureLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters()));
	connect(this->UI_.WallTemperatureLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters()));
	connect(this->UI_.EquilibriumRadiusLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters()));
	connect(this->UI_.DriverPressureLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters()));
	connect(this->UI_.FrequencyLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters()));
	connect(this->UI_.InitialTimeLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters()));
	connect(this->UI_.InitialRadiusLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters()));
	connect(this->UI_.InitialVelocityLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters()));
	connect(this->UI_.AmbientPressureLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters()));
	connect(this->UI_.AdiabaticRadiusLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters()));
	connect(this->UI_.IsothermalRadiusLineText, SIGNAL(editingFinished()),
			this, SLOT(UpdateParameters())); 

	// Connects simulation initialization to the action log to write
	// initialization information to the log when the simulation is started.
	connect(this->SimControl_->GetSimWorker(), SIGNAL(LogMsgReady(const QString&)), 
		this, SLOT(WriteToLog(const QString&)));

	// Connects the controller to the start button name toggle method to change the
	// name of the start button whenever the live state of the simulation has changed.
	connect(this->SimControl_->GetSimWorker(), SIGNAL(ChangedLiveState(const bool)),
		this, SLOT(ToggleStartButtonName(const bool)));

	// Connects the controller to the play button name toggle method to change the
	// name of the play button whenever the run state of the simulation has changed.
	connect(this->SimControl_->GetSimWorker(), SIGNAL(ChangedRunState(const bool)),
		this, SLOT(TogglePlayButtonName(const bool)));

	// Connect buttons to the simulation control slots.
	connect(this->UI_.StartButton, SIGNAL(clicked()), this, SLOT(ToggleRunSim()));
	connect(this->UI_.PlayPauseButton, SIGNAL(clicked()), this, SLOT(TogglePlaySim()));
}

QString Scheduler::FullName(const string& symbol) {

	// Create an iterator that will go over the mapping
	map<string,QString>::iterator it;

	// Base case: if no symbol->fullname pairing is found, return the symbol
	QString noneFound = QString::fromStdString(symbol);

	// Check each symbol->fullname mapping; if a pairing is found, return the full name
	for(it = FullGasNames_.begin(); it != FullGasNames_.end(); it++){
		if(symbol == it->first){
			return it->second;
		}
	}

	// Return the base case
	return noneFound;
}

bool Scheduler::ValidParam(QLineEdit &paramBox) {

	double dValue = 0;                // Double Value of the QLineEdit
	bool validData = false;           // True if it is a double; false otherwise
	QString value(paramBox.text());   // Text inside the parameter box

	// Sets dValue equal to the double value of the text in the parameter box
	// validData is set to true if it was a double; otherwise it is set to false
	dValue = value.toDouble(&validData);

	// If the input was negative, it should be rejected (even if it was a valid double)
	if(dValue < 0){
		validData = false;
	}

	// If it was a valid double, keep it in the box, otherwise empty the box (set it to 0)
	if(validData) {
		paramBox.setText(QString::number(dValue));
		return true;
	} else {
		paramBox.setText("");
		return false;
	}
}

void Scheduler::ResetGasComboBoxes() {

	int i = 0;

	// For each gas combo/spin box combo, set the spinbox to 0 and
	//    give the combo boxes different indices (if i > # of gases, the first
	//    gas is used as a default)
	for(GasCombo gCombo : this->GasList_){
		gCombo.SpinBox_->setValue(0);
		gCombo.ComboBox_->setCurrentIndex(i);
		i++;
	}
}

bool Scheduler::InfoStructDifference(InfoStruct structA, InfoStruct structB) {

	// Is set to false if there is a difference between any of the structs' values
	bool difference = false;

	// 0th entry is the value of InfoStruct A, 1st is the value of InfoStruct B
	double ConeAngle[2]                = {structA.ConeAngle,           structB.ConeAngle};
	double Temperature[2]              = {structA.Temperature,         structB.Temperature};
	double WallTemp[2]                 = {structA.WallTemperature,     structB.WallTemperature};
	double EquilibriumRadius[2]        = {structA.AmbientRadius,       structB.AmbientRadius};
	double DriverPressure[2]           = {structA.DriverPressure,      structB.DriverPressure};
	double Frequency[2]                = {structA.Frequency,           structB.Frequency};
	double InitialTime[2]              = {structA.InitialTime,         structB.InitialTime};
	double InitialRadius[2]            = {structA.InitialRadius,       structB.InitialRadius};
	double InitialVelocity[2]          = {structA.InitialVelocity,     structB.InitialVelocity};
	double AmbientPressure[2]          = {structA.AmbientPressure,     structB.AmbientPressure};
	double AdiabaticRadius[2]          = {structA.AdiabaticRadius,     structB.AdiabaticRadius};
	double IsothermalRadius[2]         = {structA.IsothermalRadius,    structB.IsothermalRadius};
	int LiquidType[2]                  = {structA.LiquidType,          structB.LiquidType};
	DriverMode Driver[2]               = {structA.Driver,              structB.Driver};
	std::vector<double> AtomicParts[2] = {structA.AtomicParts,         structB.AtomicParts};


	/*
		Goes through each item in the InfoStruct and sees if there is a difference between the two.
		If there is, then signify a change by adding relevant info (name of what was changed,
		original value, and new value) to the list of changes. Consolas is used as it is
		fixed-width and can be aligned with ease.
	*/
	if(DoubleChanged(ConeAngle[0], ConeAngle[1])){
		difference = true;
	}

#pragma region Additional Value Comparisons
	if(DoubleChanged(Temperature[0], Temperature[1])){
		difference = true;
	}

	if(DoubleChanged(WallTemp[0], WallTemp[1])){
		difference = true;
	}

	if(DoubleChanged(EquilibriumRadius[0], EquilibriumRadius[1])){
		difference = true;
	}

	if(DoubleChanged(DriverPressure[0], DriverPressure[1])){
		difference = true;
	}

	if(DoubleChanged(Frequency[0], Frequency[1])){
		difference = true;
	}

	if(DoubleChanged(InitialTime[0], InitialTime[1])){
		difference = true;
	}

	if(DoubleChanged(InitialRadius[0], InitialRadius[1])){
		difference = true;
	}

	if(DoubleChanged(InitialVelocity[0], InitialVelocity[1])){
		difference = true;
	}

	if(DoubleChanged(AmbientPressure[0], AmbientPressure[1])){
		difference = true;
	}

	if(DoubleChanged(AdiabaticRadius[0], AdiabaticRadius[1])){
		difference = true;
	}

	if(DoubleChanged(IsothermalRadius[0], IsothermalRadius[1])){
		difference = true;
	}

	if(LiquidType[0] != LiquidType[1]){
		difference = true;
	}

	if(Driver[0] != Driver[1]){
		difference = true;
	}

	if(AtomicParts[0] != AtomicParts[1]){
		difference = true;
	}
#pragma endregion

	return difference;
}

int Scheduler::ShowInfoStructDifference(InfoStruct structA, InfoStruct structB) {

	int shouldChange = DoNothing;
	ChangeConfirmation diffUI(structA, structB, &shouldChange);
	diffUI.exec();
	return shouldChange;
}

InfoStruct Scheduler::GUIToInfoStruct() {

	InfoStruct tempStruct;									// InfoStruct representation of parameters
	int i = 0;												// index
	int gasType = 0;										// holds the index of a gas
	double sum = 0;											// holds the sum of atomic particles
	double particleCount = 0;								// holds a gases atomic particle count
	const int elementListSize = Constants::ELEMENT_TYPES;	// constant size of the list of elements
	double particleList[elementListSize] = {0};				// double array that will hold the particles 
															//    of each gas

	// Load the parameters from the UI into the temporary struct.
	// Every time a parameter box is edited, a signal to check the parameter box is sent out
	//     If the string entered is not a valid double, the box is emptied, and will return
	//     a double value of 0 when .toDouble() is called. If it is a valid double, .toDouble()
	//     will return the double value.
	tempStruct.ConeAngle           = this->UI_.ConeAngleLineText->text().toDouble();
	tempStruct.Temperature         = this->UI_.TemperatureLineText->text().toDouble();
	tempStruct.WallTemperature     = this->UI_.WallTemperatureLineText->text().toDouble();
	tempStruct.AmbientRadius       = this->UI_.EquilibriumRadiusLineText->text().toDouble();
	tempStruct.DriverPressure      = this->UI_.DriverPressureLineText->text().toDouble();
	tempStruct.Frequency           = this->UI_.FrequencyLineText->text().toDouble();
	tempStruct.InitialTime         = this->UI_.InitialTimeLineText->text().toDouble();
	tempStruct.InitialRadius       = this->UI_.InitialRadiusLineText->text().toDouble();
	tempStruct.InitialVelocity     = this->UI_.InitialVelocityLineText->text().toDouble();
	tempStruct.AmbientPressure     = this->UI_.AmbientPressureLineText->text().toDouble();
	tempStruct.AdiabaticRadius     = this->UI_.AdiabaticRadiusLineText->text().toDouble();
	tempStruct.IsothermalRadius    = this->UI_.IsothermalRadiusLineText->text().toDouble();

	// As the liquids are loaded in order, the current index corresponds to the index of the
	//    liquid's name in the liquid name list. Driver is similar.
	tempStruct.LiquidType          = this->UI_.LiquidTypeComboBox->currentIndex();
	tempStruct.Driver              = (this->UI_.DriverModeComboBox->currentIndex() == Sinusoidal) ? Sinusoidal : Constant;

	// As the gases are loaded in order, the current index corresponds to the index of the
	//    element's name in the gas list. particleList is a double array the same size as
	//    the list of element names. particleList is set up so the index of the value in the
	//    double array corresponds to the index of the element in the the element list
	for(GasCombo gasCombo : this->GasList_) {
		gasType       = gasCombo.ComboBox_->currentIndex();   // Index of the gas
		particleCount = gasCombo.SpinBox_->value();			  // Gas's atomic particles
		sum += particleCount;                                 // Sum of particles. Should be 1.0
		particleList[gasType] += particleCount;               // Add the gas to the list
	}

	// Adds the gas particles to the temp InfoStruct's gas list. As mentioned previously, this function should only be
	//    used for comparison. InfoStruct_ should used for simulation, which is only updated after loads or saves
	//    (and the data validation in the loads and saves). Due to that data validation, if this function returns
	//    an InfoStruct that matches InfoStruct_, the particle list is valid.
	for(i = 0; i < elementListSize; i++) {
		tempStruct.AtomicParts.push_back(particleList[i]);
	}

	return tempStruct;
}

bool Scheduler::DoubleChanged(double a, double b) {

	double epsilon = FLT_EPSILON; // epsilon = threshold where the difference between
								  //           values is significant enough to be a change
								  // (DBL_EPSILON was/is incorrectly reporting that initial time
								  //      and radius were changing even though they weren't,
								  //      FLT_EPSILON produces desirable output.)
	
	if(abs(a - b) < epsilon){
		return false;
	} else {
		return true;
	}
}
#pragma endregion