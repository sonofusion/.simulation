/*
  ValidationFunctions.cpp - Validation Function unit tests

  Authored by Nicholas Kriner
*/

#ifndef __ValidationFunctions_Test_H__
#define __ValidationFunctions_Test_H__

#include <gtest/gtest.h>
#include <DefaultSimulation.h>

using namespace MDSimulation;
using namespace AcceptanceTesting;

class ValidationFunctionsTest
	: public ::testing::Test {
protected:
	virtual void SetUp() {
		// nothing
	}
};

#endif // !__ValidationFunctions_Test_H__
