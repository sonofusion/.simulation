/*
  FunctionsTest.h - Functions Unit Test

  Authored by Jesse Z. Zhong
*/
#ifndef __Utilities_Test_H__
#define __Utilities_Test_H__

#include <gtest/gtest.h>
#include <../Utilities/Utilities.h>

class UtilitiesTest
	: public ::testing::Test {
protected:
	virtual void SetUp() {
		// nothing
	}

};

#endif // !__Utilities_Test_H__
