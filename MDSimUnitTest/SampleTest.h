/* 
  SampleTest.h - Sample Unit Test

  Authored by Joshua Mellott-Lillie and Jesse Z. Zhong
*/
#ifndef __Sample_Test_H__
#define __Sample_Test_H__

#include "Sample.h"
#include "gtest/gtest.h"

class SampleTest
	: public ::testing::Test {
protected:
	virtual void SetUp() {
		this->sample = new Sample();
	}

	virtual void TearDown() {
		if(this->sample) {
			delete this->sample;
			this->sample = NULL;
		}
	}

	Sample* sample;
};

#endif // !__Sample_Test_H__
