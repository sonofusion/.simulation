/*
  FunctionsTest.h - Functions Unit Test

  Authored by Jesse Z. Zhong
*/
#ifndef __Functions_Test_H__
#define __Functions_Test_H__

#include <gtest/gtest.h>
#include "Math/Functions.h"

using namespace MDSimulation;

class FunctionsTest
	: public ::testing::Test {
protected:
	virtual void SetUp() {
		Particle arr[] = {
			Particle(DoubleVector(), DoubleVector(11, 9, 0), 0, 0),
			Particle(DoubleVector(), DoubleVector(6, 14, 1), 0, 0),
			Particle(DoubleVector(), DoubleVector(9, 7, 12), 0, 0),
			Particle(DoubleVector(), DoubleVector(2, 19, 5), 0, 0),
			Particle(DoubleVector(), DoubleVector(6, 8, 20), 0, 0)
		};
		this->particles = std::vector<Particle>(arr, arr + (sizeof(arr) / sizeof(Particle)));
	}

	std::vector<Particle> particles;
};

#endif // !__Functions_Test_H__
