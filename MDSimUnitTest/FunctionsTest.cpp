/*
  FunctionsTest.cpp - Functions Unit Test

  Authored by Jesse Z. Zhong
*/
#include "FunctionsTest.h"

TEST_F(FunctionsTest, CalculatesMeanVelocity) {

	double result = 17.627810143280377619400810624928;	// Result calculated by hand.
	EXPECT_DOUBLE_EQ(result, mean_velocity(particles));
}

TEST_F(FunctionsTest, TestForMedianForOddNumberOfVelocities) {

	double result = 16.552945357246848593246977205376;	// Result calculated by hand.
	EXPECT_DOUBLE_EQ(result, median_velocity(particles));
}

TEST_F(FunctionsTest, TestForMedianForEvenNumberOfVelocities) {
	Particle arr[] = {
		Particle(DoubleVector(), DoubleVector(11, 9, 0), 0, 0),
		Particle(DoubleVector(), DoubleVector(6, 14, 1), 0, 0),
		Particle(DoubleVector(), DoubleVector(9, 7, 12), 0, 0),
		Particle(DoubleVector(), DoubleVector(2, 19, 5), 0, 0)
	};
	this->particles = std::vector<Particle>(arr, arr + (sizeof(arr) / sizeof(Particle)));

	double result = 15.908641439860298309251462952988;	// Result calculated by hand.
	EXPECT_DOUBLE_EQ(result, median_velocity(particles));
}

TEST_F(FunctionsTest, TestIntersectionTimeForNonIntersectingParticles) {
	DoubleVector pos1(10, -10, 0);
	DoubleVector vel1(0, 0, 0);
	DoubleVector pos2(10, 10, 0);
	DoubleVector vel2(2, 2, 2);
	double diameter = 1.5;

	EXPECT_DOUBLE_EQ(Constants::NEVER, intersection_time(pos1, vel1, pos2, vel2, diameter));
}

TEST_F(FunctionsTest, TestIntersectionTimeForIntersectingParticles) {
	DoubleVector pos1(10, -10, 0);
	DoubleVector vel1(0, 2, 0);
	DoubleVector pos2(10, 10, 0);
	DoubleVector vel2(0, -2, 0);
	double diameter = 2;

	double result = 4.5;	// Result calculated by hand.
							// distance(20) = diameter(2) - totalVelocity(4) * time(x)
							// x = 4.5

	EXPECT_DOUBLE_EQ(result, intersection_time(pos1, vel1, pos2, vel2, diameter));
}

