/*
  SolverTest.h - Solver Unit Test

  Authored by Joshua Mellott-Lillie and Jesse Z. Zhong
*/

#ifndef __Solver_Test_H__
#define __Solver_Test_H__

#include "ODE/Solver.h"
#include "gtest/gtest.h"

using namespace MDSimulation;

class SolverTest : 
	public ::testing::Test {
protected:

	virtual void TearDown() {
		if(this->solver) {
			delete this->solver;
			this->solver = NULL;
		}
	}

	Solver<void (double, const double*, double*)>* solver;
};

#endif // !__Solver_Test_H__
