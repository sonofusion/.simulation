#include "Sample.h"

long Sample::factorial(long n)
{
	return n == 1 ? 1 : n * factorial(n - 1);
}
