/*
  ValidationFunctions.cpp - Validation Function testing

  Authored by Nicholas Kriner
  http://www.chemteam.info/GasLaw/WS1-Ideal.html example Ideal Gas problems retrieved 2013-12-18
                                                 Units converted to simulation-appropriate units where applicable,
												 e.g. pressure should be tested in Pascals and not atm
*/
#include "ValidationFunctions.h"

const double ACCEPTABLE_PERCENT_ERROR = 0.0005; // 0.05% error (mostly to account for rounding
                                                //              at very, very small decimal places)

TEST_F(ValidationFunctionsTest, AverageOfVector){

	double exactResult = (1 + 16 + 14 + 2 + 13 + 15) / 6.0;

	double testDoubles[] = {1, 16, 14, 2, 13, 15};
	std::vector<double> testVector(begin(testDoubles), end(testDoubles));
	
	EXPECT_DOUBLE_EQ(exactResult, DefaultSimulation::AverageOfVector(testVector));
}

TEST_F(ValidationFunctionsTest, VarianceOfVector){

	double expected = 46.166666667; // Variance using Bessel's correction

	double testDoubles[] = {1, 16, 14, 2, 13, 15};
	std::vector<double> testVector(begin(testDoubles), end(testDoubles));

	double actual = DefaultSimulation::VarianceOfVector(testVector);

	double percentError = std::abs(actual - expected) / expected;
	
	EXPECT_TRUE(percentError < ACCEPTABLE_PERCENT_ERROR);
}

TEST_F(ValidationFunctionsTest, StdevOfVector){

	double expected = 6.794605704; // Variance using Bessel's correction

	double testDoubles[] = {1, 16, 14, 2, 13, 15};
	std::vector<double> testVector(begin(testDoubles), end(testDoubles));
	
	double actual = DefaultSimulation::StdevOfVector(testVector);

	double percentError = std::abs(actual - expected) / expected;

	EXPECT_TRUE(percentError < ACCEPTABLE_PERCENT_ERROR);
}

TEST_F(ValidationFunctionsTest, IdealGasLawPressureCalculation){

	double expected = 79351.6418;

	double temperature = 300.0;
	double volume = 0.004;
	double mols = 0.1272467;

	double actual =  DefaultSimulation::IdealGasLaw_GetPressure(temperature, mols, volume);

	double percentError = std::abs(actual - expected) / expected;

	EXPECT_TRUE(percentError < ACCEPTABLE_PERCENT_ERROR);
}

TEST_F(ValidationFunctionsTest, IdealGasLawTemperatureCalculation){

	double expected = 446.9206913;


	double pressure = 197600.0;
	double volume = 0.0123;
	double mols = 0.654;

	double actual =  DefaultSimulation::IdealGasLaw_GetTemperature(pressure, mols, volume);

	double percentError = std::abs(actual - expected) / expected;

	EXPECT_TRUE(percentError < ACCEPTABLE_PERCENT_ERROR);
}

TEST_F(ValidationFunctionsTest, IdealGasLawVolumeCalculation){

	double expected = 0.001191399;

	double pressure = 101325;
	double temperature = 273.0;
	double mols = (2.34 / 44.0);

	double actual = DefaultSimulation::IdealGasLaw_GetVolume(pressure, temperature, mols);

	double percentError = std::abs(actual - expected) / expected;

	EXPECT_TRUE(percentError < ACCEPTABLE_PERCENT_ERROR);
}