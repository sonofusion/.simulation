/*
  UtilitiesTest.cpp - Utilities Unit Test

  Authored by Nicholas Kriner
*/

#include "UtilitiesTest.h"


TEST_F(UtilitiesTest, ConicalIntegration) {

	double expected = 253.72584235751591665045044486384472104982;

	double rho = 4;
	double phi = 3.14159265359/4;
	double theta = 2*3.14159265359;
	
	double actual = Utilities::ConicalSection(rho, phi, true);

	double percentError = abs(actual - expected) / expected;
	
	EXPECT_TRUE(percentError < 0.005);
}