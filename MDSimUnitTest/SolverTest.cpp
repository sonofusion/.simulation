/* 
  SolverTest.cpp - Solver Unit Test

  Authored by Joshua Mellott-Lillie and Jesse Z. Zhong
*/
#include "SolverTest.h"

void DerivativeFunc(double x, const double* y, double* dydx) {
	*dydx =  -2 * (*y) + x + 4;
}

TEST_F(SolverTest, Init) {
	solver = NULL;
	EXPECT_EQ(NULL, solver);
}

TEST_F(SolverTest, FirstStep) {
	double *x = NULL;
	double *y = NULL;
	solver = new Solver<void (double, const double*, double*)>(DerivativeFunc, 1, x, y);
	EXPECT_EQ(0, solver->GetNextStep());
}

TEST_F(SolverTest, AttemptStep) {
	double x[8];
	double y[8];
	for(int i = 0; i < 8; i++)
		x[i] = y[i] = 0;

	x[0] = 0;
	y[0] = 1;

	solver = new Solver<void (double, const double*, double*)>(DerivativeFunc, 1, x, y);

	double epsilon = 0.00001;
	double trueValue = 1.34726;
	int val = solver->RK4(0.2, epsilon, 0.05, 8);

	EXPECT_TRUE((y[val] > (trueValue - epsilon)) && (y[val] < (trueValue + epsilon)));
}