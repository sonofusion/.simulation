/* 
  SampleTest.cpp - Sample Unit Test

  Authored by Joshua Mellott-Lillie and Jesse Z. Zhong
*/
#include "SampleTest.h"

TEST_F(SampleTest,  Init) {
	EXPECT_EQ(3628800L, sample->factorial(10));
}